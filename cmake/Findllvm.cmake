# Copyright (c) 2014 Andrew Kelley
# This file is MIT licensed.
# See http://opensource.org/licenses/MIT

# LLVM_FOUND
# LLVM_INCLUDE_DIRS
# LLVM_LIBRARIES

find_program(LLVM_CONFIG_EXE
        NAMES llvm-config llvm-config-9.0
        PATHS
        "/mingw64/bin"
        "/c/msys64/mingw64/bin"
        "c:/msys64/mingw64/bin"
        "C:/Libraries/llvm-9.0.0/bin")

message(STATUS "llvm-config found on ${LLVM_CONFIG_EXE}")

#if (NOT (CMAKE_BUILD_TYPE STREQUAL "Debug"))
    execute_process(
            COMMAND ${LLVM_CONFIG_EXE} --libfiles --link-shared
            OUTPUT_VARIABLE LLVM_LIBRARIES_SPACES
            OUTPUT_STRIP_TRAILING_WHITESPACE)
    string(REPLACE " " ";" LLVM_LIBRARIES "${LLVM_LIBRARIES_SPACES}")

#endif ()

if (NOT LLVM_LIBRARIES)
    execute_process(
            COMMAND ${LLVM_CONFIG_EXE} --libfiles
            OUTPUT_VARIABLE LLVM_LIBRARIES_SPACES
            OUTPUT_STRIP_TRAILING_WHITESPACE)
    string(REPLACE " " ";" LLVM_LIBRARIES "${LLVM_LIBRARIES_SPACES}")
    message(STATUS "Using static LLVM library")
else()
    message(STATUS "Using shared LLVM library")
endif ()

execute_process(
        COMMAND ${LLVM_CONFIG_EXE} --system-libs
        OUTPUT_VARIABLE LLVM_SYSTEM_LIBS_SPACES
        OUTPUT_STRIP_TRAILING_WHITESPACE)
string(REPLACE "-l" "" LLVM_SYSTEM_LIBS_SPACES2 "${LLVM_SYSTEM_LIBS_SPACES}")
string(REPLACE " " ";" LLVM_SYSTEM_LIBS "${LLVM_SYSTEM_LIBS_SPACES2}")

execute_process(
        COMMAND ${LLVM_CONFIG_EXE} --includedir
        OUTPUT_VARIABLE LLVM_INCLUDE_DIRS
        OUTPUT_STRIP_TRAILING_WHITESPACE)

set(LLVM_LIBRARIES ${LLVM_LIBRARIES} ${LLVM_SYSTEM_LIBS})

if (NOT LLVM_LIBRARIES)
    find_library(LLVM_LIBRARIES NAMES LLVM LLVM-9.0 LLVM-9)
endif ()

message(STATUS "LLVM include dirs: ${LLVM_INCLUDE_DIRS}")
message(STATUS "LLVM libs: ${LLVM_LIBRARIES}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LLVM DEFAULT_MSG LLVM_LIBRARIES LLVM_INCLUDE_DIRS)

mark_as_advanced(LLVM_INCLUDE_DIRS LLVM_LIBRARIES)
