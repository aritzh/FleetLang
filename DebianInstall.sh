#!/usr/bin/env bash
apt-get update -y
apt-get install wget gnupg ca-certificates -y --no-install-recommends
echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-6.0 main" > /etc/apt/sources.list.d/llvm.list && \
echo "deb-src http://apt.llvm.org/stretch/ llvm-toolchain-stretch-6.0 main" >> /etc/apt/sources.list.d/llvm.list && \
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
apt update -y
apt install -y --no-install-recommends \
    llvm-6.0-dev \
    lld-6.0 \
    cmake \
    build-essential \
    pkg-config \
    uuid-dev \
    openjdk-8-jdk \
    python3 \
    lld-6.0-dev \
    libz-dev

echo "Done"