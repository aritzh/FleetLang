#include <iostream>
#include <stdint.h>
#include <cstdlib>

extern "C" {

    struct Test {
        uint64_t a;
        uint64_t b;
        uint64_t c;
    };

    void test(int a) {
        std::cout << "\"" << a << "\"" << std::endl;
    }

    void test_struct(Test &a)  {
        std::cout << "A: " << a.a << std::endl << "B: " << a.b << std::endl;
    }

    void print_cstr(char *c) {
        std::cout << c << std::endl;
    }

    void print_byte(int8_t v) {
        std::cout << (uint)v << std::endl;
    }

    int randomint() {
        return rand()%10;
    }

    void println() {
        std::cout << std::endl;
    }

    void printiln(int32_t x) {
        std::cout << x << std::endl;
    }

    void printihln(int32_t x) {
        std::cout << std::hex << x << std::endl;
    }

    void initrand() {
        srand(time(0));
    }

    void print(int32_t x) {
        std::cout << x << " ";
    }

    void printd(double v) {
        std::cout << v << std::endl;
    }
}