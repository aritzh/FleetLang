#!/usr/bin/env python3
import os
import re
import sys
from subprocess import Popen, PIPE

compiler_path = sys.argv[1]
input_file = sys.argv[2]

# obj_file_name = "test.o"
executable_file_name = "test"

current_file_dir = os.path.dirname(os.path.realpath(__file__))
compiler_command = "{} -si {} -o {} --stdlib ../src/fleet".format(compiler_path, input_file, executable_file_name)
# linking_command = "g++ {}/test.cpp {} -o {}".format(current_file_dir, obj_file_name, executable_file_name)

print("Running at: '{}'".format(os.getcwd()))


def remove_file(name):
    if os.path.isfile(name):
        pass
        # os.remove(name)


def exit_and_clean(code=0):
    # remove_file(obj_file_name)
    remove_file(executable_file_name)
    exit(code)


def run_command(command):
    print("Running '{}'".format(command), flush=True)
    process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()
    return process.returncode, out.decode("utf-8"), err.decode("utf-8")


def compile():
    return run_command(compiler_command)


# def link():
#     return run_command(linking_command)


def run():
    return run_command(os.path.join(os.getcwd(), executable_file_name))


with open(input_file, 'r') as in_file:
    first = in_file.readline()

should_compile = False if re.match("//\s+(\w+)", first).group(1) == "FAIL" else True
without_extension = re.match("(.*)\.[^.]*", input_file).group(1)
output_file_path = os.path.join(os.path.dirname(input_file), without_extension + ".out")

retcode, out, err = compile()

if retcode != 0 and should_compile:
    print("Error compiling the test '{}'".format(input_file), file=sys.stderr)
    print(out, file=sys.stderr)
    print(err, file=sys.stderr)
    exit_and_clean(1)
elif retcode == 0 and not should_compile:
    print("Error: The test '{}' should not compile".format(input_file), file=sys.stderr)
    exit_and_clean(1)
elif retcode != 0 and not should_compile:
    exit_and_clean()

# If an output file exists, the output has to be checked
if not os.path.exists(output_file_path):
    exit_and_clean()

# Run the program, check the output
# retcode, out, err = link()
# if retcode != 0:
#     print("Error linking test '{}'".format(input_file), file=sys.stderr)
#     print(err, file=sys.stderr)
#     exit_and_clean(1)

retcode, out, err = run()
if retcode != 0:
    print("Test exited with exit status {}".format(retcode), file=sys.stderr)
    print(out, file=sys.stderr)
    print(err, file=sys.stderr)
    exit_and_clean(1)

with open(output_file_path, 'r') as out_file:
    expected_output = out_file.read()

if expected_output != out:
    print("Output does not match in test '{}'".format(input_file), file=sys.stderr)
    print("Expected:\n" + expected_output, file=sys.stderr)
    print("Real:\n" + out, file=sys.stderr)
    exit_and_clean(1)
else:
    exit_and_clean()
