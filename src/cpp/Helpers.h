#pragma once

#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <map>

std::string repeat(const std::string &str, int times);

bool ends_with(const std::string &value, const std::string &ending);

std::vector<std::string> split(std::string haystack, const std::string &needle);

template <typename T>
void filter(std::vector<T> &list, const std::function<bool(const T &)> &predicate) {
    if (list.size() == 0)
        return;
    list.erase(std::remove_if(list.begin(), list.end(), std::not1(predicate)), list.end());
}

template<typename K, typename V>
int erase_if(std::map<K, V> & mapOfElemen, V value)
{
    int totalDeletedElements = 0;
    auto it = mapOfElemen.begin();
    // Iterate through the map
    while (it != mapOfElemen.end())
    {
        // Check if value of this entry matches with given value
        if (it->second == value)
        {
            totalDeletedElements++;
            // Erase the current element, erase() will return the
            // next iterator. So, don't need to increment
            it = mapOfElemen.erase(it);
        }
        else
        {
            // Go to next entry in map
            it++;
        }
    }
    return totalDeletedElements;
}
