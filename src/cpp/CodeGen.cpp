#include <llvm/IR/Instructions.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/Scalar.h>
#include <llvm/Transforms/Scalar/GVN.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/Verifier.h>
#include <unistd.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/FileSystem.h>
#include <fstream>
#include <lld/Common/Driver.h>
#include <sstream>
#include <utility>
#include "CodeGen.h"
#include "SymbolTable.h"
#include "AST/Module.h"
#include "Settings.h"
#include "AST/ExprAST.h"
#include "Platform.h"


CodeGen::CodeGen()
        : table(new SymbolTable(this)),
          successful(true),
          TheContext(new llvm::LLVMContext()),
          TheModule(new llvm::Module("FleetLang", *TheContext)),
          Builder(*TheContext),
          DB(new llvm::DIBuilder(*TheModule)),
          log(spd::get("console")) {
    using namespace llvm;
    using namespace llvm::sys;

    compileUnit = DB->createCompileUnit(dwarf::DW_LANG_C, DB->createFile("file", "."), "Fleetlang compiler", false, "",
                                        0);

    InitializeAllTargetInfos();
    InitializeAllTargets();
    InitializeAllTargetMCs();
    InitializeAllAsmParsers();
    InitializeAllAsmPrinters();

    std::string TargetTriple;
    if (settings.target_triple.length() == 0) {
        TargetTriple = Triple::normalize(sys::getDefaultTargetTriple());
    } else {
        TargetTriple = settings.target_triple;
    }
//    TargetTriple = "arm-none-linux-gnueabihf";
//    TargetTriple = "i686-pc-windows-msvc";
    llvm::Triple triple(TargetTriple);

    this->target = {triple.getArchName(), triple.getVendorName(), triple.getOSName(), triple.getEnvironmentName()};

    TheModule->setTargetTriple(TargetTriple);

    log->debug("Building for target {}", TargetTriple);

    std::string Error;
    auto *Target = TargetRegistry::lookupTarget(TargetTriple, Error);

    // Print an error and exit if we couldn't find the requested target.
    // This generally occurs if we've forgotten to initialise the
    // TargetRegistry or we have a bogus target triple.
    if (!Target) {
        errs() << Error;
        return;
    }

    auto CPU = "generic";
    auto Features = "";

    TargetOptions opt;
    opt.ExceptionModel = ExceptionHandling::WinEH;
    auto RM = Optional<Reloc::Model>();
    TheTargetMachine = std::unique_ptr<llvm::TargetMachine>(Target->createTargetMachine(TargetTriple,
                                                                                        CPU,
                                                                                        Features,
                                                                                        opt,
                                                                                        RM));
}

void CodeGen::Generate(const Module &p) {
    for (const auto &f : p.GetTopLevelDecls()) {
        if (auto func = dynamic_cast<FunctionDeclaration *>(f.get())) {
            this->InsertUngenerated(func);
        } else if (dynamic_cast<StructDeclaration *>(f.get())) {
            this->InsertUngenerated(std::dynamic_pointer_cast<StructDeclaration>(f));
        } else if (/*auto var = */dynamic_cast<VarDeclStmtAST *>(f.get())) {
            // For now, do nothing
        } else {
            throw std::logic_error("Unknown top level declaration");
        }
    }

    for (const auto &f : p.GetTopLevelDecls()) {
        if (auto var = dynamic_cast<VarDeclStmtAST *>(f.get())) {
            var->Generate(*this);
        }
    }

    for (const auto &f : p.GetTopLevelDecls()) {
        if (auto func = dynamic_cast<Function *>(f.get())) {
            if (/*func->GetName() == "main" || */func->GetName() == "_start" || settings.stdlib_path.empty()) {
                this->currentFunction = func->GetName();
                func->Generate(*this);
            }
        }
    }

    while (!this->toGenerate.empty()) {
        FunctionDeclaration *it = *this->toGenerate.begin();
        this->toGenerate.erase(it);
        this->currentFunction = it->GetName();
        it->Generate(*this);
    }

    this->RunPasses();

    this->DB->finalize();
}

VarDeclStmtAST *CodeGen::FindVariable(const std::string &name) {
    return this->table->FindVariable(name);
}

FunctionDeclaration *CodeGen::FindFunction(const std::string &name, const std::vector<std::weak_ptr<Type>> &types) {
    FunctionDeclaration *function = this->table->FindFunction(name, types);
    if (function == nullptr) {
        function = this->table->FindUngeneratedFunction(name, types);

        if (function != nullptr) {
            function->GenerateDeclaration(*this);
            toGenerate.insert(function);
            this->table->GenerateFunction(function);
            return function;
        } else {
            return nullptr;
        }
    }
    return function;
}

FunctionDeclaration *CodeGen::FindDeclaredFunction(const std::string &name,
                                                   const std::vector<std::weak_ptr<Type>> &types) {
    return this->table->FindFunction(name, types);
}


std::weak_ptr<StructDeclaration> CodeGen::FindStruct(const std::string &name) {
    std::shared_ptr<StructDeclaration> str = this->table->FindStruct(name).lock();
    if (!str) {
        str = this->table->FindUngeneratedStruct(name).lock();
        if (str != nullptr) {
            str->Generate(*this);
            this->table->GenerateStruct(name);
            return str;
        } else {
            return std::weak_ptr<StructDeclaration>();
        }
    }
    return str;
}

llvm::Value *CodeGen::InsertSymbol(VarDeclStmtAST *var) {
    this->table->addVariable(var);
    return var->GetLLVMValue();
}

llvm::Value *CodeGen::InsertSymbol(const std::string &name, VarDeclStmtAST *var) {
    this->table->addVariable(name, var);
    return var->GetLLVMValue();
}

llvm::Function *CodeGen::InsertSymbol(FunctionDeclaration *func) {
    this->table->addFunction(func);
    return func->GetLLVMValue();
}

llvm::StructType *CodeGen::InsertSymbol(const std::weak_ptr<StructDeclaration> &type) {
    this->table->addStruct(type);
    return llvm::dyn_cast<llvm::StructType>(type.lock()->GetLLVMType(*this));
}

void CodeGen::WriteOBJ(std::string Filename) {
    using namespace llvm;
    using namespace llvm::sys;

//    MPM.run(*TheModule);

    if (Filename == "-") {
        char tmp_file_name[24];
        strncpy(tmp_file_name, "/tmp/obj-file-XXXXXX\0", 21);
        int fd = mkstemp(tmp_file_name);
        if (fd != -1) {
            close(fd);
            std::error_code EC;
            raw_fd_ostream dest(tmp_file_name, EC, sys::fs::F_None);

            if (EC) {
                errs() << "Could not open file: " << EC.message();
                unlink(tmp_file_name);
                return;
            }

            legacy::PassManager pass;
            auto FileType = TargetMachine::CGFT_ObjectFile;

            if (TheTargetMachine->addPassesToEmitFile(pass, dest, nullptr, FileType)) {
                errs() << "The target machine can't emit a file of this type";
                unlink(tmp_file_name);
                return;
            }

            pass.run(*TheModule);
            dest.flush();

            std::string line;
            std::ifstream openfile(tmp_file_name);
            if (openfile.is_open()) {
                while (!openfile.eof()) {
                    std::getline(openfile, line);
                    std::cout << line << std::endl;
                }
            }
            unlink(tmp_file_name);
        } else {
            errs() << "Could not get temporary file";
        }
    } else {
        std::error_code EC;
        raw_fd_ostream dest(Filename, EC, sys::fs::F_None);

        if (EC) {
            errs() << "Could not open file: " << EC.message();
            return;
        }

        legacy::PassManager pass;
        auto FileType = TargetMachine::CGFT_ObjectFile;

        if (TheTargetMachine->addPassesToEmitFile(pass, dest, nullptr, FileType)) {
            errs() << "The target machine can't emit a file of this type";
            return;
        }

        pass.run(*TheModule);
        dest.flush();
    }

    //outs() << "Wrote " << Filename << "\n";
}

void CodeGen::RunPasses() {
    using namespace llvm;
    using namespace llvm::sys;

    TheModule->setDataLayout(TheTargetMachine->createDataLayout());

    legacy::FunctionPassManager FPM(TheModule);

    if (settings.release) {
        FPM.add(llvm::createConstantPropagationPass());
        FPM.add(llvm::createDeadCodeEliminationPass());
        FPM.add(llvm::createReassociatePass());
        FPM.add(llvm::createGVNPass());
        FPM.add(llvm::createCFGSimplificationPass());
//        FPM.add(llvm::createScalarizerPass());

        PassManagerBuilder PMBuilder;
        PMBuilder.OptLevel = 3;
        PMBuilder.SizeLevel = 0;

        // TargetLibraryInfoImpl tlii(Triple(TheModule.getTargetTriple()));
        // PMBuilder.LibraryInfo = &tlii;

        legacy::PassManager MPM;
        PMBuilder.populateFunctionPassManager(FPM);
        PMBuilder.populateModulePassManager(MPM);
        MPM.run(*TheModule);
    }

    FPM.doInitialization();
    for (llvm::Function &F : *TheModule)
        if (!F.isDeclaration()) {
            FPM.run(F);
        }
    FPM.doFinalization();
}

llvm::AllocaInst *CodeGen::CreateAlloca(Type *type, std::string name) {
    llvm::Function *TheFunction = Builder.GetInsertBlock()->getParent();
    llvm::IRBuilder<> TmpB(&TheFunction->getEntryBlock(), TheFunction->getEntryBlock().begin());

    std::string varName = TheFunction->getName().str() + "_" + name;

    // TODO Check redeclared var names

    llvm::Type *ty = type->GetLLVMType(*this);
    if (ty == nullptr) return nullptr;
    llvm::AllocaInst *alloca = TmpB.CreateAlloca(ty, nullptr, varName);

    return alloca;
}

void CodeGen::printLLVM_IR() {
    TheModule->print(llvm::outs(), nullptr);
}

CodeGen::~CodeGen() {
    delete table;
    delete TheContext;
    delete DB;
}

bool CodeGen::wasSuccessful() const {
    return successful;
}

void CodeGen::InsertUngenerated(FunctionDeclaration *func) {
    this->table->addUngenerated(func);
}

void CodeGen::InsertUngenerated(std::weak_ptr<StructDeclaration> type) {
    this->table->addUngenerated(std::move(type));
}

std::weak_ptr<StructDeclaration> CodeGen::FindUngeneratedStruct(const std::string &name) {
    return this->table->FindUngeneratedStruct(name);
}

std::weak_ptr<Type> CodeGen::ResolveType(const std::weak_ptr<Type> &type) {
    auto shType = type.lock();
    if (auto primitive = std::dynamic_pointer_cast<PrimitiveType>(shType)) {
        unsigned int pointerSize = this->TheModule->getDataLayout().getPointerSize() * 8;
        if (primitive->getType() == EPrimitiveType::Int) {
            this->types.emplace_back(new PrimitiveType(getPrimitiveFromSize(pointerSize, true)));
            std::shared_ptr<Type> &shNewType = this->types.back();
            return shNewType;
        } else if (primitive->getType() == EPrimitiveType::Uint) {
            this->types.emplace_back(new PrimitiveType(getPrimitiveFromSize(pointerSize, false)));
            return this->types.back();
        }
    } else if (auto named = std::dynamic_pointer_cast<NamedType>(shType)) {
        return FindStruct(named->getName());
    }
    return type;
}

unsigned int CodeGen::GetPointerSize() {
    return this->TheModule->getDataLayout().getPointerSize() * 8;
}

void CodeGen::PopScope() {
    assert(this->table->GetParent() != nullptr);

    auto *newTable = this->table->GetParent();
    delete this->table;
    this->table = newTable;
}

void CodeGen::PushScope() {
    this->table = new SymbolTable(this->table);
}

const Target &CodeGen::GetTarget() const {
    return target;
}

const std::string &CodeGen::GetCurrentFunction() const {
    return currentFunction;
}

//void MyOStream::write_impl(const char *ptr, size_t len) {
//    ss << ptr << '\n';
//    pos += len;
//}
//
//uint64_t MyOStream::current_pos() const {
//    return pos;
//}

void CodeGen::linkFile(const std::string &objPath, const std::string &output, bool removeObj) {
    std::vector<const char *> args;
    args.push_back("link");
    args.push_back(objPath.c_str());
    args.push_back("-o");
    args.push_back(output.c_str());

    std::string out;
    llvm::raw_string_ostream os(out);
    bool result = lld::elf::link(args, false, os);

    if (!result) {
        log->error("Error linking executable");
        os.flush();
        log->error(out);
    }

    if (removeObj) {
        removeFile(objPath);
    }
}

std::weak_ptr<Type> CodeGen::addType(std::shared_ptr<Type> type) {
    this->types.push_back(type);
    return type;
}

llvm::ConstantInt *CodeGen::CreateLiteralInt(uint64_t value, PrimitiveType &type) {
    return llvm::ConstantInt::get(*TheContext, llvm::APInt(type.getSizeInBits(*this), value, type.isSigned()));
}

llvm::ConstantFP *CodeGen::CreateLiteralFloat(float value) {
    return llvm::ConstantFP::get(*TheContext, llvm::APFloat(value));
}

llvm::ConstantFP *CodeGen::CreateLiteralDouble(double value) {
    return llvm::ConstantFP::get(*TheContext, llvm::APFloat(value));
}

llvm::ConstantInt *CodeGen::CreateLiteralBool(bool value) {
    return llvm::ConstantInt::get(*TheContext, llvm::APInt(1, value ? 1 : 0, false));
}
