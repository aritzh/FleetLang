// Created by aritz.

#pragma once


#include <memory>
#include <vector>
#include <unordered_map>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include "AST/StmtAST.h"
#include "AST/Function.h"
#include "AST/StructDeclaration.h"

class SymbolTable {

    SymbolTable *parent;

    std::unordered_map<std::string, VarDeclStmtAST *> variables;
    std::unordered_map<std::string, std::vector<FunctionDeclaration *>> functions;
    std::unordered_map<std::string, std::weak_ptr<StructDeclaration>> types;

    std::unordered_map<std::string, std::vector<FunctionDeclaration *>> ungenerated_functions;
    std::unordered_map<std::string, std::weak_ptr<StructDeclaration>> ungenerated_types;

    CodeGen *gen;

public:
    explicit SymbolTable(CodeGen *gen);

    explicit SymbolTable(SymbolTable *parent);

    void addVariable(VarDeclStmtAST *v);

    void addVariable(const std::string &name, VarDeclStmtAST *v);

    void addFunction(FunctionDeclaration *f);

    void addStruct(std::weak_ptr<StructDeclaration> t);

    VarDeclStmtAST *FindVariable(const std::string &name);

    FunctionDeclaration *FindFunction(const std::string &name, const std::vector<std::weak_ptr<Type>> &argTypes);

    std::weak_ptr<StructDeclaration> FindStruct(const std::string &name);

    VarDeclStmtAST *FindVariableInCurrentScope(const std::string &name);

    FunctionDeclaration *FindFunctionInCurrentScope(const std::string &name,
                                                    const std::vector<std::weak_ptr<Type>> &argTypes);

    std::weak_ptr<StructDeclaration> FindStructInCurrentScope(const std::string &name);

    FunctionDeclaration *FindUngeneratedFunction(const std::string &name,
                                                 const std::vector<std::weak_ptr<Type>> &argTypes);

    FunctionDeclaration *FindUngeneratedFunctionInCurrentScope(const std::string &name,
                                                               const std::vector<std::weak_ptr<Type>> &argTypes);

    std::weak_ptr<StructDeclaration> FindUngeneratedStruct(const std::string &name);

    std::weak_ptr<StructDeclaration> FindUngeneratedStructInCurrentScope(const std::string &name);

    void GenerateFunction(FunctionDeclaration *f);

    void GenerateStruct(const std::string &name);

    SymbolTable *GetParent();

    void addUngenerated(FunctionDeclaration *f);

    void addUngenerated(std::weak_ptr<StructDeclaration> t);
};


