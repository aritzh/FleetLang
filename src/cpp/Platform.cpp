#include "Platform.h"
#include <spdlog/spdlog.h>

#if defined(__unix__)

#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <list>
#include <stack>
#include "Helpers.h"

namespace spd = spdlog;

std::string cannonicalPath(const std::string &path) {
    std::vector<std::string> components = split(path, "/");
    std::vector<std::string> current;

    if (!components[0].empty()) {
        char currentDir[PATH_MAX];
        if (!getcwd(currentDir, PATH_MAX)) {
            perror("Error getting current working directory");
            exit(1);
        }
        std::vector<std::string> currentComponents = split(currentDir, "/");
        currentComponents.insert(currentComponents.end(), components.begin(), components.end()); // Add the rest at the end
        components = currentComponents;
    }
    for (const std::string &component : components) {
        if (component.empty() || component == ".") continue;
        else if (component == "..") current.pop_back();
        else current.push_back(component);
    }
    std::string result;
    for (const std::string &component : current) {
        result.append("/").append(component);
    }
    return result;
}

std::vector<std::string> ListDir(const std::string &path) {
    DIR *dir;
    std::vector<std::string> result;
    std::string cannonical_path = cannonicalPath(path);
    struct dirent *ent;
    if ((dir = opendir(cannonical_path.c_str()))) {
        while ((ent = readdir(dir))) {
            if (ent->d_type == DT_DIR && (std::string(".") == ent->d_name || std::string("..") == ent->d_name)) continue;
            result.push_back(cannonical_path + "/" + ent->d_name);
        }
        closedir(dir);
    } else {
        switch (errno) {
            case ENOTDIR:
                spd::get("console")->warn("Compiling a single file in directory mode. Consider passing the -s flag");
                result.push_back(path);
                return result;
            case ENOENT:
            default:
                spd::get("console")->error("Error: Directory '{}' does not exists", path);
        }
    }
    return result;
}

std::string getParentDir(const std::string &path) {
    std::string cannon = cannonicalPath(path);
    auto pos = cannon.rfind('/');
    if (pos == std::string::npos) {
        throw std::logic_error("Cannonical path does not contain a trailing slash");
    }
    if (pos == 0) { // If the last slash is the fist char, it must be the filesystem root, or its parent is the root
        return "/";
    }
    return cannon.substr(0, pos);
}

bool removeFile(const std::string &path) {
    return unlink(path.c_str()) == 0;
}

#else
#error Current platform is not implemented yet
#endif

std::vector<std::string> ListDir(const std::string &path, const std::function<bool(const std::string &)> &predicate) {
    std::vector<std::string> list = ListDir(path);
    filter<std::string>(list, predicate);
    return list;
}

DirectoryNotExists::DirectoryNotExists() : reason("Directory does not exist") {}

NotADirectory::NotADirectory() : reason("Path is not a directory") {}

const char *NotADirectory::what() const noexcept {
    return reason.c_str();
}

const char *DirectoryNotExists::what() const noexcept {
    return reason.c_str();
}
