// Created by aritz.

#pragma once


#include "AST/Module.h"
#include "SymbolTable.h"

class Validator {
    SymbolTable *table;
    FunctionDeclaration *currentFunction = nullptr;

public:
    Validator();

    ~Validator();

    bool check(Module *p);

private:

    void pushScope();

    void popScope();

    bool check(FunctionDeclaration *f);

    bool check(Function *f);

    bool check(StmtAST *s);

    bool check(AssignStmtAST *s);

    bool check(IfStmtAST *s);

    bool check(WhileStmtAST *s);

    bool check(ReturnStmtAST *s);

    bool check(ExprStmtAST *s);

    bool check(BlockStmtAST *s);

    bool check(VarDeclStmtAST *s);


    std::pair<bool, std::unique_ptr<Type>> check(ExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(StringExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(FloatExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(DoubleExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(IntExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(LiteralBoolExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(BinaryExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(UnaryExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(CallExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(VariableExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(ComparisonExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(BoolOpExprAST *e);

    std::pair<bool, std::unique_ptr<Type>> check(CastingExprAST *e);

    EPrimitiveType CanCoerce(EPrimitiveType a, EPrimitiveType b);

    Type *CanCoerce(Type *a, Type *b);

    EPrimitiveType SmallestType(EPrimitiveType a, EPrimitiveType b);
};


