// Created by aritz.

#include <sstream>
#include "SymbolTable.h"
#include "AST/ExprAST.h"

SymbolTable::SymbolTable(SymbolTable *parent)
        : parent(parent) {
    assert(parent != nullptr);
    gen = parent->gen;
}

SymbolTable::SymbolTable(CodeGen *gen)
        : parent(nullptr), gen(gen) {}

void SymbolTable::addVariable(VarDeclStmtAST *v) {
    variables.emplace(v->GetName(), v);
}

void SymbolTable::addVariable(const std::string &name, VarDeclStmtAST *v) {
    variables.emplace(name, v);
}

void SymbolTable::addFunction(FunctionDeclaration *f) {
    auto it = functions.find(f->GetName());
    if (it == functions.end()) {
        std::vector<FunctionDeclaration *> v;
        v.push_back(f);
        functions.emplace(f->GetName(), v);
    } else {
        it->second.push_back(f);
    }
}

void SymbolTable::addStruct(std::weak_ptr<StructDeclaration> t) {
    types.emplace(t.lock()->GetName(), t);
}

VarDeclStmtAST *SymbolTable::FindVariable(const std::string &name) {
    auto v = this->variables.find(name);
    if (v != this->variables.end()) return v->second;
    if (this->parent != nullptr) {
        auto var = this->parent->FindVariable(name);
        if (var != nullptr) return var;
    }
    return nullptr;
}

FunctionDeclaration *SymbolTable::FindFunction(const std::string &name,
                                               const std::vector<std::weak_ptr<Type>> &argTypes) {
    auto functions = this->functions.find(name);
    if (functions != this->functions.end()) {
        for (auto func : functions->second) {
            if (func->accepts(*gen, argTypes)) {
                return func;
            }
        }
    }
    if (this->parent != nullptr) {
        auto var = this->parent->FindFunction(name, argTypes);
        if (var != nullptr) return var;
    }
    return nullptr;
}

SymbolTable *SymbolTable::GetParent() {
    return this->parent;
}

VarDeclStmtAST *SymbolTable::FindVariableInCurrentScope(const std::string &name) {
    auto v = this->variables.find(name);
    if (v != this->variables.end()) return v->second;
    return nullptr;
}

FunctionDeclaration *SymbolTable::FindFunctionInCurrentScope(const std::string &name,
                                                             const std::vector<std::weak_ptr<Type>> &argTypes) {
    auto functions = this->functions.find(name);
    if (functions == this->functions.end()) return nullptr;

    for (auto func : functions->second) {
        if (func->accepts(*gen, argTypes)) {
            return func;
        }
    }
    return nullptr;
}

std::weak_ptr<StructDeclaration> SymbolTable::FindStructInCurrentScope(const std::string &name) {
    auto s = this->types.find(name);
    if (s != this->types.end()) return s->second;
    return std::weak_ptr<StructDeclaration>();
}

std::weak_ptr<StructDeclaration> SymbolTable::FindStruct(const std::string &name) {
    auto s = this->types.find(name);
    if (s != this->types.end()) return s->second;
    if (this->parent != nullptr) {
        auto st = this->parent->FindStruct(name);
        if (st.lock()) return st;
    }
    return std::weak_ptr<StructDeclaration>();
}

void SymbolTable::addUngenerated(std::weak_ptr<StructDeclaration> t) {
    this->ungenerated_types.emplace(t.lock()->GetName(), t);
}

void SymbolTable::addUngenerated(FunctionDeclaration *f) {
    auto it = ungenerated_functions.find(f->GetName());
    if (it == ungenerated_functions.end()) {
        std::vector<FunctionDeclaration *> v;
        v.push_back(f);
        ungenerated_functions.emplace(f->GetName(), v);
    } else {
        it->second.push_back(f);
    }
}

FunctionDeclaration *SymbolTable::FindUngeneratedFunction(const std::string &name,
                                                          const std::vector<std::weak_ptr<Type>> &argTypes) {
    auto functions = this->ungenerated_functions.find(name);
    if (functions != this->ungenerated_functions.end()) {
        for (auto func : functions->second) {
            if (func->accepts(*gen, argTypes)) {
                return func;
            }
        }
    }
    if (this->parent != nullptr) {
        auto var = this->parent->FindUngeneratedFunction(name, argTypes);
        if (var != nullptr) return var;
    }
    return nullptr;
}

FunctionDeclaration *SymbolTable::FindUngeneratedFunctionInCurrentScope(const std::string &name,
                                                                        const std::vector<std::weak_ptr<Type>> &argTypes) {
    auto functions = this->ungenerated_functions.find(name);
    if (functions == this->ungenerated_functions.end()) return nullptr;

    for (auto func : functions->second) {
        if (func->accepts(*gen, argTypes)) {
            return func;
        }
    }
    return nullptr;
}

std::weak_ptr<StructDeclaration> SymbolTable::FindUngeneratedStruct(const std::string &name) {
    auto s = this->ungenerated_types.find(name);
    if (s != this->ungenerated_types.end()) return s->second;
    if (this->parent != nullptr) {
        auto st = this->parent->FindUngeneratedStruct(name);
        if (st.lock()) return st;
    }
    return std::weak_ptr<StructDeclaration>();
}

std::weak_ptr<StructDeclaration> SymbolTable::FindUngeneratedStructInCurrentScope(const std::string &name) {
    auto s = this->ungenerated_types.find(name);
    if (s != this->ungenerated_types.end()) return s->second;
    return std::weak_ptr<StructDeclaration>();
}

void SymbolTable::GenerateFunction(FunctionDeclaration *f) {
    auto s = this->ungenerated_functions.find(f->GetName());
    if (s != this->ungenerated_functions.end()) {
        this->addFunction(f);
        s->second.erase(std::remove(s->second.begin(), s->second.end(), f), s->second.end());
        if (s->second.empty()) {
            this->ungenerated_functions.erase(s);
        }
    } else if (this->parent != nullptr) {
        this->parent->GenerateFunction(f);
    } else {
        throw std::logic_error("Unknown function registered");
    }
}

void SymbolTable::GenerateStruct(const std::string &name) {
    auto s = this->ungenerated_types.find(name);
    if (s != this->ungenerated_types.end()) {
        this->addStruct(s->second);
        this->ungenerated_types.erase(s);
    } else if (this->parent != nullptr) {
        this->parent->GenerateStruct(name);
    } else {
        throw std::logic_error("Unknown function registered");
    }
}
