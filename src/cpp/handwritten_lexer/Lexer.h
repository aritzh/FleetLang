#pragma once

#include <istream>
#include "FleetToken.h"

class Lexer {
public:
    Lexer(const std::istream &is, std::string file_path);

    FleetToken next();

private:
    const std::istream &is;
    std::string currentFilePath;
    int currentLine = 0;
    int currentCol = 0;
};


