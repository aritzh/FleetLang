#pragma once

#include <string>
#include "FleetTokenType.h"

class FleetToken {
    FleetTokenType type;
    std::string value;
    std::string file_path;
    int startLine;
    int endLine;
    int startCol;
    int endCol;
};


