#pragma once

#include <string>

struct Settings {
    bool verbose = false;
    bool print_llvm = false;
    bool release = false;
    bool single_file = false;
    bool only_compile = false;
    std::string output_filename = "";
    std::string input_filename = "";
    std::string stdlib_path = "";
    std::string target_triple = "";
};

extern Settings settings;
