// Created by aritz.

#include <FleetLangLexer.h>
#include "Visitor.h"
#include "AST/StmtAST.h"
#include "AST/ExprAST.h"

Position Visitor::posFromToken(antlr4::Token *token) const {
    return {fileName, directory, token->getLine(), token->getCharPositionInLine()};
}

antlrcpp::Any Visitor::visitProgram(FleetLangParser::ProgramContext *ctx, std::string fileName, std::string directory) {
    this->fileName = std::move(fileName);
    this->directory = std::move(directory);
    for (auto f : ctx->tlds) {
        std::shared_ptr<TopLevelDecl> tld = std::shared_ptr<TopLevelDecl>(this->visit(f).as<TopLevelDecl *>());
        if (auto strct = std::dynamic_pointer_cast<StructDeclaration>(tld)) {
            this->types.push_back(strct);
        }
        this->p.add(tld);
    }
    return antlrcpp::Any();
}

antlrcpp::Any Visitor::visitAssignStmt(FleetLangParser::AssignStmtContext *ctx) {
    return this->visit(ctx->assign);
}

antlrcpp::Any Visitor::visitIfStmt(FleetLangParser::IfStmtContext *ctx) {
    ExprAST *cond = this->visit(ctx->cond).as<ExprAST *>();
    StmtAST *tru = ctx->whenTrue != nullptr ? this->visit(ctx->whenTrue).as<StmtAST *>() : nullptr;
    StmtAST *fals = ctx->whenFalse != nullptr ? this->visit(ctx->whenFalse).as<StmtAST *>() : nullptr;

    return static_cast<StmtAST *>(new IfStmtAST(posFromToken(ctx->start),
                                                std::unique_ptr<ExprAST>(cond),
                                                std::unique_ptr<StmtAST>(tru),
                                                std::unique_ptr<StmtAST>(fals)));
}

antlrcpp::Any Visitor::visitWhileStmt(FleetLangParser::WhileStmtContext *ctx) {
    ExprAST *cond = this->visit(ctx->cond).as<ExprAST *>();
    StmtAST *body = ctx->whenTrue != nullptr ? this->visit(ctx->whenTrue).as<StmtAST *>() : nullptr;
    return static_cast<StmtAST *>(new WhileStmtAST(posFromToken(ctx->start),
                                                   std::unique_ptr<ExprAST>(cond),
                                                   std::unique_ptr<StmtAST>(body)));
}

antlrcpp::Any Visitor::visitReturnStmt(FleetLangParser::ReturnStmtContext *ctx) {
    ExprAST *expr = ctx->expr != nullptr ? this->visit(ctx->expr).as<ExprAST *>() : nullptr;
    return static_cast<StmtAST *>(new ReturnStmtAST(posFromToken(ctx->start), std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitBlockStmt(FleetLangParser::BlockStmtContext *ctx) {
    std::vector<std::unique_ptr<StmtAST>> stmts;
    for (auto s : ctx->stmts) {
        if (s != nullptr) {
            stmts.push_back(std::unique_ptr<StmtAST>(this->visit(s).as<StmtAST *>()));
        }
    }
    return static_cast<StmtAST *>(new BlockStmtAST(posFromToken(ctx->start), std::move(stmts)));
}

antlrcpp::Any Visitor::visitDeclStmt(FleetLangParser::DeclStmtContext *ctx) {
    StmtAST *stmt = this->visit(ctx->declaration()).as<StmtAST *>();
    auto *decl = dynamic_cast<VarDeclStmtAST *>(stmt);
    if (ctx->sttc) decl->SetStatic(true);
    return static_cast<StmtAST *>(decl);
}

antlrcpp::Any Visitor::visitExpressionStmt(FleetLangParser::ExpressionStmtContext *ctx) {
    ExprAST *expr = this->visit(ctx->expr).as<ExprAST *>();
    return static_cast<StmtAST *>(new ExprStmtAST(posFromToken(ctx->start), std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitIntExpr(FleetLangParser::IntExprContext *ctx) {
    std::string valueAndExtras = ctx->getText();
    uint8_t base = 10;
    unsigned int bitCount = 0;
    bool sign = true;

    size_t startPos = 0;
    size_t length = valueAndExtras.size();

    if (valueAndExtras.size() >= 2) {
        if (valueAndExtras[0] == '0') {
            if (std::isdigit(valueAndExtras[1])) {
                startPos = 1;
                base = 8;
            } else if (valueAndExtras[1] == 'x' || valueAndExtras[1] == 'X') {
                startPos = 2;
                base = 16;
            } else if (valueAndExtras[1] == 'b' || valueAndExtras[1] == 'B') {
                startPos = 2;
                base = 2;
            } else if (valueAndExtras[1] == 'o' || valueAndExtras[1] == 'O') {
                startPos = 2;
                base = 8;
            } else if (valueAndExtras[1] == 'i' || valueAndExtras[1] == 'u') {
                // Do nothing, just a 0uXXX or 0iXXX
            } else {
                throw std::logic_error("Unknown base");
            }
        }
    }

    size_t pos;
    if ((pos = valueAndExtras.find('u')) != std::string::npos) {
        sign = false;
        if (pos < valueAndExtras.size() - 1) {
            std::string bitCountStr = valueAndExtras.substr(pos);
            bitCount = static_cast<unsigned int>(std::stoi(bitCountStr));
            length -= bitCountStr.size() + 1;
        } else {
            length--;
        }

    } else if ((pos = valueAndExtras.find('i')) != std::string::npos) {
        sign = true;
        if (pos < valueAndExtras.size() - 1) {
            std::string bitCountStr = valueAndExtras.substr(pos + 1);
            bitCount = static_cast<unsigned int>(std::stoi(bitCountStr));
            length -= bitCountStr.size() + 1;
        } else {
            length--;
        }
    }

    return static_cast<ExprAST *>(new IntExprAST(posFromToken(ctx->start),
                                                 valueAndExtras.substr(startPos, length - startPos),
                                                 bitCount,
                                                 base,
                                                 sign));
}

antlrcpp::Any Visitor::visitFunctionCallExpr(FleetLangParser::FunctionCallExprContext *ctx) {
    std::string functionName = ctx->func->getText();
    std::vector<std::unique_ptr<ExprAST>> args;

    for (auto a : ctx->args) {
        auto it = this->visit(a);
        args.push_back(std::unique_ptr<ExprAST>(it.as<ExprAST *>()));
    }

    bool isIntrinsic = ctx->func->getType() == FleetLangLexer::INTRINSIC_FUNC_ID;
    if (isIntrinsic) {
        functionName = functionName.substr(1);
    }

    return static_cast<ExprAST *>(new CallExprAST(posFromToken(ctx->start), functionName, std::move(args),
                                                  isIntrinsic));
}

antlrcpp::Any Visitor::visitOpExpr(FleetLangParser::OpExprContext *ctx) {
    ExprAST *lhs = this->visit(ctx->left).as<ExprAST *>();
    ExprAST *rhs = this->visit(ctx->right).as<ExprAST *>();

    BinaryOp op;
    switch (ctx->op->getType()) {
        case FleetLangLexer::ADD:
            op = BinaryOp::Add;
            break;
        case FleetLangLexer::SUBTRACT:
            op = BinaryOp::Sub;
            break;
        case FleetLangLexer::MULTIPLY:
            op = BinaryOp::Mul;
            break;
        case FleetLangLexer::DIVIDE:
            op = BinaryOp::Div;
            break;
        case FleetLangLexer::LEFT_SHIFT:
            op = BinaryOp::LeftShift;
            break;
        case FleetLangLexer::ARITHMETIC_RIGHT_SHIFT:
            op = BinaryOp::ArithmeticRightShift;
            break;
        case FleetLangLexer::LOGICAL_RIGHT_SHIFT:
            op = BinaryOp::LogicalRightShift;
            break;
        case FleetLangLexer::ADDRESS: // Bit-and
            op = BinaryOp::BitAnd;
            break;
        case FleetLangLexer::BIT_OR:
            op = BinaryOp::BitOr;
            break;
        case FleetLangLexer::BIT_XOR:
            op = BinaryOp::BitXor;
            break;
        case FleetLangLexer::MODULO:
            op = BinaryOp::Modulo;
            break;
        default:
            // TODO ERROR
            throw std::logic_error("Unknown token '" + ctx->op->getText() + "'");
    }

    return static_cast<ExprAST *>(new BinaryExprAST(posFromToken(ctx->start),
                                                    op,
                                                    std::unique_ptr<ExprAST>(lhs),
                                                    std::unique_ptr<ExprAST>(rhs)));
}

antlrcpp::Any Visitor::visitLiteralBool(FleetLangParser::LiteralBoolContext *ctx) {
    return static_cast<ExprAST *>(new LiteralBoolExprAST(posFromToken(ctx->start),
                                                         ctx->atom->getType() == FleetLangLexer::TRUE));
}

antlrcpp::Any Visitor::visitComparisonOp(FleetLangParser::ComparisonOpContext *ctx) {
    ExprAST *lhs = this->visit(ctx->left).as<ExprAST *>();
    ExprAST *rhs = this->visit(ctx->right).as<ExprAST *>();

    ComparisonOp op;
    switch (ctx->op->getType()) {
        case FleetLangLexer::LESS_THAN:
            op = ComparisonOp::LT;
            break;
        case FleetLangLexer::LESS_EQUAL:
            op = ComparisonOp::LE;
            break;
        case FleetLangLexer::GREATER_THAN:
            op = ComparisonOp::GT;
            break;
        case FleetLangLexer::GREATER_EQUAL:
            op = ComparisonOp::GE;
            break;
        case FleetLangLexer::EQUALS:
            op = ComparisonOp::EQ;
            break;
        case FleetLangLexer::NOT_EQUALS:
            op = ComparisonOp::NE;
            break;
        default:
            // TODO ERROR
            throw std::logic_error("Unknown token '" + ctx->op->getText() + "'");
    }

    return static_cast<ExprAST *>(new ComparisonExprAST(posFromToken(ctx->start),
                                                        op,
                                                        std::unique_ptr<ExprAST>(lhs),
                                                        std::unique_ptr<ExprAST>(rhs)));
}

antlrcpp::Any Visitor::visitStringExpr(FleetLangParser::StringExprContext *ctx) {
    return static_cast<ExprAST *>(new StringExprAST(posFromToken(ctx->start),
                                                    ctx->atom->getText().substr(1, ctx->atom->getText().size() - 2)));
}

antlrcpp::Any Visitor::visitFloatExpr(FleetLangParser::FloatExprContext *ctx) {
    float value = std::stof(ctx->atom->getText());
    return static_cast<ExprAST *>(new FloatExprAST(posFromToken(ctx->start), value));
}

antlrcpp::Any Visitor::visitSignedExpression(FleetLangParser::SignedExpressionContext *ctx) {
    UnaryOp op = ctx->sign->getType() == FleetLangLexer::ADD ? UnaryOp::Pos : UnaryOp::Neg;
    ExprAST *expr = this->visit(ctx->expr).as<ExprAST *>();
    return static_cast<ExprAST *>(new UnaryExprAST(posFromToken(ctx->start), op, std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitBooleanOp(FleetLangParser::BooleanOpContext *ctx) {
    BoolOp op;
    switch (ctx->op->getType()) {
        case FleetLangLexer::BOOL_AND:
            op = BoolOp::AND;
            break;
        case FleetLangLexer::BOOL_OR:
            op = BoolOp::OR;
            break;
        default:
            // TODO ERROR
            throw std::logic_error("Unknown token '" + ctx->op->getText() + "'");
    }

    ExprAST *lhs = this->visit(ctx->left).as<ExprAST *>();
    ExprAST *rhs = this->visit(ctx->right).as<ExprAST *>();

    return static_cast<ExprAST *>(new BoolOpExprAST(posFromToken(ctx->start),
                                                    op,
                                                    std::unique_ptr<ExprAST>(lhs),
                                                    std::unique_ptr<ExprAST>(rhs)));
}

antlrcpp::Any Visitor::visitIdExpr(FleetLangParser::IdExprContext *ctx) {
    return static_cast<ExprAST *>(new VariableExprAST(posFromToken(ctx->start), ctx->atom->getText()));
}

antlrcpp::Any Visitor::visitFunction(FleetLangParser::FunctionContext *ctx) {

    std::vector<std::unique_ptr<VarDeclStmtAST>> args;

    for (unsigned int i = 0; i < ctx->argNames.size(); i++) {
        Type *realType = this->visit(ctx->args[i]).as<Type *>();
        assert(realType);
        this->types.emplace_back(realType);
        std::shared_ptr<Type> shRealType = this->types.back();
        // TODO Change when composite types are allowed as arguments
        args.push_back(std::make_unique<VarDeclStmtAST>(posFromToken(ctx->start),
                                                        ctx->argNames[i]->getText(),
                                                        shRealType, std::unique_ptr<ExprAST>()));
    }

    std::vector<std::unique_ptr<Annotation>> annotations;
    for (auto arg : ctx->annotations) {
        Annotation *a = this->visit(arg).as<Annotation *>();
        annotations.emplace_back(a);
    }

    std::string functionName = ctx->name->getText();
    FunctionType functionType = FunctionType::Normal;
    if (ctx->name->getType() == FleetLangLexer::FOREIGN_FUNC_ID) {
        functionType = FunctionType::Foreign;
        functionName = functionName.substr(1);
    }


    Type *retType;
    if (ctx->retType != nullptr) retType = this->visit(ctx->retType).as<Type *>();
    else retType = this->visit(ctx->voidTy).as<Type *>();
    assert(retType);
    this->types.emplace_back(retType);
    std::shared_ptr<Type> shRetType = this->types.back();

    bool isPublic = ctx->pub != nullptr;

    if (ctx->stmts.empty()) {
        return static_cast<TopLevelDecl *>(new FunctionDeclaration(posFromToken(ctx->start),
                                                                   shRetType,
                                                                   functionName,
                                                                   std::move(args),
                                                                   std::move(annotations),
                                                                   isPublic,
                                                                   functionType));
    }

    std::vector<std::unique_ptr<StmtAST>> stmts;

    for (auto s : ctx->stmts) {
        StmtAST *p = this->visit(s).as<StmtAST *>();
        if (p != nullptr) stmts.push_back(std::unique_ptr<StmtAST>(p));
    }
    return static_cast<TopLevelDecl *>(new Function(posFromToken(ctx->start),
                                                    shRetType,
                                                    functionName,
                                                    std::move(args),
                                                    std::move(stmts),
                                                    std::move(annotations),
                                                    isPublic,
                                                    functionType));
}

antlrcpp::Any Visitor::visitEmptyStmt(FleetLangParser::EmptyStmtContext *ctx) {
    return static_cast<StmtAST *>(nullptr);
}

antlrcpp::Any Visitor::visitDoubleExpr(FleetLangParser::DoubleExprContext *ctx) {
    double value = std::stod(ctx->atom->getText());
    return static_cast<ExprAST *>(new DoubleExprAST(posFromToken(ctx->start), value));
}

antlrcpp::Any Visitor::visitCustomBaseType(FleetLangParser::CustomBaseTypeContext *ctx) {
    return static_cast<Type *>(new NamedType(posFromToken(ctx->start), ctx->name->getText()));
}

antlrcpp::Any Visitor::visitPrimitiveBaseType(FleetLangParser::PrimitiveBaseTypeContext *ctx) {
    return this->visit(ctx->name);
}

antlrcpp::Any Visitor::visitArrayExpr(FleetLangParser::ArrayExprContext *ctx) {
    std::unique_ptr<ExprAST> idx(this->visit(ctx->idx).as<ExprAST *>());
    std::unique_ptr<ExprAST> var(this->visit(ctx->var).as<ExprAST *>());

    return static_cast<ExprAST *>(new ArrayExprAST(posFromToken(ctx->start),
                                                   std::move(var),
                                                   std::move(idx)));
}

PrimitiveType *parseSizedIntType(const std::string &name) {
    if (name.size() < 2 || name.size() > 3) throw std::logic_error(fmt::format("Unknown integer type '{}'", name));
    bool sign = name[0] == 'i';
    int size = std::stoi(name.substr(1));
    EPrimitiveType type;
    if (sign) {
        switch (size) {
            case 8:
                type = EPrimitiveType::i8;
                break;
            case 16:
                type = EPrimitiveType::i16;
                break;
            case 32:
                type = EPrimitiveType::i32;
                break;
            case 64:
                type = EPrimitiveType::i64;
                break;
            case 128:
                type = EPrimitiveType::i128;
                break;
            default:
                throw std::logic_error(fmt::format("Unknown signed integer type of size {}", size));
        }
    } else {
        switch (size) {
            case 8:
                type = EPrimitiveType::u8;
                break;
            case 16:
                type = EPrimitiveType::u16;
                break;
            case 32:
                type = EPrimitiveType::u32;
                break;
            case 64:
                type = EPrimitiveType::u64;
                break;
            case 128:
                type = EPrimitiveType::u128;
                break;
            default:
                throw std::logic_error(fmt::format("Unknown signed integer type of size {}", size));
        }
    }
    return new PrimitiveType(type);
}

antlrcpp::Any Visitor::visitPrimitiveType(FleetLangParser::PrimitiveTypeContext *ctx) {
    switch (ctx->start->getType()) {
        case FleetLangLexer::FLOAT:
            return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Float));
        case FleetLangLexer::INT:
            return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Int));
        case FleetLangLexer::UINT:
            return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Uint));
        case FleetLangLexer::DOUBLE:
            return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Double));
        case FleetLangLexer::BOOLEAN:
            return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Bool));
        case FleetLangLexer::SIZED_INT_TYPE:
            return static_cast<Type *>(parseSizedIntType(ctx->start->getText()));
        default:
            throw std::logic_error("Unknown token '" + ctx->start->getText() + "'");
    }
}

antlrcpp::Any Visitor::visitVoidType(FleetLangParser::VoidTypeContext *ctx) {
    return static_cast<Type *>(new PrimitiveType(EPrimitiveType::Void));
}

antlrcpp::Any Visitor::visitParenExpr(FleetLangParser::ParenExprContext *ctx) {
    return this->visit(ctx->expr);
}

antlrcpp::Any Visitor::visitForStmt(FleetLangParser::ForStmtContext *ctx) {
    StmtAST *init = nullptr;
    ExprAST *cond = nullptr;
    StmtAST *update = nullptr;
    StmtAST *body = nullptr;

    if (ctx->init != nullptr) init = this->visit(ctx->init).as<StmtAST *>();
    if (ctx->cond != nullptr) cond = this->visit(ctx->cond).as<ExprAST *>();
    if (ctx->update != nullptr) update = this->visit(ctx->update).as<StmtAST *>();
    if (ctx->body != nullptr) body = this->visit(ctx->body).as<StmtAST *>();

    return static_cast<StmtAST *>(new ForStmtAST(posFromToken(ctx->start),
                                                 std::unique_ptr<StmtAST>(init),
                                                 std::unique_ptr<ExprAST>(cond),
                                                 std::unique_ptr<StmtAST>(update),
                                                 std::unique_ptr<StmtAST>(body)));
}

antlrcpp::Any Visitor::visitDoWhileStmt(FleetLangParser::DoWhileStmtContext *ctx) {
    ExprAST *cond = this->visit(ctx->cond).as<ExprAST *>();
    StmtAST *body = nullptr;
    if (ctx->body != nullptr) body = this->visit(ctx->body).as<StmtAST *>();

    return static_cast<StmtAST *>(new DoWhileStmtAST(posFromToken(ctx->start),
                                                     std::unique_ptr<ExprAST>(cond),
                                                     std::unique_ptr<StmtAST>(body)));
}

antlrcpp::Any Visitor::visitTopLevelDecl(FleetLangParser::TopLevelDeclContext *ctx) {
    if (ctx->function() != nullptr) {
        return this->visit(ctx->function()).as<TopLevelDecl *>();
    } else if (ctx->structDecl() != nullptr) {
        return this->visit(ctx->structDecl()).as<TopLevelDecl *>();
    } else if (ctx->declaration() != nullptr) {
        VarDeclStmtAST *var = static_cast<VarDeclStmtAST *>(this->visit(ctx->declaration()).as<StmtAST *>());
        var->SetGlobal(true);
        return static_cast<TopLevelDecl *>(var);
    } else {
        throw std::logic_error("Unknown top level declaration type");
    }
}

antlrcpp::Any Visitor::visitStructDecl(FleetLangParser::StructDeclContext *ctx) {
    const std::string &type_name = ctx->name->getText();

    std::vector<std::unique_ptr<VarDeclStmtAST>> fields;
    std::vector<std::unique_ptr<Annotation>> annotations;

    for (auto field_ctx : ctx->fields) {
        fields.push_back(std::unique_ptr<VarDeclStmtAST>(
                dynamic_cast<VarDeclStmtAST *>(this->visit(field_ctx).as<StmtAST *>())));
    }

    for (auto ann : ctx->annotations) {
        annotations.emplace_back(this->visit(ann).as<Annotation *>());
    }

    return static_cast<TopLevelDecl *>(new StructDeclaration(posFromToken(ctx->start),
                                                             type_name,
                                                             std::move(fields),
                                                             std::move(annotations)));
}

antlrcpp::Any Visitor::visitDeclaration(FleetLangParser::DeclarationContext *ctx) {
    std::string name = ctx->name->getText();

    Type *type = this->visit(ctx->baseType).as<Type *>();
    assert(type);

    this->types.emplace_back(type);
    std::shared_ptr<Type> shType = this->types.back();

    ExprAST *expr = nullptr;
    if (ctx->expr) {
        expr = this->visit(ctx->expr).as<ExprAST *>();
    }

    return static_cast<StmtAST *>(new VarDeclStmtAST(posFromToken(ctx->start),
                                                     name,
                                                     shType,
                                                     std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitMemberAccess(FleetLangParser::MemberAccessContext *ctx) {
    return static_cast<ExprAST *>(new MemberAccessAST(posFromToken(ctx->start),
                                                      std::unique_ptr<ExprAST>(this->visit(ctx->expr).as<ExprAST *>()),
                                                      ctx->member->getText()));
}

antlrcpp::Any Visitor::visitCastExpr(FleetLangParser::CastExprContext *ctx) {
    ExprAST *value = this->visit(ctx->expr).as<ExprAST *>();
    Type *type = this->visit(ctx->ty).as<Type *>();
    assert(type);
    this->types.emplace_back(type);
    std::shared_ptr<Type> shType = this->types.back();
    return static_cast<ExprAST *>(new CastingExprAST(posFromToken(ctx->start),
                                                     std::unique_ptr<ExprAST>(value),
                                                     shType));
}

antlrcpp::Any Visitor::visitDereferenceExpr(FleetLangParser::DereferenceExprContext *ctx) {
    ExprAST *expr = this->visit(ctx->expr).as<ExprAST *>();
    return static_cast<ExprAST *>(new DereferenceExprAST(posFromToken(ctx->start), std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitAddressExpr(FleetLangParser::AddressExprContext *ctx) {
    ExprAST *expr = this->visit(ctx->expr).as<ExprAST *>();
    return static_cast<ExprAST *>(new AddressOfExprAST(posFromToken(ctx->start), std::unique_ptr<ExprAST>(expr)));
}

antlrcpp::Any Visitor::visitPointerToType(FleetLangParser::PointerToTypeContext *ctx) {
    Type *type = this->visit(ctx->ty).as<Type *>();
    assert(type);
    this->types.emplace_back(type);
    std::shared_ptr<Type> shType = this->types.back();
    return static_cast<Type *>(new PointerToType(shType));
}

Visitor::Visitor(Module &p)
        : p(p) {}

antlrcpp::Any Visitor::visitAnnotation(FleetLangParser::AnnotationContext *ctx) {
    std::string name = ctx->name->getText().substr(1); // To remove the '@' sign
    std::vector<std::string> args;
    for (auto arg : ctx->args) {
        args.push_back(arg->getText());
    }
    return new Annotation(std::move(name), std::move(args));
}

antlrcpp::Any Visitor::visitAsmExpr(FleetLangParser::AsmExprContext *ctx) {

    std::vector<AsmExprPart> outputs;
    std::vector<AsmExprPart> inputs;
    std::vector<std::string> clobbers;

    for (auto it : ctx->outputs) {
        AsmExprPart *part = this->visit(it).as<AsmExprPart *>();
        outputs.push_back(std::move(*part));
        delete part;
    }

    for (auto it : ctx->inputs) {
        AsmExprPart *part = this->visit(it).as<AsmExprPart *>();
        inputs.push_back(std::move(*part));
        delete part;
    }

    for (auto it : ctx->clobberStr) {
        clobbers.push_back(it->getText().substr(1, it->getText().size() - 2));
    }

    return static_cast<ExprAST *>(new AsmExprAST(posFromToken(ctx->start),
                                                 ctx->asmStr->getText().substr(1, ctx->asmStr->getText().size() - 2),
                                                 std::move(outputs),
                                                 std::move(inputs),
                                                 std::move(clobbers)));
}

antlrcpp::Any Visitor::visitAsmExprPart(FleetLangParser::AsmExprPartContext *ctx) {
    return new AsmExprPart(posFromToken(ctx->start),
                           ctx->str->getText().substr(1, ctx->str->getText().size() - 2),
                           std::unique_ptr<ExprAST>(this->visit(ctx->var).as<ExprAST *>()));
}

antlrcpp::Any Visitor::visitAssignmentOperator(FleetLangParser::AssignmentOperatorContext *ctx) {
    switch (ctx->op->getType()) {
        case FleetLangLexer::ASSIGN:
            return AssignmentOp::ASSIGN;
        case FleetLangLexer::ADD_ASSIGN:
            return AssignmentOp::ADD_ASSIGN;
        case FleetLangLexer::SUB_ASSIGN:
            return AssignmentOp::SUB_ASSIGN;
        case FleetLangLexer::MUL_ASSIGN:
            return AssignmentOp::MUL_ASSIGN;
        case FleetLangLexer::DIV_ASSIGN:
            return AssignmentOp::DIV_ASSIGN;
        case FleetLangLexer::MODULO_ASSIGN:
            return AssignmentOp::MODULO_ASSIGN;
        case FleetLangLexer::BOOL_AND_ASSIGN:
            return AssignmentOp::BOOL_AND_ASSIGN;
        case FleetLangLexer::BOOL_OR_ASSIGN:
            return AssignmentOp::BOOL_OR_ASSIGN;
        case FleetLangLexer::BIT_AND_ASSIGN:
            return AssignmentOp::BIT_AND_ASSIGN;
        case FleetLangLexer::BIT_OR_ASSIGN:
            return AssignmentOp::BIT_OR_ASSIGN;
        case FleetLangLexer::BIT_XOR_ASSIGN:
            return AssignmentOp::BIT_XOR_ASSIGN;
    }
    throw std::logic_error("Unknown assignment operator");
}

antlrcpp::Any Visitor::visitCharExpr(FleetLangParser::CharExprContext *ctx) {
    std::string all = ctx->getText();
    all = all.substr(1, all.length() - 2);
    if (all == "\\n") {
        all = "\n";
    }
    // TODO Handle unicode chars
    return static_cast<ExprAST *>(new CharExprAST(posFromToken(ctx->start), static_cast<uint32_t>(all[0])));
}

antlrcpp::Any Visitor::visitSizeofExpr(FleetLangParser::SizeofExprContext *ctx) {
    Type *type = this->visit(ctx->type()).as<Type *>();
    assert(type);
    this->types.emplace_back(type);
    std::shared_ptr<Type> shType = this->types.back();
    return static_cast<ExprAST *>(new SizeofExprAST(posFromToken(ctx->start), shType));
}

antlrcpp::Any Visitor::visitNotExpression(FleetLangParser::NotExpressionContext *ctx) {
    return static_cast<ExprAST *>(new UnaryExprAST(posFromToken(ctx->start),
                                                   UnaryOp::Not,
                                                   std::unique_ptr<ExprAST>(this->visit(ctx->expr).as<ExprAST *>())));
}

antlrcpp::Any Visitor::visitArrayType(FleetLangParser::ArrayTypeContext *ctx) {
    Type *baseType = this->visit(ctx->base).as<Type *>();
    this->types.emplace_back(baseType);
    std::shared_ptr<Type> shType = this->types.back();

    unsigned long size = std::stoul(ctx->arrayIdxs->getText());

    return static_cast<Type *>(new ArrayType(shType, size));
}

antlrcpp::Any Visitor::visitNormalAssignment(FleetLangParser::NormalAssignmentContext *ctx) {
    ExprAST *left = this->visit(ctx->left).as<ExprAST *>();
    ExprAST *right = this->visit(ctx->right).as<ExprAST *>();
    AssignmentOp op = this->visit(ctx->op).as<AssignmentOp>();


    ExprAST *left2;
    ExprAST *sum;

    switch (op) {
        case AssignmentOp::ASSIGN:
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(right),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::ADD_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::Add,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::SUB_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::Sub,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::MUL_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::Mul,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::DIV_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::Div,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::MODULO_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::Modulo,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::BOOL_AND_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BoolOpExprAST(posFromToken(ctx->start),
                                    BoolOp::AND,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::BOOL_OR_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BoolOpExprAST(posFromToken(ctx->start),
                                    BoolOp::OR,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::BIT_AND_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::BitAnd,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::BIT_OR_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::BitOr,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        case AssignmentOp::BIT_XOR_ASSIGN:
            left2 = this->visit(ctx->left).as<ExprAST *>();
            sum = new BinaryExprAST(posFromToken(ctx->start),
                                    BinaryOp::BitXor,
                                    std::unique_ptr<ExprAST>(left2),
                                    std::unique_ptr<ExprAST>(right));
            return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                            std::unique_ptr<ExprAST>(left),
                                                            std::unique_ptr<ExprAST>(sum),
                                                            AssignmentOp::ASSIGN));
        default:
            throw std::logic_error(fmt::format("Assignment operator '{}' not implemented", ctx->op->getText()));
    }
}

antlrcpp::Any Visitor::visitIncDecAssignment(FleetLangParser::IncDecAssignmentContext *ctx) {
    ExprAST *left = this->visit(ctx->left).as<ExprAST *>();

    ExprAST *left2;
    ExprAST *sum;
    left2 = this->visit(ctx->left).as<ExprAST *>();
    sum = new BinaryExprAST(posFromToken(ctx->start),
                            ctx->op->getType() == FleetLangLexer::INCREMENT ? BinaryOp::Add : BinaryOp::Sub,
                            std::unique_ptr<ExprAST>(left2),
                            std::unique_ptr<ExprAST>(new IntExprAST(posFromToken(ctx->start), "1", 0, 10, true)));
    return static_cast<StmtAST *>(new AssignStmtAST(posFromToken(ctx->start),
                                                    std::unique_ptr<ExprAST>(left),
                                                    std::unique_ptr<ExprAST>(sum),
                                                    AssignmentOp::ASSIGN));

}

uint32_t parseCharEscape(const std::string &chr) {
    auto first = static_cast<uint32_t>(chr[0]);
    if (chr.length() == 1) return first;
    if (first != '\\') {
        throw std::logic_error(fmt::format("Invalid escape sequence '{}'. First char is not a backslash (\\)"));
    }
//    uint32_t value = static_cast<uint32_t>(chr[1] << 8 | chr[2]);


    return 0;
}

bool isMultiByteChar(const std::string &chr) {
    return !chr.empty() && (static_cast<uint8_t>(chr[0]) & 0b11000000) != 0;
}
