#pragma once

#include <unordered_map>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/DIBuilder.h>
#include <llvm/IR/Module.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Support/raw_ostream.h>
#include <unordered_set>
#include "AST/ASTfwd.h"
#include <spdlog/spdlog.h>

class SymbolTable;

class Module;

namespace spd = spdlog;

struct Target {
    std::string arch;
    std::string vendor;
    std::string os;
    std::string environment;
};

class CodeGen {
private:
    SymbolTable *table;
    bool successful;
    std::string currentFunction;
    std::unique_ptr<llvm::TargetMachine> TheTargetMachine;

    Target target;
    std::unordered_set<FunctionDeclaration *> toGenerate;

    std::vector<std::shared_ptr<Type>> types;

public:
    llvm::LLVMContext *TheContext;
    llvm::Module *TheModule;
    llvm::IRBuilder<> Builder;
    llvm::DIBuilder *DB = nullptr;
    std::shared_ptr<spd::logger> log;
    llvm::DICompileUnit *compileUnit;

    CodeGen();

    ~CodeGen();

    bool wasSuccessful() const;

    void Generate(const Module &p);

    void WriteOBJ(std::string Filename);

    void printLLVM_IR();

    void linkFile(const std::string &objPath, const std::string &output, bool removeObj = true);

    llvm::Value *InsertSymbol(VarDeclStmtAST *var);

    llvm::Value *InsertSymbol(const std::string &name, VarDeclStmtAST *var);

    llvm::Function *InsertSymbol(FunctionDeclaration *func);

    llvm::StructType *InsertSymbol(const std::weak_ptr<StructDeclaration> &type);

    void InsertUngenerated(FunctionDeclaration *func);

    void InsertUngenerated(std::weak_ptr<StructDeclaration> type);

    VarDeclStmtAST *FindVariable(const std::string &name);

    FunctionDeclaration *FindFunction(const std::string &name, const std::vector<std::weak_ptr<Type>> &types);

    FunctionDeclaration *FindDeclaredFunction(const std::string &name, const std::vector<std::weak_ptr<Type>> &types);

    std::weak_ptr<StructDeclaration> FindStruct(const std::string &name);

    llvm::AllocaInst *CreateAlloca(Type *type, std::string name);

    std::weak_ptr<StructDeclaration> FindUngeneratedStruct(const std::string &name);

    std::weak_ptr<Type> ResolveType(const std::weak_ptr<Type> &type);

    std::weak_ptr<Type> addType(std::shared_ptr<Type> type);

    void PushScope();

    void PopScope();

    unsigned int GetPointerSize();

    const Target &GetTarget() const;

    const std::string &GetCurrentFunction() const;

    llvm::ConstantInt *CreateLiteralInt(uint64_t value, PrimitiveType &type);
    llvm::ConstantFP *CreateLiteralFloat(float value);
    llvm::ConstantFP *CreateLiteralDouble(double value);
    llvm::ConstantInt *CreateLiteralBool(bool value);

    template<typename Arg1, typename... Args>
    inline void error(Position pos, const char *fmt, const Arg1 &arg1, const Args &... args) {
        successful = false;
        this->log->error(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, fmt).c_str(), arg1, args...);
        (void(0)); // To put a breakpoint
    }

    template<typename T>
    inline void error(Position pos, const T &msg) {
        successful = false;
        this->log->error(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, msg).c_str());
        (void(0)); // To put a breakpoint
    }

    template<typename Arg1, typename... Args>
    inline void info(Position pos, const char *fmt, const Arg1 &arg1, const Args &... args) {
        this->log->info(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, fmt).c_str(), arg1, args...);
    }

    template<typename T>
    inline void info(Position pos, const T &msg) {
        this->log->info(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, msg).c_str());
    }

    template<typename Arg1, typename... Args>
    inline void debug(Position pos, const char *fmt, const Arg1 &arg1, const Args &... args) {
        this->log->debug(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, fmt).c_str(), arg1, args...);
    }

    template<typename T>
    inline void debug(Position pos, const T &msg) {
        this->log->debug(fmt::format("[{}:{}:{}] {}", pos.fileName, pos.line, pos.col, msg).c_str());
    }

private:

    void RunPasses();

};

//class MyOStream: public llvm::raw_ostream {
//private:
//    size_t pos = 0;
//public:
//    std::stringstream ss;
//
//    void write_impl(const char *ptr, size_t len) override;
//    uint64_t current_pos() const override;
//};
