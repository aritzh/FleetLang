#pragma once

#include <string>
#include <vector>
#include <stdexcept>
#include <functional>

class DirectoryNotExists : public std::exception {
    std::string reason;
public:
    explicit DirectoryNotExists();
    const char *what() const noexcept override;
};

class NotADirectory : public std::exception {
    std::string reason;
public:
    explicit NotADirectory();
    const char *what() const noexcept override;
};

std::vector<std::string> ListDir(const std::string &path);
std::vector<std::string> ListDir(const std::string &path, const std::function<bool(const std::string &)> &predicate);

bool removeFile(const std::string &path);
std::string getParentDir(const std::string &path);
std::string cannonicalPath(const std::string &path);
