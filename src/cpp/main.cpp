#include <iostream>
#include <cstdlib>
#include <ANTLRInputStream.h>
#include <CommonTokenStream.h>
#include <FleetLangParser.h>
#include <FleetLangLexer.h>
#include <CLI11.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include "Visitor.h"
#include "CodeGen.h"
#include "Settings.h"
#include "Platform.h"
#include "Helpers.h"
#include "MyErrorListener.h"

bool parse(Visitor &v,
           std::unique_ptr<antlr4::ANTLRInputStream> is,
           const std::string &filename,
           const std::string &directory) {
    FleetLangLexer l(is.get());
    antlr4::CommonTokenStream ts(&l);
    FleetLangParser p(&ts);
    auto errorListener = new MyErrorListener(spd::get("console"), filename);
    p.removeErrorListeners();
    p.addErrorListener(errorListener);
    FleetLangParser::ProgramContext *programContext = p.program();
    if (p.getNumberOfSyntaxErrors() > 0) {
        delete errorListener;
        return false;
    }
    v.visitProgram(programContext, filename, directory);
    delete errorListener;
    return true;
}

bool filter_files(const std::string &name, const std::string &arch) {
    std::string without_extension;
    size_t lastdot = name.find_last_of('.');
    if (lastdot != std::string::npos) {
        without_extension = name.substr(0, lastdot);
    } else without_extension = name;

    std::vector<std::string> parts = split(without_extension, ".");
    size_t i;
    for (i = 1; i < parts.size(); i++) {
        if (arch != parts[i]) {
            spd::get("console")->debug("Skipping file '{}'", name);
            return false;
        }
    }
    return true;
}

int main(int argc, char **argv) {
    using namespace antlr4;

    CLI::App app;

    // @formatter:off
    //auto build = app.add_subcommand("build", "Compile input, and write an object file");
    auto build = &app;
    build->add_option("input_folder", settings.input_filename, "Folder to compile. Use a single dash (-) to read from stdin. Reading from stdin assumes -s.")->set_type_name("FILE_NAME");
    build->add_flag("-v,--verbose", settings.verbose, "Print extra logging information.");
    build->add_flag("-r,--release", settings.release, "Optimize the generated code.");
    build->add_option("-o,--output", settings.output_filename, "Output filename. Use a single dash (-) to print to stdout.")->required(true)->set_type_name("FILE_NAME");
    build->add_flag("-s,--single", settings.single_file, "Build single file instead of a whole directory.");
    build->add_flag("-i,--print-llvm", settings.print_llvm, "Instead of writing an object file, print the generated LLVM IR to stdout.");
    build->add_flag("-c,--only-compile", settings.only_compile, "Only compile the input, do not link the object file into an executable.");
    build->add_option("-l,--stdlib", settings.stdlib_path, "Path to the Fleet standard library root")->set_type_name("PATH");
    build->add_option("-t,--triple", settings.target_triple, "Target triple. Used for cross-compiling. Example: 'arm-none-linux-gnueabihf'")->set_type_name("TRIPLE");

//    auto format = app.add_subcommand("format", "Reformat input file");
//    format->add_option("input_file", settings.input_filename, "Folder to compile. Use a single dash (-) to print to stdin.")->set_type_name("FILE_NAME");
//    format->add_flag("-r,--release", settings.release, "Optimize the generated code.");
    // @formatter:on

    CLI11_PARSE(app, argc, argv);

    std::shared_ptr<spdlog::logger> logger = spd::stdout_color_mt("console");
    if (settings.verbose) logger->set_level(spd::level::trace);
    logger->set_pattern("%^[%l]%$ %v");

/*    if (!app.got_subcommand(build) && !app.got_subcommand(format)) {
        std::cout << app.help() << std::endl;
        return 1;
    }*/

//    if (app.got_subcommand(build)) {
    if (settings.single_file) {
        logger->debug("Single file mode");
    }
    if (settings.release) {
        logger->debug("Building on release mode");
    } else {
        logger->debug("Building on debug mode");
    }
//    } else {
//        logger->debug("Formatting files");
//    }

    std::vector<std::string> filesToCompile;
    CodeGen generator;
    if (settings.stdlib_path.empty()) {
        logger->debug("Building without standard library");
    } else {
        filesToCompile = ListDir(settings.stdlib_path);
        filter<std::string>(filesToCompile,
                            [&generator](std::string s) { return filter_files(s, generator.GetTarget().arch); });
        for (const std::string &name : filesToCompile) {
            logger->debug("Found library file '{}'", name);
        }
    }

    if (settings.input_filename == "-") {
        filesToCompile.emplace_back("-");
    } else {
        if (settings.single_file) {
            // When compiling a single file, it will be compile, no matter the arch filter.
            filesToCompile.push_back(settings.input_filename);
        } else {
            std::vector<std::string> files = ListDir(settings.input_filename,
                                                     [](const std::string &it) -> bool {
                                                         return ends_with(it, ".fl");
                                                     });
            filter<std::string>(files,
                                [&generator](std::string s) { return filter_files(s, generator.GetTarget().arch); });
            filesToCompile.insert(filesToCompile.end(), files.begin(), files.end());
        }
    }

    Module module;
    Visitor v(module);
    bool someError = false;
    for (const std::string &file : filesToCompile) {
        std::unique_ptr<ANTLRInputStream> is;
        if (file == "-") {
            std::istreambuf_iterator<char> eos;
            std::string std_input(std::istreambuf_iterator<char>(std::cin), eos);
            is = std::make_unique<ANTLRInputStream>(std_input);
            logger->debug("Parsing from stdin");
        } else {
            std::ifstream stream(file);
            if (stream.fail()) {
                logger->error("Error: file {} does not exists.", settings.input_filename);
                return 4;
            }
            is = std::make_unique<ANTLRInputStream>(stream);
            logger->debug("Parsing file '{}'", file);
        }

        if (!parse(v, std::move(is), file, getParentDir(file))) {
            someError = true;
            continue;
        }
    }
    if (someError) {
        return 3;
    }

    // Validator validator;
    // bool isValid = validator.check(module);
    bool isValid = true;
    if (!isValid) {
        logger->error("Semantic errors");
        return 2;
    }

    /*if (app.got_subcommand(format)) {
        std::cout << module.print() << std::endl;
    } else*/ {
        generator.Generate(module);
        if (generator.wasSuccessful()) {
            logger->debug("Copilation successful");
//            if (app.got_subcommand(build)) {
            if (settings.print_llvm) {
                logger->debug("Printing LLVM IR");
                generator.printLLVM_IR();
            }

            logger->debug("Writing object file");
            generator.WriteOBJ(settings.output_filename);
            if (!settings.only_compile) {
                logger->debug("Linking");
                generator.linkFile(settings.output_filename, settings.output_filename, false);
            }
            logger->debug("Done.");
//            }
        } else {
            generator.log->info("Error compiling.");
            return 3;
        }
    }
    return 0;
}
