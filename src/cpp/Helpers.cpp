#include "Helpers.h"

#include <sstream>

std::string repeat(const std::string &str, int times) {
    std::stringstream ss;
    for (int i = 0; i < times; i++) {
        ss << str;
    }
    return ss.str();
}

bool ends_with(const std::string &value, const std::string &ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}


std::vector<std::string> split(std::string haystack, const std::string &needle) {
    size_t pos = 0;
    std::vector<std::string> result;
    while ((pos = haystack.find(needle)) != std::string::npos) {
        const std::string &str = haystack.substr(0, pos);
        result.push_back(str);
        haystack.erase(0, pos + needle.length());
    }
    result.push_back(haystack);
    return result;
}
