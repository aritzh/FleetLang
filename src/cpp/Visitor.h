// Created by aritz.

#pragma once


#include <FleetLangBaseVisitor.h>
#include "AST/Module.h"
#include "AST/Position.h"
#include "AST/Types.h"

class Visitor : public FleetLangBaseVisitor {
    Module &p;
    std::string fileName;
    std::string directory;

    std::vector<std::shared_ptr<Type>> types;
public:
    explicit Visitor(Module &p);

    antlrcpp::Any visitProgram(FleetLangParser::ProgramContext *ctx, std::string fileName, std::string directory);

    antlrcpp::Any visitFunction(FleetLangParser::FunctionContext *ctx) override;

    antlrcpp::Any visitNormalAssignment(FleetLangParser::NormalAssignmentContext *ctx) override;

    antlrcpp::Any visitIncDecAssignment(FleetLangParser::IncDecAssignmentContext *ctx) override;

    antlrcpp::Any visitIfStmt(FleetLangParser::IfStmtContext *ctx) override;

    antlrcpp::Any visitWhileStmt(FleetLangParser::WhileStmtContext *ctx) override;

    antlrcpp::Any visitReturnStmt(FleetLangParser::ReturnStmtContext *ctx) override;

    antlrcpp::Any visitBlockStmt(FleetLangParser::BlockStmtContext *ctx) override;

    antlrcpp::Any visitDeclStmt(FleetLangParser::DeclStmtContext *ctx) override;

    antlrcpp::Any visitExpressionStmt(FleetLangParser::ExpressionStmtContext *ctx) override;

    antlrcpp::Any visitIntExpr(FleetLangParser::IntExprContext *ctx) override;

    antlrcpp::Any visitFunctionCallExpr(FleetLangParser::FunctionCallExprContext *ctx) override;

    antlrcpp::Any visitOpExpr(FleetLangParser::OpExprContext *ctx) override;

    antlrcpp::Any visitLiteralBool(FleetLangParser::LiteralBoolContext *ctx) override;

    antlrcpp::Any visitComparisonOp(FleetLangParser::ComparisonOpContext *ctx) override;

    antlrcpp::Any visitStringExpr(FleetLangParser::StringExprContext *ctx) override;

    antlrcpp::Any visitFloatExpr(FleetLangParser::FloatExprContext *ctx) override;

    antlrcpp::Any visitSignedExpression(FleetLangParser::SignedExpressionContext *ctx) override;

    antlrcpp::Any visitBooleanOp(FleetLangParser::BooleanOpContext *ctx) override;

    antlrcpp::Any visitIdExpr(FleetLangParser::IdExprContext *ctx) override;

    antlrcpp::Any visitEmptyStmt(FleetLangParser::EmptyStmtContext *ctx) override;

    antlrcpp::Any visitDoubleExpr(FleetLangParser::DoubleExprContext *ctx) override;

    antlrcpp::Any visitCustomBaseType(FleetLangParser::CustomBaseTypeContext *ctx) override;

    antlrcpp::Any visitPrimitiveBaseType(FleetLangParser::PrimitiveBaseTypeContext *ctx) override;

    antlrcpp::Any visitArrayExpr(FleetLangParser::ArrayExprContext *ctx) override;

    antlrcpp::Any visitPrimitiveType(FleetLangParser::PrimitiveTypeContext *ctx) override;

    antlrcpp::Any visitVoidType(FleetLangParser::VoidTypeContext *ctx) override;

    antlrcpp::Any visitParenExpr(FleetLangParser::ParenExprContext *ctx) override;

    antlrcpp::Any visitForStmt(FleetLangParser::ForStmtContext *ctx) override;

    antlrcpp::Any visitDoWhileStmt(FleetLangParser::DoWhileStmtContext *ctx) override;

    antlrcpp::Any visitTopLevelDecl(FleetLangParser::TopLevelDeclContext *ctx) override;

    antlrcpp::Any visitStructDecl(FleetLangParser::StructDeclContext *ctx) override;

    antlrcpp::Any visitDeclaration(FleetLangParser::DeclarationContext *ctx) override;

    antlrcpp::Any visitMemberAccess(FleetLangParser::MemberAccessContext *ctx) override;

    antlrcpp::Any visitCastExpr(FleetLangParser::CastExprContext *ctx) override;

    antlrcpp::Any visitDereferenceExpr(FleetLangParser::DereferenceExprContext *ctx) override;

    antlrcpp::Any visitAddressExpr(FleetLangParser::AddressExprContext *ctx) override;

    antlrcpp::Any visitPointerToType(FleetLangParser::PointerToTypeContext *ctx) override;

    antlrcpp::Any visitAssignStmt(FleetLangParser::AssignStmtContext *ctx) override;

    antlrcpp::Any visitAnnotation(FleetLangParser::AnnotationContext *ctx) override;

    antlrcpp::Any visitAsmExpr(FleetLangParser::AsmExprContext *ctx) override;

    antlrcpp::Any visitAsmExprPart(FleetLangParser::AsmExprPartContext *ctx) override;

    antlrcpp::Any visitAssignmentOperator(FleetLangParser::AssignmentOperatorContext *ctx) override;

    antlrcpp::Any visitCharExpr(FleetLangParser::CharExprContext *ctx) override;

    antlrcpp::Any visitSizeofExpr(FleetLangParser::SizeofExprContext *ctx) override;

    antlrcpp::Any visitNotExpression(FleetLangParser::NotExpressionContext *ctx) override;

    antlrcpp::Any visitArrayType(FleetLangParser::ArrayTypeContext *ctx) override;

private:
    using FleetLangBaseVisitor::visitProgram;
    Position posFromToken(antlr4::Token *token) const;
};
