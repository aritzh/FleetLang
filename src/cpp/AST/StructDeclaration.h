#pragma once

#include <memory>
#include <vector>
#include <llvm/IR/Type.h>
#include <llvm/IR/DerivedTypes.h>
#include "TopLevelDecl.h"
#include "Annotation.h"
#include "Types.h"

class VarDeclStmtAST;

class CodeGen;

class StructDeclaration : public TopLevelDecl, public Type {
    std::string name;
    std::vector<std::unique_ptr<VarDeclStmtAST>> fields;
    std::vector<std::unique_ptr<Annotation>> annotations;

    llvm::StructType *llvmValue;
    bool isMangling = false;
    bool isPrinting = false;

    Position pos;

public:
    StructDeclaration(Position pos,
                      std::string name,
                      std::vector<std::unique_ptr<VarDeclStmtAST>> fields,
                      std::vector<std::unique_ptr<Annotation>> annotations);

    ~StructDeclaration() override = default;

    std::string print() override;

    const std::string &GetName() const;

    const std::vector<std::unique_ptr<VarDeclStmtAST>> &GetFields() const;

    llvm::Type *Generate(CodeGen &gen);

    llvm::Type *GetLLVMType(CodeGen &gen) override;

    llvm::DIType *GetDIType(CodeGen &gen) override;

    bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const override;

    std::string GetManglingName() override;

    unsigned int getSize(CodeGen &gen) const override;

    Position getPosition() const;
};
