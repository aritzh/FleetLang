#include <sstream>
#include "StructDeclaration.h"
#include "ExprAST.h"
#include "StmtAST.h"

StructDeclaration::StructDeclaration(Position pos,
                                     std::string name,
                                     std::vector<std::unique_ptr<VarDeclStmtAST>> fields,
                                     std::vector<std::unique_ptr<Annotation>> annotations)
        : name(std::move(name)), fields(std::move(fields)), annotations(std::move(annotations)), pos(std::move(pos)) {}

std::string StructDeclaration::print() {
    if (this->isPrinting) return "R";
    this->isPrinting = true;
    // TODO Implement printing
    std::stringstream ss;
    ss << "{";
    for (size_t i = 0; i < this->fields.size() - 1; i++) {
        ss << fields[i]->GetType().lock()->print() << " " << fields[i]->GetName() << "; ";
    }
    if (!this->fields.empty()) {
        ss << fields.back()->GetType().lock()->print() << " " << fields.back()->GetName() << ";";
    }
    ss << "}";
    this->isPrinting = false;
    return ss.str();
}

const std::string &StructDeclaration::GetName() const {
    return name;
}

const std::vector<std::unique_ptr<VarDeclStmtAST>> &StructDeclaration::GetFields() const {
    return fields;
}

llvm::Type *StructDeclaration::Generate(CodeGen &gen) {
    this->llvmValue = llvm::StructType::create(*gen.TheContext, name);

    // Find the "canonical" weak_ptr to this.
    std::weak_ptr<StructDeclaration> ptr = gen.FindUngeneratedStruct(this->name);
    assert(ptr.lock().get() == this);

    llvm::StructType *llvmType = gen.InsertSymbol(ptr);

    std::vector<llvm::Type *> elems;
    for (const auto &decl : fields) {
        std::weak_ptr<Type> type = gen.ResolveType(decl->GetType());
        elems.push_back(type.lock()->GetLLVMType(gen));
    }
    this->llvmValue->setBody(elems, false);

    return llvmType;
}

llvm::Type *StructDeclaration::GetLLVMType(CodeGen &gen) {
    return this->llvmValue;
}

bool StructDeclaration::isEqual(CodeGen &gen, std::weak_ptr<Type> other) const {
    auto shType = other.lock();
    const auto &o = std::dynamic_pointer_cast<StructDeclaration>(shType);
    if (this->fields.size() != o->fields.size()) return false;
    for (size_t i = 0; i < fields.size(); i++) {
        if (Type::areEqual(gen, fields[i]->GetType(), o->fields[i]->GetType())) {
            return false;
        }
    }
    return true;
}

std::string StructDeclaration::GetManglingName() {
    if (this->isMangling) return "R";
    this->isMangling = true;
    std::stringstream ss;
    ss << "St";
    for (const auto &t : this->fields) {
        ss << t->GetType().lock()->GetManglingName();
    }
    ss << "tS";
    this->isMangling = false;
    return ss.str();
}

unsigned int StructDeclaration::getSize(CodeGen &gen) const {
    unsigned int size = 0;
    for (const auto &it : this->fields) {
        size += it->GetType().lock()->getSize(gen);
    }
    return size;
}

llvm::DIType *StructDeclaration::GetDIType(CodeGen &gen) {
    std::vector<llvm::Metadata *> elems;
    for (const auto &decl : fields) {
        std::weak_ptr<Type> type = gen.ResolveType(decl->GetType());
        elems.push_back(type.lock()->GetDIType(gen));
    }

    return gen.DB->createStructType(gen.compileUnit, this->name, gen.compileUnit->getFile(),
                             static_cast<unsigned int>(pos.line), getSize(gen), 0, llvm::DINode::FlagZero,
                             nullptr, gen.DB->getOrCreateArray(elems));
}

Position StructDeclaration::getPosition() const {
    return pos;
}
