// Created by aritz.

#pragma once

#include <string>
#include <memory>
#include <array>
#include "../Helpers.h"
#include "Types.h"
#include "../CodeGen.h"

class ExprAST;

// @formatter:off
//                                                      BYTE          CHAR          SHORT         INT           LONG          FLOAT         DOUBLE        BOOL        VOID
//constexpr std::array<std::array<EPrimitiveType, 9>, 9> TypeCoercions{{{{EPrimitiveType::i8,   EPrimitiveType::i8,   EPrimitiveType::i16,  EPrimitiveType::Int,    EPrimitiveType::Long,   EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // BYTE
//                                                                      {{EPrimitiveType::i8,   EPrimitiveType::Char,   EPrimitiveType::i16,  EPrimitiveType::Int,    EPrimitiveType::Long,   EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // CHAR
//                                                                      {{EPrimitiveType::i16,  EPrimitiveType::i16,  EPrimitiveType::i16,  EPrimitiveType::Int,    EPrimitiveType::Long,   EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // SHORT
//                                                                      {{EPrimitiveType::Int,    EPrimitiveType::Int,    EPrimitiveType::Int,    EPrimitiveType::Int,    EPrimitiveType::Long,   EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // INT
//                                                                      {{EPrimitiveType::Long,   EPrimitiveType::Long,   EPrimitiveType::Long,   EPrimitiveType::Long,   EPrimitiveType::Long,   EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // LONG
//                                                                      {{EPrimitiveType::Float,  EPrimitiveType::Float,  EPrimitiveType::Float,  EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Float,  EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // FLOAT
//                                                                      {{EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Double, EPrimitiveType::Void, EPrimitiveType::Void}},   // DOUBLE
//                                                                      {{EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Bool, EPrimitiveType::Void}},   // BOOL
//                                                                      {{EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void,   EPrimitiveType::Void, EPrimitiveType::Void}}}}; // VOID
//
//constexpr std::array<int, 9> TypeSizes{/*i8*/   1,
//                                       /*Char*/   1,
//                                       /*i16*/  2,
//                                       /*Int*/    4,
//                                       /*Long*/   8,
//                                       /*Float*/  4,
//                                       /*Double*/ 8,
//                                       /*Bool*/   1,
//                                       /*Void*/   0};

// @formatter:on

enum class AssignmentOp {
    ASSIGN,
    ADD_ASSIGN,
    SUB_ASSIGN,
    MUL_ASSIGN,
    DIV_ASSIGN,
    MODULO_ASSIGN,
    BOOL_AND_ASSIGN,
    BOOL_OR_ASSIGN,
    BIT_AND_ASSIGN,
    BIT_OR_ASSIGN,
    BIT_XOR_ASSIGN,
};

class StmtAST {
protected:
    Position pos;

public:
    explicit StmtAST(Position pos);

    virtual ~StmtAST();

    virtual std::string print() { throw std::logic_error("Not implemented"); }

    virtual void Generate(CodeGen &gen) = 0;
};

class VarDeclStmtAST;

class AssignStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> left;
    std::unique_ptr<ExprAST> value;
    AssignmentOp op;
public:
    AssignStmtAST(Position pos, std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> value, AssignmentOp op);

    std::string print() override;

    ExprAST *GetLeft();

    ExprAST *GetValue();

    void Generate(CodeGen &gen) override;
};

class IfStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> cond;
    std::unique_ptr<StmtAST> tru;
    std::unique_ptr<StmtAST> fals;
public:
    IfStmtAST(Position pos, std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> tru, std::unique_ptr<StmtAST> fals);

    std::string print() override;

    ExprAST *GetCond();

    StmtAST *GetTrue();

    StmtAST *GetFalse();

    void Generate(CodeGen &gen) override;
};

class WhileStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> cond;
    std::unique_ptr<StmtAST> body;

public:
    WhileStmtAST(Position pos, std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> body);

    std::string print() override;

    ExprAST *GetCond();

    StmtAST *GetBody();

    void Generate(CodeGen &gen) override;
};

class ReturnStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> value;
public:
    explicit ReturnStmtAST(Position pos, std::unique_ptr<ExprAST> value);

    std::string print() override;

    ExprAST *GetExpr();

    void Generate(CodeGen &gen) override;
};

class ExprStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> expr;

public:
    explicit ExprStmtAST(Position pos, std::unique_ptr<ExprAST> expr);

    std::string print() override;

    ExprAST *GetExpr();

    void Generate(CodeGen &gen) override;
};

class BlockStmtAST : public StmtAST {
    std::vector<std::unique_ptr<StmtAST>> stmts;
public:
    explicit BlockStmtAST(Position pos, std::vector<std::unique_ptr<StmtAST>> stmts);

    std::string print() override;

    const std::vector<std::unique_ptr<StmtAST>> &GetStmts();

    void Generate(CodeGen &gen) override;
};

class VarDeclStmtAST : public StmtAST, public TopLevelDecl {
    std::string name;
    std::weak_ptr<Type> type;
    std::unique_ptr<ExprAST> expr;
    llvm::Value *llvmValue;
    bool sttc = false;
    bool global = false;
public:
    VarDeclStmtAST(Position pos, std::string name, std::weak_ptr<Type> type, std::unique_ptr<ExprAST> expr);

    std::string print() override;

    std::string GetName();

    std::weak_ptr<Type> GetType();

    void Generate(CodeGen &gen) override;

    llvm::Value *GetLLVMValue() const;

    void SetLLVMValue(llvm::Value *value);

    friend class FunctionDeclaration;

    friend class AssignStmtAST;

    void SetStatic(bool sttc);

    bool isStatic() const;

    void SetGlobal(bool global);
};

class ForStmtAST : public StmtAST {
    std::unique_ptr<StmtAST> init;
    std::unique_ptr<ExprAST> cond;
    std::unique_ptr<StmtAST> update;
    std::unique_ptr<StmtAST> body;
public:
    ForStmtAST(Position pos,
               std::unique_ptr<StmtAST> init,
               std::unique_ptr<ExprAST> cond,
               std::unique_ptr<StmtAST> update,
               std::unique_ptr<StmtAST> body);

    std::string print() override;

    StmtAST *GetInit() const;

    ExprAST *GetCond() const;

    StmtAST *GetUpdate() const;

    StmtAST *GetBody() const;

    void Generate(CodeGen &gen) override;
};


class DoWhileStmtAST : public StmtAST {
    std::unique_ptr<ExprAST> cond;
    std::unique_ptr<StmtAST> body;

public:
    DoWhileStmtAST(Position pos, std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> body);

    std::string print() override;

    ExprAST *GetCond();

    StmtAST *GetBody();

    void Generate(CodeGen &gen) override;
};
