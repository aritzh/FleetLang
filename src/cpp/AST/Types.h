#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include <llvm/IR/Type.h>
#include <spdlog/spdlog.h>
#include <llvm/IR/DIBuilder.h>
#include "Position.h"

class CodeGen;

enum class EPrimitiveType : int {
    Void = 0,
    Bool = 100,
    Float = 200,
    Double,
    Int = 300,
    i8,
    i16,
    i32,
    i64,
    i128,
    Uint = 350,
    u8,
    u16,
    u32,
    u64,
    u128
};

EPrimitiveType getPrimitiveFromSize(unsigned int size, bool sign);

class Type {
private:
    virtual void noop();
public:
    virtual ~Type() = 0;

    virtual std::string print() = 0;

    virtual llvm::Type *GetLLVMType(CodeGen &gen) = 0;

    virtual llvm::DIType *GetDIType(CodeGen &gen) = 0;

    virtual bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const = 0;

    virtual std::string GetManglingName() = 0;

    static bool areEqual(CodeGen &gen, std::weak_ptr<Type> a, std::weak_ptr<Type> b);

    virtual unsigned int getSize(CodeGen &gen) const = 0;
};

class PrimitiveType : public Type {
    EPrimitiveType type;
public:
    explicit PrimitiveType(EPrimitiveType type);

    EPrimitiveType getType();

    std::string print() override;

    llvm::Type *GetLLVMType(CodeGen &gen) override;

    bool isIntegerType() const;

    bool isFloatingType() const;

    unsigned int getSizeInBits(CodeGen &gen) const;

    unsigned int getSize(CodeGen &gen) const override;

    bool isSigned() const;

    bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const override;

    std::string GetManglingName() override;

    llvm::DIType *GetDIType(CodeGen &gen) override;

};

//class CompoundType : public Type {
//    std::vector<std::pair<std::string, std::weak_ptr<Type>>> components;
//
//public:
//    explicit CompoundType(std::vector<std::pair<std::string, std::weak_ptr<Type>>> components);
//
//    const std::vector<std::pair<std::string, std::weak_ptr<Type> > > &Components();
//
//    llvm::Type *GetLLVMType(CodeGen &gen) override;
//};

class ArrayType : public Type {
    std::weak_ptr<Type> component;
    unsigned long size;
public:
    ArrayType(std::weak_ptr<Type> component, unsigned long size);

    std::string print() override;

    std::weak_ptr<Type> ComponentType() const;

    unsigned long Size() const;

    llvm::Type *GetLLVMType(CodeGen &gen) override;

    llvm::DIType *GetDIType(CodeGen &gen) override;

    bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const override;

    std::string GetManglingName() override;

    unsigned int getSize(CodeGen &gen) const override;
};

class NamedType : public Type {
    Position pos;
    std::string name;
public:
    explicit NamedType(Position pos, std::string name);

    const std::string &getName() const;

    std::string print() override;

    llvm::Type *GetLLVMType(CodeGen &gen) override;

    llvm::DIType *GetDIType(CodeGen &gen) override;

    bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const override;

    std::string GetManglingName() override;

    unsigned int getSize(CodeGen &gen) const override;
};

class PointerToType : public Type {
    std::weak_ptr<Type> type;
public:
    explicit PointerToType(std::weak_ptr<Type> type);

    std::weak_ptr<Type> getElement() const;

    std::string print() override;

    llvm::Type *GetLLVMType(CodeGen &gen) override;

    llvm::DIType *GetDIType(CodeGen &gen) override;

    bool isEqual(CodeGen &gen, std::weak_ptr<Type> other) const override;

    std::string GetManglingName() override;

    unsigned int getSize(CodeGen &gen) const override;
};
