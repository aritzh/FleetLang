#include "Position.h"

Position::Position(std::string filename, std::string directory, size_t line, size_t col)
        : fileName(std::move(filename)), directory(std::move(directory)), line(line), col(col) {}

Position::Position(Position &&other) noexcept
        : fileName(std::move(other.fileName)), directory(std::move(other.directory)), line(other.line), col(other.col) {}

Position &Position::operator=(Position &&other) noexcept {
    this->fileName = std::move(other.fileName);
    this->directory = std::move(other.directory);
    this->line = other.line;
    this->col = other.col;
    return *this;
}
