// Created by aritz.

#include <sstream>
#include <llvm/IR/Verifier.h>
#include "Function.h"
#include "../CodeGen.h"
#include "StmtAST.h"
#include "ExprAST.h"

Function::Function(Position pos,
                   std::weak_ptr<Type> ret,
                   std::string name,
                   std::vector<std::unique_ptr<VarDeclStmtAST>> args,
                   std::vector<std::unique_ptr<StmtAST>> stmts,
                   std::vector<std::unique_ptr<Annotation>> annotations,
                   bool isPublic,
                   FunctionType type)
        : FunctionDeclaration(pos, std::move(ret), std::move(name), std::move(args), std::move(annotations), isPublic,
                              type),
          stmts(std::move(stmts)) {}

std::string Function::print() {

    std::stringstream ss;
    ss << this->name << "(";
    if (!this->args.empty()) {
        for (size_t i = 0; i < this->args.size() - 1; i++) {
            ss << this->args[i]->GetType().lock()->print() << " " << this->args[i]->GetName() << ", ";
        }
        ss << this->args[this->args.size() - 1]->GetType().lock()->print() << " "
           << this->args[this->args.size() - 1]->GetName();
    }
    ss << ") {\n";

    for (auto &s : this->stmts) {
        ss << s->print();
    }

    ss << "}\n";

    return ss.str();
}

const std::vector<std::unique_ptr<StmtAST>> &Function::GetStmts() {
    return this->stmts;
}

llvm::Function *Function::Generate(CodeGen &gen) {

    std::vector<std::weak_ptr<Type>> paramTypes;
    for (const auto &it : this->args) {
        paramTypes.push_back(it->GetType());
    }

    llvm::Function *TheFunction;
    if (this->declared) {
        TheFunction = this->llvmValue;
    } else {
        FunctionDeclaration *decl = gen.FindDeclaredFunction(this->name, paramTypes);
        if (decl) {
            TheFunction = decl->GetLLVMValue();
        } else {
            TheFunction = this->GenerateDeclaration(gen);
        }
        if (TheFunction == nullptr) return nullptr;
    }

    if (!TheFunction->empty()) {
        gen.error(pos, "Function '{}' is already defined", this->name);
        return nullptr;
    }

    gen.debug(pos, "Generating function {}", this->PrintSignature());

    llvm::BasicBlock *BB = llvm::BasicBlock::Create(*gen.TheContext, fmt::format("{}_entry", this->name), TheFunction);
    gen.Builder.SetInsertPoint(BB);

    gen.PushScope();

    size_t i = 0;
    for (auto &arg : TheFunction->args()) {
        std::unique_ptr<VarDeclStmtAST> &argg = args[i];
        argg->SetLLVMValue(&arg);
        gen.InsertSymbol(argg.get());
        i++;
    }

    for (const auto &stmt : stmts) {
        stmt->Generate(gen);
    }

    // Add terminators to blocks that do not have one
    const auto &originalPoint = gen.Builder.GetInsertPoint();
    llvm::BasicBlock *originalBlock = gen.Builder.GetInsertBlock();

    for (auto &b : TheFunction->getBasicBlockList()) {
        if (b.getTerminator() == nullptr) {
            gen.Builder.SetInsertPoint(&b);
            if (&TheFunction->getBasicBlockList().back() == &b) {
                if (TheFunction->getReturnType()->isVoidTy()) {
                    gen.Builder.CreateRetVoid();
                } else {
                    gen.error(pos, "Function '{}' is missing a return!", this->PrintSignature());
                    return nullptr;
                    // TODO Maybe return nullptr?
                }
            } else {
                gen.Builder.CreateUnreachable();
            }
        }
    }
    gen.Builder.SetInsertPoint(originalBlock, originalPoint);

    if (llvm::verifyFunction(*TheFunction, &llvm::errs())) {
        // Errors, so print function
        TheFunction->print(llvm::errs());
    }

    gen.PopScope();

    return TheFunction;
}

FunctionDeclaration::FunctionDeclaration(Position pos, std::weak_ptr<Type> ret,
                                         std::string name,
                                         std::vector<std::unique_ptr<VarDeclStmtAST>> args,
                                         std::vector<std::unique_ptr<Annotation>> annotations,
                                         bool isPublic,
                                         FunctionType type)
        : pos(std::move(pos)),
          ret(std::move(ret)),
          name(std::move(name)),
          args(std::move(args)),
          annotations(std::move(annotations)),
          isPublic(isPublic),
          type(type) {}

std::string FunctionDeclaration::print() {
    std::stringstream ss;
    ss << this->ret.lock()->print() << " " << this->name << "(";
    if (!this->args.empty()) {
        for (size_t i = 0; i < this->args.size() - 1; i++) {
            ss << this->args[i]->GetType().lock()->print() << " " << this->args[i]->GetName() << ", ";
        }
        ss << this->args[this->args.size() - 1]->GetType().lock()->print() << " "
           << this->args[this->args.size() - 1]->GetName();
    }
    ss << ");\n";
    return ss.str();
}

std::string FunctionDeclaration::GetName() {
    return this->name;
}

std::weak_ptr<Type> FunctionDeclaration::GetReturnType() {
    return this->ret;
}

const std::vector<std::unique_ptr<VarDeclStmtAST>> &FunctionDeclaration::GetArgs() {
    return this->args;
}

bool FunctionDeclaration::operator==(const FunctionDeclaration &other) const {
    return this->name == other.name && this->ret.lock() == other.ret.lock() && this->args == other.args;
}

bool FunctionDeclaration::operator!=(const FunctionDeclaration &other) const {
    return !(*this == other);
}

llvm::Function *FunctionDeclaration::GenerateDeclaration(CodeGen &gen) {
    if (declared) return this->llvmValue;
    declared = true;
    gen.debug(pos, "Declaring function {}", this->PrintSignature());

    std::vector<llvm::Type *> ArgTypes;

    llvm::AttrBuilder fnAttrBuilder;
    if (this->FindAnnotation("inline")) {
        fnAttrBuilder.addAttribute(llvm::Attribute::AttrKind::AlwaysInline);
    }

    llvm::AttributeSet fnAttr = llvm::AttributeSet::get(*gen.TheContext, fnAttrBuilder);

    llvm::AttributeSet retAttr = llvm::AttributeSet::get(*gen.TheContext, llvm::AttrBuilder());
    std::vector<llvm::AttributeSet> argsAttr;

    for (auto &p : args) {
        std::weak_ptr<Type> type = gen.ResolveType(p->GetType());
        p->type = type;
        llvm::Type *argType = type.lock()->GetLLVMType(gen);
        if (argType == nullptr) return nullptr;
        llvm::AttrBuilder attrBuilder;
        if (argType->isStructTy()) { // If a struct, change it to a ptr to it, and set it "ByVal"
            attrBuilder.addAttribute(llvm::Attribute::ByVal);
            ArgTypes.push_back(llvm::PointerType::get(argType, 0));
        } else {
            ArgTypes.push_back(argType);
        }
        argsAttr.push_back(llvm::AttributeSet::get(*gen.TheContext, attrBuilder));
    }

    llvm::Function::LinkageTypes linkage =
            isPublic || this->type == FunctionType::Foreign ? llvm::Function::ExternalLinkage
                                                           : llvm::Function::InternalLinkage;

    assert(!ret.expired());
    llvm::FunctionType *FT = llvm::FunctionType::get(ret.lock()->GetLLVMType(gen), ArgTypes, false);
    llvm::Function *TheFunction = llvm::Function::Create(FT,
                                                         linkage,
                                                         this->GetMangledName(),
                                                         gen.TheModule);
    TheFunction->setAttributes(llvm::AttributeList::get(*gen.TheContext, fnAttr, retAttr, argsAttr));
    unsigned Idx = 0;
    for (auto &arg : TheFunction->args()) {
        arg.setName(args[Idx++]->GetName());
    }
    this->llvmValue = TheFunction;

    return gen.InsertSymbol(this);
}

llvm::Function *FunctionDeclaration::Generate(CodeGen &gen) {
    return GenerateDeclaration(gen);
}

llvm::Function *FunctionDeclaration::GetLLVMValue() {
    return this->llvmValue;
}

bool FunctionDeclaration::accepts(CodeGen &gen, const std::vector<std::weak_ptr<Type>> &types) const {
    if (types.size() != this->args.size()) return false;

    for (size_t i = 0; i < types.size(); i++) {
        if (!Type::areEqual(gen, this->args[i]->GetType(), types[i])) {
            return false;
        }
    }
    return true;
}

std::string FunctionDeclaration::GetMangledName() const {
    if (this->type == FunctionType::Foreign) {
        return this->name;
    }
    std::stringstream ss;
    ss << this->name << "_";
    for (const auto &it : this->args) {
        ss << it->GetType().lock()->GetManglingName();
    }
    return ss.str();
}

Annotation *FunctionDeclaration::FindAnnotation(const std::string &name) const {
    for (const auto &a : this->annotations) {
        if (a->GetName() == name) return a.get();
    }
    return nullptr;
}

std::string FunctionDeclaration::PrintSignature() {
    std::stringstream ss;
    ss << this->name << "(";
    for (size_t i = 0; i < this->args.size(); i++) {
        ss << this->args[i]->GetType().lock()->print();
        if (i != this->args.size() - 1) {
            ss << ", ";
        }
    }
    ss << ")";
    return ss.str();
}

Position FunctionDeclaration::GetPosition() const {
    return pos;
}
