// Created by aritz.

#pragma once

#include <string>
#include <memory>
#include <vector>
#include <llvm/IR/Value.h>
#include "Types.h"
#include "../CodeGen.h"

enum class BinaryOp {
    Add,
    Sub,
    Mul,
    Div,
    BitAnd,
    BitOr,
    BitXor,
    Modulo,
    LeftShift,
    ArithmeticRightShift,
    LogicalRightShift,
};

enum class UnaryOp {
    Pos,
    Neg,
    Not,
    PostInc,
    PostDec,
//    PreInc,
//    PreDec,
};

enum class ComparisonOp {
    LT,
    LE,
    GT,
    GE,
    EQ,
    NE,
};

enum class BoolOp {
    AND,
    OR,
};

class MemberAccessAST;

class ExprAST {
protected:
    llvm::Value *llvmValue = nullptr;
    llvm::Value *llvmAddress = nullptr;
    Position pos;
    std::weak_ptr<Type> type = std::weak_ptr<Type>();
    bool comptimeConstant = false;
    llvm::Constant *constant = nullptr;

public:
    explicit ExprAST(Position pos);

    ExprAST(Position pos, std::weak_ptr<Type> type);

    virtual ~ExprAST() = default;

    virtual std::string print();

    virtual llvm::Value *Generate(CodeGen &gen) = 0;

    llvm::Type *GetLLVMType();

    llvm::Value *GetLLVMValue();

    llvm::Value *GetLLVMAddress();

    std::shared_ptr<Type> GetType();

    bool isComptimeConstant() const;

    llvm::Constant *GetConstant() const;

    friend class MemberAccessAST;
};

class StringExprAST : public ExprAST {
    std::string value;
public:
    explicit StringExprAST(Position pos, std::string value);

    std::string print() override;

    llvm::Value *Generate(CodeGen &gen) override;

};

class FloatExprAST : public ExprAST {
    float value;
public:
    explicit FloatExprAST(Position pos, float value);

    std::string print() override;

    float GetValue() const;

    llvm::Value *Generate(CodeGen &gen) override;
};

class DoubleExprAST : public ExprAST {
    double value;
public:
    explicit DoubleExprAST(Position pos, double value);

    std::string print() override;

    double GetValue() const;

    llvm::Value *Generate(CodeGen &gen) override;
};

class IntExprAST : public ExprAST {
    std::string value;
    unsigned int bitCount;
    uint8_t base;
    bool sign;
public:
    IntExprAST(Position pos, std::string value, unsigned int bitCount, uint8_t base, bool sign);

    std::string print() override;

    std::string GetValue() const;

    llvm::Value *Generate(CodeGen &gen) override;
};

class BinaryExprAST : public ExprAST {
    std::unique_ptr<ExprAST> LHS, RHS;
    BinaryOp op;
public:
    BinaryExprAST(Position pos, BinaryOp op, std::unique_ptr<ExprAST> LHS, std::unique_ptr<ExprAST> RHS);

    std::string print() override;

    ExprAST *GetLHS();

    ExprAST *GetRHS();

    BinaryOp GetOp();

    void SetLHS(ExprAST *lhs);

    void SetRHS(ExprAST *rhs);

    llvm::Value *Generate(CodeGen &gen) override;
};

class UnaryExprAST : public ExprAST {
    UnaryOp op;
    std::unique_ptr<ExprAST> value;
public:
    UnaryExprAST(Position pos, UnaryOp op, std::unique_ptr<ExprAST> value);

    std::string print() override;

    UnaryOp GetOp();

    ExprAST *GetExpr();

    llvm::Value *Generate(CodeGen &gen) override;
};

class CallExprAST : public ExprAST {
    std::string functionName;
    std::vector<std::unique_ptr<ExprAST>> args;
    bool isIntrinsic;
public:
    CallExprAST(Position pos, std::string callee, std::vector<std::unique_ptr<ExprAST>> args, bool isIntrinsic);

    std::string print() override;

    std::string GetCallee();

    const std::vector<std::unique_ptr<ExprAST>> &GetArgs();

    llvm::Value *Generate(CodeGen &gen) override;
};

class VariableExprAST : public ExprAST {
    std::string name;
public:
    explicit VariableExprAST(Position pos, std::string name);

    std::string print() override;

    std::string GetName();

    llvm::Value *Generate(CodeGen &gen) override;
};

class ComparisonExprAST : public ExprAST {
    ComparisonOp op;
    std::unique_ptr<ExprAST> LHS, RHS;
public:
    ComparisonExprAST(Position pos, ComparisonOp op, std::unique_ptr<ExprAST> LHS, std::unique_ptr<ExprAST> RHS);

    std::string print() override;

    ComparisonOp GetOp();

    ExprAST *GetLHS();

    ExprAST *GetRHS();

    void SetLHS(ExprAST *lhs);

    void SetRHS(ExprAST *rhs);

    llvm::Value *Generate(CodeGen &gen) override;
};

class BoolOpExprAST : public ExprAST {
    BoolOp op;
    std::unique_ptr<ExprAST> LHS, RHS;
public:
    BoolOpExprAST(Position pos, BoolOp op, std::unique_ptr<ExprAST> LHS, std::unique_ptr<ExprAST> RHS);

    std::string print() override;

    BoolOp GetOp();

    ExprAST *GetLHS();

    ExprAST *GetRHS();

    llvm::Value *Generate(CodeGen &gen) override;
};

class LiteralBoolExprAST : public ExprAST {
    bool value;
public:
    explicit LiteralBoolExprAST(Position pos, bool value);

    std::string print() override;

    bool GetValue();

    llvm::Value *Generate(CodeGen &gen) override;
};

class CastingExprAST : public ExprAST {
    std::unique_ptr<ExprAST> value;
public:
    CastingExprAST(Position pos, std::unique_ptr<ExprAST> value, std::weak_ptr<Type> type);

    std::string print() override;

    ExprAST *GetValue();

    std::weak_ptr<Type> GetTargetType();

    llvm::Value *Generate(CodeGen &gen) override;
};

class ArrayExprAST : public ExprAST {
    std::unique_ptr<ExprAST> var;
    std::unique_ptr<ExprAST> idx;

public:
    ArrayExprAST(Position pos, std::unique_ptr<ExprAST> var, std::unique_ptr<ExprAST> idx);

    std::string print() override;

    ExprAST *GetVar();

    ExprAST *GetIdx();

    llvm::Value *Generate(CodeGen &gen) override;
};

class MemberAccessAST : public ExprAST {
    std::unique_ptr<ExprAST> expr;
    std::string member;

public:
    MemberAccessAST(Position pos, std::unique_ptr<ExprAST> expr, std::string member);

    ExprAST *GetExpr();

    std::string GetMember();

    llvm::Value *Generate(CodeGen &gen) override;
};

class AddressOfExprAST : public ExprAST {
    std::unique_ptr<ExprAST> expr;

public:
    explicit AddressOfExprAST(Position pos, std::unique_ptr<ExprAST> expr);

    ExprAST *GetExpr();

    llvm::Value *Generate(CodeGen &gen) override;

};

class DereferenceExprAST : public ExprAST {
    std::unique_ptr<ExprAST> expr;

public:
    explicit DereferenceExprAST(Position pos, std::unique_ptr<ExprAST> expr);

    ExprAST *GetExpr();

    llvm::Value *Generate(CodeGen &gen) override;

};

class AsmExprPart {
    Position pos;
    std::string str;
    std::unique_ptr<ExprAST> var;
public:
    AsmExprPart(Position pos, std::string str, std::unique_ptr<ExprAST> var);

    AsmExprPart(AsmExprPart &&other) noexcept;

    AsmExprPart &operator==(AsmExprPart &&other);

    AsmExprPart(const AsmExprPart &) = delete;

    AsmExprPart &operator=(const AsmExprPart &) = delete;

    const std::string &GetStr() const;

    ExprAST *GetVar() const;

    Position GetPosition() const;
};

class AsmExprAST : public ExprAST {
    std::string asmExpr;
    std::vector<AsmExprPart> outputExpr;
    std::vector<AsmExprPart> inputExpr;
    std::vector<std::string> clobberExpr;

public:
    AsmExprAST(Position pos,
               std::string asmExpr,
               std::vector<AsmExprPart> outputExpr,
               std::vector<AsmExprPart> inputExpr,
               std::vector<std::string> clobberExpr);

    std::string print() override;

    llvm::Value *Generate(CodeGen &gen) override;
};

class CharExprAST : public ExprAST {
    uint32_t value;

public:
    CharExprAST(Position pos, uint32_t value);

    std::string print() override;

    llvm::Value *Generate(CodeGen &gen) override;
};

class SizeofExprAST : public ExprAST {
    std::weak_ptr<Type> exprType;

public:
    SizeofExprAST(Position pos, std::weak_ptr<Type> type);

    std::string print() override;

    llvm::Value *Generate(CodeGen &gen) override;
};
