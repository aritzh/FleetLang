// Created by aritz.

#pragma once

#include <memory>
#include <vector>
#include "TopLevelDecl.h"

class Module {
    std::vector<std::shared_ptr<TopLevelDecl>> functions;

public:

    std::string print();

    const std::vector<std::shared_ptr<TopLevelDecl>> &GetTopLevelDecls() const;

    void add(std::shared_ptr<TopLevelDecl> tld);
};
