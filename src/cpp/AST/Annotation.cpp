#include "Annotation.h"

Annotation::Annotation(std::string name, std::vector<std::string> args)
        : name(std::move(name)), args(std::move(args)) {}

const std::string &Annotation::GetName() const {
    return this->name;
}

const std::vector<std::string> &Annotation::GetArgs() const {
    return this->args;
}

Annotation::Annotation(std::string name)
        : name(std::move(name)), args() {}
