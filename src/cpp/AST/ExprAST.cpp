// Created by aritz.

#include <sstream>
#include <utility>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/PatternMatch.h>
#include "ExprAST.h"
#include "StmtAST.h"

llvm::Type *ExprAST::GetLLVMType() {
    return this->llvmValue->getType();
}

llvm::Value *ExprAST::GetLLVMValue() {
    return this->llvmValue;
}

llvm::Value *ExprAST::GetLLVMAddress() {
    return this->llvmAddress;
}

ExprAST::ExprAST(Position pos)
        : pos(std::move(pos)) {}

std::shared_ptr<Type> ExprAST::GetType() {
    return this->type.lock();
}

std::string ExprAST::print() {
    return "";
}

ExprAST::ExprAST(Position pos, std::weak_ptr<Type> type)
        : pos(std::move(pos)), type(std::move(type)) {}

bool ExprAST::isComptimeConstant() const {
    return comptimeConstant;
}

llvm::Constant *ExprAST::GetConstant() const {
    return constant;
}

StringExprAST::StringExprAST(Position pos, std::string value)
        : ExprAST(std::move(pos)), value(std::move(value)) {}

std::string StringExprAST::print() {
    return "\"" + this->value + "\"";
}

llvm::Value *StringExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(
            std::make_shared<PointerToType>(gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::i8))));

    std::stringstream ss;
    for (size_t i = 0; i < this->value.length(); i++) {
        char value = this->value[i];
        if ((value >= 'a' && value <= 'z') ||
            (value >= 'A' && value <= 'Z') ||
            (value >= '0' && value <= '9') ||
            value == '-' ||
            value == '_' ||
            value == '.' ||
            value == '$') {
            ss << value;
        } else {
            if (value == '\\') {
                i++;
                assert(i != this->value.length());
                char escape = this->value[i];
                switch (escape) {
                    case 'n':
                        value = '\n';
                        break;
                    case 'r':
                        value = '\r';
                        break;
                    case '0':
                        value = '\0';
                        break;
                    case 'f':
                        value = '\f';
                        break;
                    case 't':
                        value = '\t';
                        break;
                    case 'v':
                        value = '\v';
                        break;
                    case 'a':
                        value = '\a';
                        break;
                    case 'b':
                        value = '\b';
                        break;
                    default:
                        // TODO Add octal, hex and unicode escapes
                        gen.error(pos, "Unrecognized escape sequence '\\{}'", escape);
                        break;
                }
            }
            ss << value;
        }
    }
    this->value = ss.str();

    return this->llvmValue = gen.Builder.CreateConstInBoundsGEP2_64(gen.Builder.CreateGlobalString(this->value, "str"),
                                                                    0,
                                                                    0);
}

FloatExprAST::FloatExprAST(Position pos, float value)
        : ExprAST(std::move(pos)), value(value) {}

std::string FloatExprAST::print() {
    return std::to_string(this->value);
}

float FloatExprAST::GetValue() const {
    return this->value;
}

llvm::Value *FloatExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::Float));
    this->comptimeConstant = true;
    return this->llvmValue = this->constant = gen.CreateLiteralFloat(this->value);
}

IntExprAST::IntExprAST(Position pos, std::string value, unsigned int bitCount, uint8_t base, bool sign)
        : ExprAST(std::move(pos)), value(std::move(value)), bitCount(bitCount), base(base), sign(sign) {}

std::string IntExprAST::print() {
    std::stringstream ss;
    ss << this->value;
    return ss.str();
}

std::string IntExprAST::GetValue() const {
    return this->value;
}

llvm::Value *IntExprAST::Generate(CodeGen &gen) {
    if (bitCount == 0) {
        bitCount = gen.GetPointerSize();
    }
    this->type = gen.addType(std::make_shared<PrimitiveType>(getPrimitiveFromSize(bitCount, sign)));
    this->comptimeConstant = true;
    return this->llvmValue = this->constant = llvm::ConstantInt::get(*gen.TheContext,
                                                                     llvm::APInt(bitCount, this->value, base));
}

BinaryExprAST::BinaryExprAST(Position pos, BinaryOp op, std::unique_ptr<ExprAST> LHS, std::unique_ptr<ExprAST> RHS)
        : ExprAST(std::move(pos)), LHS(std::move(LHS)), RHS(std::move(RHS)), op(op) {}

std::string BinaryExprAST::print() {
    std::string oper;
    switch (this->op) {
        case BinaryOp::Add:
            oper = " + ";
            break;
        case BinaryOp::Sub:
            oper = " - ";
            break;
        case BinaryOp::Mul:
            oper = " * ";
            break;
        case BinaryOp::Div:
            oper = " / ";
            break;
        case BinaryOp::BitAnd:
            oper = " & ";
            break;
        case BinaryOp::BitOr:
            oper = " | ";
            break;
        case BinaryOp::BitXor:
            oper = " ^ ";
            break;
        case BinaryOp::Modulo:
            oper = " % ";
            break;
        case BinaryOp::LeftShift:
            oper = " << ";
            break;
        case BinaryOp::ArithmeticRightShift:
            oper = " >> ";
            break;
        case BinaryOp::LogicalRightShift:
            oper = " >>> ";
            break;
    }

    return this->LHS->print() + oper + this->RHS->print();
}

ExprAST *BinaryExprAST::GetLHS() {
    return this->LHS.get();
}

ExprAST *BinaryExprAST::GetRHS() {
    return this->RHS.get();
}

BinaryOp BinaryExprAST::GetOp() {
    return this->op;
}

void BinaryExprAST::SetLHS(ExprAST *lhs) {
    this->LHS.release();
    this->LHS.reset(lhs);
}

void BinaryExprAST::SetRHS(ExprAST *rhs) {
    this->RHS.release();
    this->RHS.reset(rhs);
}

llvm::Value *BinaryExprAST::Generate(CodeGen &gen) {
    llvm::Value *L = LHS->Generate(gen);
    llvm::Value *R = RHS->Generate(gen);

    if (!L || !R) return nullptr;

    auto ShLHSType = gen.ResolveType(LHS->GetType()).lock();
    auto ShRHSType = gen.ResolveType(RHS->GetType()).lock();

    auto lhsPrim = std::dynamic_pointer_cast<PrimitiveType>(ShLHSType);
    auto rhsPrim = std::dynamic_pointer_cast<PrimitiveType>(ShRHSType);
    auto lhsPointer = std::dynamic_pointer_cast<PointerToType>(ShLHSType);
    auto rhsPointer = std::dynamic_pointer_cast<PointerToType>(ShRHSType);
    if (lhsPrim && rhsPrim) {
        if (lhsPrim->getType() != rhsPrim->getType()) {
            gen.error(pos, "Cannot use binary operator between different types");
            return nullptr;
        } else {
            this->type = LHS->GetType();
        }
    } else if (lhsPointer || rhsPointer) {
        if (lhsPointer) {
            if (!rhsPrim || !rhsPrim->isIntegerType()) {
                gen.error(pos, "If one operand is a pointer, the other must be an integer type");
                return nullptr;
            } else {
                this->type = LHS->GetType();
            }
        } else {
            if (!lhsPrim || !lhsPrim->isIntegerType()) {
                gen.error(pos, "If one operand is a pointer, the other must be an integer type");
                return nullptr;
            } else {
                this->type = LHS->GetType();
            }
        }
    } else {
        gen.error(pos, "Binary operations must be between pointer and integer, or two equal primitive types");
        return nullptr;
    }

    if (!LHS->isComptimeConstant() || !RHS->isComptimeConstant()) {
        bool anyPtr = false;
        llvm::Type *oldType = nullptr;
        if (lhsPointer) {
            anyPtr = true;
            oldType = L->getType();
            L = gen.Builder.CreatePtrToInt(L, llvm::IntegerType::get(*gen.TheContext, gen.GetPointerSize()));
            unsigned int elemSize = lhsPointer->getElement().lock()->getSize(gen);
            PrimitiveType elemSizeType(EPrimitiveType::Uint);
            R = gen.Builder.CreateMul(R, gen.CreateLiteralInt(elemSize, elemSizeType));
        } else if (rhsPointer) {
            anyPtr = true;
            oldType = R->getType();
            R = gen.Builder.CreatePtrToInt(R, llvm::IntegerType::get(*gen.TheContext, gen.GetPointerSize()));
            unsigned int elemSize = rhsPointer->getElement().lock()->getSize(gen);
            PrimitiveType elemSizeType(EPrimitiveType::Uint);
            L = gen.Builder.CreateMul(L, gen.CreateLiteralInt(elemSize, elemSizeType));
        }

        llvm::Value *result = nullptr;

        if (L->getType()->isFloatingPointTy()) {
            switch (op) {
                case BinaryOp::Add:
                    result = gen.Builder.CreateFAdd(L, R, "addf");
                    break;
                case BinaryOp::Sub:
                    result = gen.Builder.CreateFSub(L, R, "subf");
                    break;
                case BinaryOp::Mul:
                    result = gen.Builder.CreateFMul(L, R, "mulf");
                    break;
                case BinaryOp::Div:
                    result = gen.Builder.CreateFDiv(L, R, "divf");
                    break;
                case BinaryOp::Modulo:
                    result = gen.Builder.CreateFRem(L, R, "addf");
                    break;
                case BinaryOp::BitAnd:
                case BinaryOp::BitOr:
                case BinaryOp::BitXor:
                    gen.error(pos, "Cannot use bitwise operators on floating point types");
                    return nullptr;
                case BinaryOp::LeftShift:
                case BinaryOp::ArithmeticRightShift:
                case BinaryOp::LogicalRightShift:
                    gen.error(pos, "Cannot use shift operators on floating point types");
                    return nullptr;
            }
        } else {
            switch (op) {
                case BinaryOp::Add:
                    result = gen.Builder.CreateAdd(L, R, "add");
                    break;
                case BinaryOp::Sub:
                    result = gen.Builder.CreateSub(L, R, "sub");
                    break;
                case BinaryOp::Mul:
                    result = gen.Builder.CreateMul(L, R, "mul");
                    break;
                case BinaryOp::Div:
                    if (!lhsPrim || lhsPrim->isSigned()) {
                        result = gen.Builder.CreateSDiv(L, R, "div");
                    } else {
                        result = gen.Builder.CreateUDiv(L, R, "div");
                    }
                    break;
                case BinaryOp::BitAnd:
                    result = gen.Builder.CreateAnd(L, R, "bit_and");
                    break;
                case BinaryOp::BitOr:
                    result = gen.Builder.CreateOr(L, R, "bit_or");
                    break;
                case BinaryOp::BitXor:
                    result = gen.Builder.CreateXor(L, R, "bit_xor");
                    break;
                case BinaryOp::Modulo:
                    if (!lhsPrim || lhsPrim->isSigned()) {
                        result = gen.Builder.CreateSRem(L, R, "mod");
                    } else {
                        result = gen.Builder.CreateURem(L, R, "mod");
                    }
                    break;
                case BinaryOp::LeftShift:
                    result = gen.Builder.CreateShl(L, R, "shl");
                    break;
                case BinaryOp::ArithmeticRightShift:
                    result = gen.Builder.CreateAShr(L, R, "sar");
                    break;
                case BinaryOp::LogicalRightShift:
                    result = gen.Builder.CreateLShr(L, R, "shr");
                    break;
            }
        }
        if (anyPtr) {
            result = gen.Builder.CreateIntToPtr(result, oldType);
        }
        return this->llvmValue = result;

    } else {
        this->comptimeConstant = true;
        if (L->getType()->isFloatingPointTy()) {
            switch (op) {
                case BinaryOp::Add:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFAdd(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::Sub:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFSub(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::Mul:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFMul(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::Div:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFDiv(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::Modulo:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFRem(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::BitAnd:
                case BinaryOp::BitOr:
                case BinaryOp::BitXor:
                    gen.error(pos, "Cannot use bitwise operators on floating point types");
                    return nullptr;
                case BinaryOp::LeftShift:
                case BinaryOp::ArithmeticRightShift:
                case BinaryOp::LogicalRightShift:
                    gen.error(pos, "Cannot use shift operators on floating point types");
                    return nullptr;
            }
        } else {
            switch (op) {
                case BinaryOp::Add:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getAdd(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::Sub:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getSub(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::Mul:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getMul(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::Div:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getSDiv(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::BitAnd:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getAnd(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::BitOr:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getOr(LHS->GetConstant(),
                                                                                        RHS->GetConstant());
                case BinaryOp::BitXor:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getXor(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::Modulo:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getSRem(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::LeftShift:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getShl(LHS->GetConstant(),
                                                                                         RHS->GetConstant());
                case BinaryOp::ArithmeticRightShift:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getAShr(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case BinaryOp::LogicalRightShift:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getLShr(LHS->GetConstant(),
                                                                                          RHS->GetConstant());
            }
        }
    }
    throw std::logic_error("Unrecognized binary operator");
}

UnaryExprAST::UnaryExprAST(Position pos, UnaryOp op, std::unique_ptr<ExprAST> value)
        : ExprAST(std::move(pos)), op(op), value(std::move(value)) {}

std::string UnaryExprAST::print() {
    const std::string &value = this->value->print();
    switch (this->op) {
        case UnaryOp::Pos:
            return "+" + value;
        case UnaryOp::Neg:
            return "-" + value;
        case UnaryOp::Not:
            return "!" + value;
        case UnaryOp::PostInc:
            return value + "++";
        case UnaryOp::PostDec:
            return value + "--";
    }
    throw std::logic_error("Unknown unary operator");
}


UnaryOp UnaryExprAST::GetOp() {
    return this->op;
}

ExprAST *UnaryExprAST::GetExpr() {
    return this->value.get();
}

llvm::Value *UnaryExprAST::Generate(CodeGen &gen) {
    llvm::Value *v = this->value->Generate(gen);
    this->type = this->value->GetType();

    if (auto prim = dynamic_cast<PrimitiveType *>(this->value->GetType().get())) {
        if (prim->getType() == EPrimitiveType::Bool && op == UnaryOp::Not) {
            if (this->value->isComptimeConstant()) {
                this->comptimeConstant = true;
                llvm::Constant *cValue = this->value->GetConstant();
                return this->llvmValue = this->constant = llvm::ConstantExpr::getNot(cValue);
            } else {
                return this->llvmValue = gen.Builder.CreateNot(v);
            }
        } else if (prim->isFloatingType()) {
            llvm::Constant *one;
            if (prim->getType() == EPrimitiveType::Float) {
                one = gen.CreateLiteralFloat(1);
            } else {
                one = gen.CreateLiteralDouble(1);
            }
            if (this->value->isComptimeConstant()) {
                llvm::Constant *cValue = this->value->GetConstant();
                switch (this->op) {
                    case UnaryOp::Pos:
                        this->comptimeConstant = true;
                        return this->llvmValue = this->constant = cValue;
                    case UnaryOp::Neg:
                        this->comptimeConstant = true;
                        return this->llvmValue = this->constant = llvm::ConstantExpr::getFNeg(cValue);
                    case UnaryOp::Not:
                        this->comptimeConstant = true;
                        gen.error(pos, "Cannot use the 'not' operator with floating point values");
                        return nullptr;
                    case UnaryOp::PostInc:
                        // TODO Test post inc/dec
                        this->comptimeConstant = true;
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot increment expression");
                        }
                        gen.Builder.CreateStore(llvm::ConstantExpr::getFAdd(cValue, one),
                                                this->value->GetLLVMAddress());
                        return this->llvmValue = this->constant = cValue;
                    case UnaryOp::PostDec:
                        this->comptimeConstant = true;
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot decrement expression");
                        }
                        gen.Builder.CreateStore(llvm::ConstantExpr::getFSub(cValue, one),
                                                this->value->GetLLVMAddress());
                        return this->llvmValue = this->constant = cValue;
                }
                throw std::logic_error("Unknown unary operator");
            } else {
                switch (this->op) {
                    case UnaryOp::Pos:
                        this->type = this->value->GetType();
                        return this->llvmValue = v;
                    case UnaryOp::Neg:
                        return this->llvmValue = gen.Builder.CreateFNeg(v);
                    case UnaryOp::Not:
                        gen.error(pos, "Cannot use the 'not' operator with floating point values");
                        return nullptr;
                    case UnaryOp::PostInc:
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot increment expression");
                        }
                        gen.Builder.CreateStore(gen.Builder.CreateAdd(v, one), this->value->GetLLVMAddress());
                        return this->llvmValue = v;
                    case UnaryOp::PostDec:
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot decrement expression");
                        }
                        gen.Builder.CreateStore(gen.Builder.CreateSub(v, one), this->value->GetLLVMAddress());
                        return this->llvmValue = v;
                }
                throw std::logic_error("Unknown unary operator");
            }
        } else if (prim->isIntegerType()) {
            llvm::Constant *one = gen.CreateLiteralInt(1, *prim);
            if (this->value->isComptimeConstant()) {
                this->comptimeConstant = true;
                llvm::Constant *cValue = this->value->GetConstant();
                switch (this->op) {
                    case UnaryOp::Pos:
                        return this->llvmValue = this->constant = cValue;
                    case UnaryOp::Neg:
                        return this->llvmValue = this->constant = llvm::ConstantExpr::getNeg(cValue);
                    case UnaryOp::Not:
                        return this->llvmValue = this->constant = llvm::ConstantExpr::getNot(cValue);
                    case UnaryOp::PostInc:
                        this->comptimeConstant = true;
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot increment expression");
                        }
                        gen.Builder.CreateStore(llvm::ConstantExpr::getAdd(cValue, one),
                                                this->value->GetLLVMAddress());
                        return this->llvmValue = this->constant = cValue;
                    case UnaryOp::PostDec:
                        this->comptimeConstant = true;
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot decrement expression");
                        }
                        gen.Builder.CreateStore(llvm::ConstantExpr::getSub(cValue, one),
                                                this->value->GetLLVMAddress());
                        return this->llvmValue = this->constant = cValue;
                }
                throw std::logic_error("Unknown unary operator");
            } else {
                switch (this->op) {
                    case UnaryOp::Pos:
                        this->type = this->value->GetType();
                        return this->llvmValue = v;
                    case UnaryOp::Neg:
                        return this->llvmValue = gen.Builder.CreateNeg(v);
                    case UnaryOp::Not:
                        gen.error(pos, "Cannot use the 'not' operator with floating point values");
                        return nullptr;
                    case UnaryOp::PostInc:
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot increment expression");
                        }
                        gen.Builder.CreateStore(gen.Builder.CreateAdd(v, one), this->value->GetLLVMAddress());
                        return this->llvmValue = v;
                    case UnaryOp::PostDec:
                        if (this->value->GetLLVMAddress() == nullptr) {
                            gen.error(pos, "Cannot decrement expression");
                        }
                        gen.Builder.CreateStore(gen.Builder.CreateSub(v, one), this->value->GetLLVMAddress());
                        return this->llvmValue = v;
                }
                throw std::logic_error("Unknown unary operator");
            }
        } else {
            gen.error(pos, "Unary expression can only be applied to integer of float expressions");
            return nullptr;
        }
    } else {
        gen.error(pos, "Cannot use unary expression on non-primitive types");
        return nullptr;
    }

}

CallExprAST::CallExprAST(Position pos, std::string callee, std::vector<std::unique_ptr<ExprAST>> args, bool isIntrinsic)
        : ExprAST(std::move(pos)), functionName(std::move(callee)), args(std::move(args)), isIntrinsic(isIntrinsic) {}

std::string CallExprAST::print() {
    std::stringstream ss;
    ss << this->functionName << "(";
    if (!this->args.empty()) {
        for (unsigned int i = 0; i < this->args.size() - 1; i++) {
            ss << this->args[i]->print() << ", ";
        }
        ss << this->args[this->args.size() - 1]->print();
    }
    ss << ")";
    return ss.str();
}


std::string CallExprAST::GetCallee() {
    return this->functionName;
}

const std::vector<std::unique_ptr<ExprAST>> &CallExprAST::GetArgs() {
    return this->args;
}

llvm::Value *CallExprAST::Generate(CodeGen &gen) {
    std::vector<llvm::Value *> argsV;
    std::vector<std::weak_ptr<Type>> argTypes;
    std::vector<llvm::Type *> argLLVMTypes;
    for (auto &arg : args) {
        llvm::Value *value = arg->Generate(gen);
        if (value == nullptr) return nullptr;

        argTypes.push_back(arg->GetType());
        argLLVMTypes.push_back(arg->GetLLVMType());

        if (value->getType()->isStructTy()) {
            if (arg->GetLLVMAddress() == nullptr) {
                // TODO Only case where this could happen, is when the struct is a literal. Implement this case too,
                // TODO it should be possible to pass struct literals by value
                throw std::logic_error("Passing struct literals as arguments is not yet supported");
            }
            argsV.push_back(arg->GetLLVMAddress());
        } else {
            argsV.push_back(value);
        }
    }

    if (isIntrinsic) {
        llvm::Intrinsic::ID intrinsicID;
        if (functionName == "sqrt") {
            intrinsicID = llvm::Intrinsic::ID::sqrt;
            if (args.size() != 1) {
                gen.error(pos, "Intrinsic 'sqrt' requires exactly one argument");
                return nullptr;
            }
            this->type = this->args[0]->GetType();
        } else {
            gen.error(pos, "Unknown intrinsic with name '{}'", functionName);
            return nullptr;
        }

        auto decl = llvm::Intrinsic::getDeclaration(gen.TheModule, intrinsicID, argLLVMTypes);
        return this->llvmValue = gen.Builder.CreateCall(decl, argsV, functionName);
    }

    FunctionDeclaration *func = gen.FindFunction(functionName, argTypes);

    if (!func) {
        std::stringstream ss;
        ss << functionName << "(";
        if (!argTypes.empty()) {
            for (size_t i = 0; i < argTypes.size() - 1; i++) {
                ss << argTypes[i].lock()->print() << ", ";
            }
            ss << argTypes.back().lock()->print();
        }
        ss << ")";
        gen.error(pos, "Undefined funtion '{}' called", ss.str());
        return nullptr;
    }

    this->type = gen.ResolveType(func->GetReturnType());
    llvm::Function *calleeF = func->GetLLVMValue();
    if (calleeF == nullptr) {
        gen.error(pos, "Unknown function '{}' referenced", functionName);
        return nullptr;
    }

    // If argument mismatch error.
    if (calleeF->arg_size() != args.size()) {
        gen.error(pos,
                  "Incorrect number of arguments for function '{}'\nGiven: {}, expected: {}",
                  functionName,
                  args.size(),
                  calleeF->arg_size());
        return nullptr;
    }


    if (calleeF->getReturnType()->isVoidTy()) {
        return this->llvmValue = gen.Builder.CreateCall(calleeF, argsV);
    } else {
        return this->llvmValue = gen.Builder.CreateCall(calleeF, argsV, functionName);
    }
}

VariableExprAST::VariableExprAST(Position pos, std::string name)
        : ExprAST(std::move(pos)), name(std::move(name)) {}

std::string VariableExprAST::print() {
    return this->name;
}

std::string VariableExprAST::GetName() {
    return this->name;
}

llvm::Value *VariableExprAST::Generate(CodeGen &gen) {
    auto var = gen.FindVariable(name);

    if (var != nullptr) {
        if (var->GetLLVMValue() == nullptr) return nullptr;
        this->type = gen.ResolveType(var->GetType());

        if (llvm::dyn_cast<llvm::AllocaInst>(var->GetLLVMValue())) {
            this->llvmAddress = var->GetLLVMValue();
            return this->llvmValue = gen.Builder.CreateLoad(var->GetLLVMValue(), name);
        } else if (auto arg = llvm::dyn_cast<llvm::Argument>(var->GetLLVMValue())) {
            if (arg->hasAttribute(llvm::Attribute::ByVal) && arg->getType()->isPointerTy() &&
                arg->getType()->getPointerElementType()->isStructTy()) {
                this->llvmAddress = var->GetLLVMValue();
                return this->llvmValue = gen.Builder.CreateLoad(var->GetLLVMValue(), name);
            } else {
                return this->llvmValue = var->GetLLVMValue();
            }
        } else {
            this->llvmAddress = var->GetLLVMValue();
            return this->llvmValue = gen.Builder.CreateLoad(var->GetLLVMValue(), name);
        }
    } else {
        gen.error(pos, "Undefined variable '{}'", name);
        return nullptr;
    }
}

ComparisonExprAST::ComparisonExprAST(Position pos,
                                     ComparisonOp op,
                                     std::unique_ptr<ExprAST> LHS,
                                     std::unique_ptr<ExprAST> RHS)
        : ExprAST(std::move(pos)), op(op), LHS(std::move(LHS)), RHS(std::move(RHS)) {}

std::string ComparisonExprAST::print() {
    std::string oper;
    switch (this->op) {
        case ComparisonOp::EQ:
            oper = " == ";
            break;
        case ComparisonOp::LT:
            oper = " < ";
            break;
        case ComparisonOp::LE:
            oper = " <= ";
            break;
        case ComparisonOp::GT:
            oper = " > ";
            break;
        case ComparisonOp::GE:
            oper = " >= ";
            break;
        case ComparisonOp::NE:
            oper = " != ";
            break;
    }

    return this->LHS->print() + oper + this->RHS->print();
}


ComparisonOp ComparisonExprAST::GetOp() {
    return this->op;
}

ExprAST *ComparisonExprAST::GetLHS() {
    return this->LHS.get();
}

ExprAST *ComparisonExprAST::GetRHS() {
    return this->RHS.get();
}

void ComparisonExprAST::SetLHS(ExprAST *lhs) {
    this->LHS.release();
    this->LHS.reset(lhs);
}

void ComparisonExprAST::SetRHS(ExprAST *rhs) {
    this->RHS.release();
    this->RHS.reset(rhs);
}

llvm::Value *ComparisonExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::Bool));
    llvm::Value *LHSV = LHS->Generate(gen);
    llvm::Value *RHSV = RHS->Generate(gen);

    auto ShLHSType = LHS->GetType();
    auto ShRHSType = RHS->GetType();
    auto lhsPrim = std::dynamic_pointer_cast<PrimitiveType>(ShLHSType);
    auto rhsPrim = std::dynamic_pointer_cast<PrimitiveType>(ShRHSType);

    auto ShPrimInt = gen.ResolveType(std::make_shared<PrimitiveType>(EPrimitiveType::Int)).lock();
    auto pointerSizedInt = std::dynamic_pointer_cast<PrimitiveType>(ShPrimInt);

    if (!lhsPrim && std::dynamic_pointer_cast<PointerToType>(ShLHSType)) {
        LHSV = gen.Builder.CreatePtrToInt(LHSV, llvm::IntegerType::get(*gen.TheContext, gen.GetPointerSize()));
        lhsPrim = pointerSizedInt;
    }
    if (!rhsPrim && std::dynamic_pointer_cast<PointerToType>(ShRHSType)) {
        RHSV = gen.Builder.CreatePtrToInt(RHSV, llvm::IntegerType::get(*gen.TheContext, gen.GetPointerSize()));
        rhsPrim = pointerSizedInt;
    }

    if (lhsPrim && rhsPrim) {
        if (lhsPrim->getType() != rhsPrim->getType()) {
            gen.error(pos, "Cannot use comparison operator between different types");
            return nullptr;
        } else {

        }
    } else {
        gen.error(pos, "Comparisons are only allowed between primitive types");
        return nullptr;
    }

    if (!LHS->isComptimeConstant() || !RHS->isComptimeConstant()) {
        if (lhsPrim->isFloatingType()) {
            switch (op) {
                case ComparisonOp::LT:
                    return this->llvmValue = gen.Builder.CreateFCmpULT(LHSV, RHSV, "lt");
                case ComparisonOp::LE:
                    return this->llvmValue = gen.Builder.CreateFCmpULE(LHSV, RHSV, "le");
                case ComparisonOp::GT:
                    return this->llvmValue = gen.Builder.CreateFCmpUGT(LHSV, RHSV, "gt");
                case ComparisonOp::GE:
                    return this->llvmValue = gen.Builder.CreateFCmpUGE(LHSV, RHSV, "ge");
                case ComparisonOp::EQ:
                    return this->llvmValue = gen.Builder.CreateFCmpUEQ(LHSV, RHSV, "eq");
                case ComparisonOp::NE:
                    return this->llvmValue = gen.Builder.CreateFCmpUNE(LHSV, RHSV, "ne");
                default:
                    gen.error(pos, "Unrecognized comparison operator");
                    return nullptr;
            }
        } else if (lhsPrim->isIntegerType()) {
            switch (op) {
                case ComparisonOp::LT:
                    return this->llvmValue = gen.Builder.CreateICmpSLT(LHSV, RHSV, "lt");
                case ComparisonOp::LE:
                    return this->llvmValue = gen.Builder.CreateICmpSLE(LHSV, RHSV, "le");
                case ComparisonOp::GT:
                    return this->llvmValue = gen.Builder.CreateICmpSGT(LHSV, RHSV, "gt");
                case ComparisonOp::GE:
                    return this->llvmValue = gen.Builder.CreateICmpSGE(LHSV, RHSV, "ge");
                case ComparisonOp::EQ:
                    return this->llvmValue = gen.Builder.CreateICmpEQ(LHSV, RHSV, "eq");
                case ComparisonOp::NE:
                    return this->llvmValue = gen.Builder.CreateICmpNE(LHSV, RHSV, "ne");
                default:
                    gen.error(pos, "Unrecognized comparison operator");
                    return nullptr;
            }
        } else {
            gen.error(pos, "Comparison operations are only allowed between integer or floating types");
            return nullptr;
        }
    } else {
        if (lhsPrim->isFloatingType()) {
            switch (op) {
                case ComparisonOp::LT:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_ULT,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::LE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_ULE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::GT:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_UGT,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::GE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_UGE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::EQ:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_UEQ,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::NE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::FCMP_UNE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                default:
                    gen.error(pos, "Unrecognized comparison operator");
                    return nullptr;
            }
        } else if (lhsPrim->isIntegerType()) {
            switch (op) {
                case ComparisonOp::LT:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_ULT,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::LE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_ULE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::GT:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_UGT,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::GE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_UGE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::EQ:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_EQ,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                case ComparisonOp::NE:
                    return this->llvmValue = this->constant = llvm::ConstantExpr::getFCmp(llvm::CmpInst::ICMP_NE,
                                                                                          LHS->GetConstant(),
                                                                                          RHS->GetConstant());
                default:
                    gen.error(pos, "Unrecognized comparison operator");
                    return nullptr;
            }
        } else {
            gen.error(pos, "Comparison operations are only allowed between integer or floating types");
            return nullptr;
        }
    }
}

BoolOpExprAST::BoolOpExprAST(Position pos, BoolOp op, std::unique_ptr<ExprAST> LHS, std::unique_ptr<ExprAST> RHS)
        : ExprAST(std::move(pos)), op(op), LHS(std::move(LHS)), RHS(std::move(RHS)) {}

std::string BoolOpExprAST::print() {
    std::string oper;
    switch (this->op) {
        case BoolOp::AND:
            oper = " && ";
            break;
        case BoolOp::OR:
            oper = " || ";
            break;
    }

    return this->LHS->print() + oper + this->RHS->print();
}

BoolOp BoolOpExprAST::GetOp() {
    return this->op;
}

ExprAST *BoolOpExprAST::GetLHS() {
    return this->LHS.get();
}

ExprAST *BoolOpExprAST::GetRHS() {
    return this->RHS.get();
}

llvm::Value *BoolOpExprAST::Generate(CodeGen &gen) {
    llvm::Value *lhs = LHS->Generate(gen);
    llvm::Value *rhs = RHS->Generate(gen);

    auto lhsType = dynamic_cast<PrimitiveType *>(LHS->GetType().get());
    auto rhsType = dynamic_cast<PrimitiveType *>(LHS->GetType().get());
    if (lhsType && rhsType && lhsType == rhsType && lhsType->getType() == EPrimitiveType::Bool) {
        this->type = LHS->GetType();
    } else {
        gen.error(pos, "Boolean operators can only be applied to boolean values");
        return nullptr;
    }

    switch (op) {
        case BoolOp::AND:
            if (LHS->isComptimeConstant() && RHS->isComptimeConstant()) {
                return this->llvmValue = this->constant = llvm::ConstantExpr::getAnd(LHS->GetConstant(),
                                                                                     RHS->GetConstant());
            } else {
                return this->llvmValue = gen.Builder.CreateAnd(lhs, rhs, "and");
            }
        case BoolOp::OR:
            if (LHS->isComptimeConstant() && RHS->isComptimeConstant()) {
                return this->llvmValue = this->constant = llvm::ConstantExpr::getOr(LHS->GetConstant(),
                                                                                    RHS->GetConstant());
            } else {
                return this->llvmValue = gen.Builder.CreateOr(lhs, rhs, "or");
            }
    }
    throw std::logic_error("Unrecognized boolean opeator");
}

LiteralBoolExprAST::LiteralBoolExprAST(Position pos, bool value)
        : ExprAST(std::move(pos)), value(value) {}

std::string LiteralBoolExprAST::print() {
    return this->value ? "true" : "false";
}

bool LiteralBoolExprAST::GetValue() {
    return this->value;
}

llvm::Value *LiteralBoolExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::Bool));
    this->comptimeConstant = true;
    return this->llvmValue = this->constant = gen.CreateLiteralBool(this->value);
}

DoubleExprAST::DoubleExprAST(Position pos, double value)
        : ExprAST(std::move(pos)), value(value) {}

std::string DoubleExprAST::print() {
    return std::to_string(this->value);
}

double DoubleExprAST::GetValue() const {
    return this->value;
}

llvm::Value *DoubleExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::Double));
    this->comptimeConstant = true;
    return this->llvmValue = this->constant = gen.CreateLiteralDouble(this->value);
}

CastingExprAST::CastingExprAST(Position pos, std::unique_ptr<ExprAST> value, std::weak_ptr<Type> type)
        : ExprAST(std::move(pos), std::move(type)), value(std::move(value)) {}

std::string CastingExprAST::print() {
    return ExprAST::print();
}

ExprAST *CastingExprAST::GetValue() {
    return this->value.get();
}

std::weak_ptr<Type> CastingExprAST::GetTargetType() {
    return this->type;
}

llvm::Value *CastingExprAST::Generate(CodeGen &gen) {

    this->type = gen.ResolveType(this->type);
    llvm::Value *v = value->Generate(gen);
    std::shared_ptr<Type> ShTargetType = this->type.lock();
    std::shared_ptr<Type> ShValueType = value->GetType();
    auto destTy = ShTargetType->GetLLVMType(gen);

    if (auto srcPtr = std::dynamic_pointer_cast<PointerToType>(ShValueType)) {
        if (auto targetPtr = std::dynamic_pointer_cast<PointerToType>(ShTargetType)) {
            if (!Type::areEqual(gen, srcPtr->getElement().lock(), targetPtr->getElement())) {
                return this->llvmValue = gen.Builder.CreateBitCast(v, destTy);
            } else {
                return this->llvmValue = v;
            }
        } else if (auto targetPrim = std::dynamic_pointer_cast<PrimitiveType>(ShTargetType)) {
            if (targetPrim->isIntegerType()) {
                return this->llvmValue = gen.Builder.CreatePtrToInt(v, destTy);
            } else {
                gen.error(pos,
                          "Cannot cast pointer type '{}' to non-integer type '{}'",
                          ShValueType->print(),
                          ShTargetType->print());
                return this->llvmValue = nullptr;
            }
        }
    }

    if (auto srcPrim = std::dynamic_pointer_cast<PrimitiveType>(ShValueType)) {
        if (auto targetPrim = std::dynamic_pointer_cast<PrimitiveType>(ShTargetType)) {
            auto srcSize = srcPrim->getSizeInBits(gen);
            auto targetSize = targetPrim->getSizeInBits(gen);
            if (srcPrim->isIntegerType() && targetPrim->isIntegerType()) {
                if (srcSize > targetSize) {
                    return this->llvmValue = gen.Builder.CreateTrunc(v, destTy);
                } else if (srcSize < targetSize) {
                    if (srcPrim->isSigned()) {
                        return this->llvmValue = gen.Builder.CreateSExt(v, destTy);
                    } else {
                        return this->llvmValue = gen.Builder.CreateZExt(v, destTy);
                    }
                } else {
                    return this->llvmValue = v;
                }
            } else if (srcPrim->isFloatingType() && targetPrim->isIntegerType()) {
                if (targetPrim->isSigned()) {
                    return this->llvmValue = gen.Builder.CreateFPToSI(v, destTy);
                } else {
                    return this->llvmValue = gen.Builder.CreateFPToUI(v, destTy);
                }
            } else if (srcPrim->isIntegerType() && targetPrim->isFloatingType()) {
                if (srcPrim->isSigned()) {
                    return this->llvmValue = gen.Builder.CreateSIToFP(v, destTy);
                } else {
                    return this->llvmValue = gen.Builder.CreateUIToFP(v, destTy);
                }
            } else if (srcPrim->isFloatingType() && targetPrim->isFloatingType()) {
                if (srcSize > targetSize) {
                    return this->llvmValue = gen.Builder.CreateFPTrunc(v, destTy);
                } else if (srcSize < targetSize) {
                    return this->llvmValue = gen.Builder.CreateFPExt(v, destTy);
                } else {
                    return this->llvmValue = v;
                }
            }
        } else if (auto targetPtr = std::dynamic_pointer_cast<PointerToType>(ShTargetType)) {
            return this->llvmValue = gen.Builder.CreateIntToPtr(v, destTy);
        }
    }

    gen.error(pos,
              "Unknown cast from type '{}' to type '{}'",
              ShValueType->print(),
              ShTargetType->print());
    return this->llvmValue = nullptr;
}

ArrayExprAST::ArrayExprAST(Position pos, std::unique_ptr<ExprAST> var, std::unique_ptr<ExprAST> idx)
        : ExprAST(std::move(pos)), var(std::move(var)), idx(std::move(idx)) {}

ExprAST *ArrayExprAST::GetVar() {
    return this->var.get();
}

ExprAST *ArrayExprAST::GetIdx() {
    return this->idx.get();
}

std::string ArrayExprAST::print() {
    return this->var->print() + "[" + idx->print() + "]";
}

llvm::Value *ArrayExprAST::Generate(CodeGen &gen) {
    llvm::Value *val = this->var->Generate(gen);
    if (val == nullptr) {
        return nullptr;
    }

    std::weak_ptr<Type> varType = gen.ResolveType(var->GetType());
    if (auto arrType = dynamic_cast<ArrayType *>(varType.lock().get())) {
        this->type = gen.ResolveType(arrType->ComponentType());

        // First index is 0 because we access the current item.
        std::vector<llvm::Value *> idxs;
        PrimitiveType idxType(EPrimitiveType::Uint);
        idxs.push_back(gen.CreateLiteralInt(0, idxType));

        idxs.push_back(idx->Generate(gen));

        this->llvmAddress = gen.Builder.CreateInBoundsGEP(this->var->GetLLVMAddress(),
                                                          idxs,
                                                          "gep_" + this->var->print());
        return this->llvmValue = gen.Builder.CreateLoad(this->llvmAddress);

    } else if (auto ptrType = dynamic_cast<PointerToType *>(varType.lock().get())) {
        this->type = gen.ResolveType(ptrType->getElement());

        std::vector<llvm::Value *> idxs;
//        idxs.push_back(llvm::ConstantInt::get(*gen.TheContext, llvm::APInt(32, 0, false)));

        idxs.push_back(idx->Generate(gen));

        this->llvmAddress = gen.Builder.CreateGEP(this->type.lock()->GetLLVMType(gen), var->GetLLVMValue(), idxs,
                                                  "gep_" + this->var->print());
        return this->llvmValue = gen.Builder.CreateLoad(this->llvmAddress);
    } else {
        gen.error(pos, "Cannot index a non-array!");
        return nullptr;
    }
}

MemberAccessAST::MemberAccessAST(Position pos, std::unique_ptr<ExprAST> expr, std::string member)
        : ExprAST(std::move(pos)), expr(std::move(expr)), member(std::move(member)) {}

ExprAST *MemberAccessAST::GetExpr() {
    return expr.get();
}

std::string MemberAccessAST::GetMember() {
    return member;
}

llvm::Value *MemberAccessAST::Generate(CodeGen &gen) {
    llvm::Value *expr = this->expr->Generate(gen);
    if (expr == nullptr) return nullptr;

    const std::weak_ptr<Type> &type = this->expr->GetType();

    if (auto structType = dynamic_cast<StructDeclaration *>(type.lock().get())) {
        unsigned int index = 0;
        for (const auto &it : structType->GetFields()) {
            if (it->GetName() == member) {
                break;
            }
            index++;
        }
        if (index == structType->GetFields().size()) {
            gen.error(pos, "Type '{}' does not have a member named '{}'", structType->GetName(), member);
            return nullptr;
        } else {
            this->type = gen.ResolveType(structType->GetFields()[index]->GetType());
            this->llvmAddress = gen.Builder.CreateStructGEP(expr->getType(),
                                                            this->expr->GetLLVMAddress(),
                                                            index,
                                                            expr->getName() + "_" + member);
            return this->llvmValue = gen.Builder.CreateLoad(this->llvmAddress);
        }
    } else {
        std::string type_str;
        llvm::raw_string_ostream rso(type_str);
        expr->getType()->print(rso);
        gen.error(pos, "Trying to access member of non-struct type '{}'", type.lock()->print());
        return nullptr;
    }
}

AddressOfExprAST::AddressOfExprAST(Position pos, std::unique_ptr<ExprAST> expr)
        : ExprAST(std::move(pos)), expr(std::move(expr)) {}

ExprAST *AddressOfExprAST::GetExpr() {
    return this->expr.get();
}

llvm::Value *AddressOfExprAST::Generate(CodeGen &gen) {
    this->expr->Generate(gen);
    this->type = gen.addType(std::make_shared<PointerToType>(this->expr->GetType()));
    return this->llvmValue = this->expr->GetLLVMAddress();
}

DereferenceExprAST::DereferenceExprAST(Position pos, std::unique_ptr<ExprAST> expr)
        : ExprAST(std::move(pos)), expr(std::move(expr)) {}

ExprAST *DereferenceExprAST::GetExpr() {
    return this->expr.get();
}

llvm::Value *DereferenceExprAST::Generate(CodeGen &gen) {
    this->llvmAddress = this->expr->Generate(gen);
    if (auto type = dynamic_cast<PointerToType *>(this->expr->GetType().get())) {
        this->type = gen.ResolveType(type->getElement());
        return this->llvmValue = gen.Builder.CreateLoad(this->llvmAddress, "Der_" + this->llvmAddress->getName());
    } else {
        gen.error(pos, "Trying to dereference a non-pointer type");
        return nullptr;
    }
}

AsmExprAST::AsmExprAST(Position pos,
                       std::string asmExpr,
                       std::vector<AsmExprPart> outputExpr,
                       std::vector<AsmExprPart> inputExpr,
                       std::vector<std::string> clobberExpr)
        : ExprAST(std::move(pos)), asmExpr(std::move(asmExpr)), outputExpr(std::move(outputExpr)), inputExpr(std::move(
        inputExpr)), clobberExpr(
        std::move(clobberExpr)) {}

std::string AsmExprAST::print() {
    throw std::logic_error("Not implemented");
}

llvm::Value *AsmExprAST::Generate(CodeGen &gen) {
    std::vector<llvm::Type *> resultTypes;
    std::vector<llvm::Type *> paramTypes;
    std::vector<llvm::Value *> params;
    std::string constraint;

    for (const auto &it : this->outputExpr) {
        llvm::Value *rValue = it.GetVar()->Generate(gen);
        llvm::Value *address = it.GetVar()->GetLLVMAddress();
        if (!address) {
            gen.error(it.GetPosition(), "Asm output is not assignamble");
            return nullptr;
        }
        resultTypes.push_back(rValue->getType());
        constraint += it.GetStr() + ",";
    }

    for (const auto &it : this->inputExpr) {
        llvm::Value *pValue = it.GetVar()->Generate(gen);
        if (pValue == nullptr) return nullptr;
        paramTypes.push_back(pValue->getType());
        params.push_back(pValue);
        constraint += it.GetStr() + ",";
    }

    for (const std::string &clobber : this->clobberExpr) {
        constraint += "~" + clobber + ",";
    }

    // Remove trailing comma
    if (constraint.size() > 1) {
        constraint = constraint.substr(0, constraint.size() - 1);
    }

    llvm::Type *resultType;
    if (resultTypes.size() > 1) {
        resultType = llvm::StructType::get(*gen.TheContext, resultTypes);
    } else if (resultTypes.size() == 1) {
        resultType = resultTypes[0];
    } else {
        resultType = llvm::Type::getVoidTy(*gen.TheContext);
    }

    llvm::FunctionType *functionType = llvm::FunctionType::get(resultType, paramTypes, false);
    llvm::InlineAsm *asmFunction = llvm::InlineAsm::get(functionType, this->asmExpr, constraint, false);
    llvm::CallInst *result = gen.Builder.CreateCall(asmFunction, params);

    if (resultTypes.size() == 1) {
        gen.Builder.CreateStore(result, outputExpr[0].GetVar()->GetLLVMAddress());
    } else {
        for (size_t i = 0; i < resultTypes.size(); i++) {
            gen.Builder.CreateStore(gen.Builder.CreateExtractValue(result,
                                                                   llvm::ArrayRef<unsigned int>(
                                                                           static_cast<unsigned int>(i))),
                                    outputExpr[i].GetVar()->GetLLVMAddress());
        }
    }

    return nullptr;
}

const std::string &AsmExprPart::GetStr() const {
    return this->str;
}

ExprAST *AsmExprPart::GetVar() const {
    return this->var.get();
}

AsmExprPart::AsmExprPart(Position pos, std::string str, std::unique_ptr<ExprAST> var)
        : pos(std::move(pos)), str(std::move(str)), var(std::move(var)) {}

Position AsmExprPart::GetPosition() const {
    return pos;
}

AsmExprPart::AsmExprPart(AsmExprPart &&other) noexcept
        : pos(std::move(other.pos)), str(std::move(other.str)), var(std::move(other.var)) {}

AsmExprPart &AsmExprPart::operator==(AsmExprPart &&other) {
    this->pos = other.pos;
    this->str = std::move(other.str);
    this->var = std::move(other.var);
    return *this;
}

CharExprAST::CharExprAST(Position pos, uint32_t value)
        : ExprAST(std::move(pos)), value(value) {}

std::string CharExprAST::print() {
    throw std::logic_error("Not implemented");
}

llvm::Value *CharExprAST::Generate(CodeGen &gen) {
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::u32));
    PrimitiveType type(EPrimitiveType::u32);
    return this->llvmValue = gen.CreateLiteralInt(value, type);
}

SizeofExprAST::SizeofExprAST(Position pos, std::weak_ptr<Type> type)
        : ExprAST(std::move(pos)), exprType(std::move(type)) {}

std::string SizeofExprAST::print() {
    return fmt::format("sizeof({})", this->exprType.lock()->print());
}

llvm::Value *SizeofExprAST::Generate(CodeGen &gen) {
    this->comptimeConstant = true;
    this->type = gen.addType(std::make_shared<PrimitiveType>(EPrimitiveType::Int));
    unsigned int size = this->exprType.lock()->getSize(gen);
    PrimitiveType sizeType(EPrimitiveType::Int);
    return this->llvmValue = this->constant = gen.CreateLiteralInt(size, sizeType);
}
