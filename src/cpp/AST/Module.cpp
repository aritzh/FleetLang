// Created by aritz.

#include <sstream>
#include "Module.h"

std::string Module::print() {
    std::stringstream ss;
    for (auto &f : this->functions) {
        ss << f->print() << "\n";
    }
    return ss.str();
}

const std::vector<std::shared_ptr<TopLevelDecl>> &Module::GetTopLevelDecls() const {
    return this->functions;
}

void Module::add(std::shared_ptr<TopLevelDecl> tld) {
    this->functions.push_back(std::move(tld));
}
