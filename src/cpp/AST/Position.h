#pragma once

#include <cstddef>
#include <string>

struct Position {
    std::string fileName;
    std::string directory;
    size_t line;
    size_t col;

    Position(std::string filename, std::string directory, size_t line, size_t col);

    Position(const Position &other) = default;

    Position(Position &&other) noexcept;

    Position &operator=(const Position &other) = default;

    Position &operator=(Position &&other) noexcept;

};
