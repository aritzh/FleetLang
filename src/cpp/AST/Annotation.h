#pragma once


#include <vector>
#include <string>

class Annotation {
    std::string name;
    std::vector<std::string> args;

public:
    Annotation(std::string name);
    Annotation(std::string name, std::vector<std::string> args);

    const std::string &GetName() const;

    const std::vector<std::string> &GetArgs() const;
};

