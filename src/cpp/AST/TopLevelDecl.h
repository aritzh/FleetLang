#pragma once

#include <string>
#include "Position.h"

class TopLevelDecl {
public:
    virtual ~TopLevelDecl();

    virtual std::string print() { return ""; }
};
