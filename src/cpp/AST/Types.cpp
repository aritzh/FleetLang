#include <sstream>
#include <utility>
#include "Types.h"
#include "../CodeGen.h"

static EPrimitiveType signed_ints[] = {EPrimitiveType::i8, EPrimitiveType::i16, EPrimitiveType::i32, EPrimitiveType::i64, EPrimitiveType::i128};
static EPrimitiveType unsigned_ints[] = {EPrimitiveType::u8, EPrimitiveType::u16, EPrimitiveType::u32, EPrimitiveType::u64, EPrimitiveType::u128};

inline static bool isPowerOfTwo(unsigned int n) {
    return n && !(n & (n - 1));
}

EPrimitiveType getPrimitiveFromSize(unsigned int size, bool sign) {
    if (isPowerOfTwo(size) && size >= 8 && size <= 128) {
        auto index = static_cast<unsigned int>(log2(size) - 3);
        assert(index >= 0 && index <= 4);
        if (sign) return signed_ints[index];
        else return unsigned_ints[index];
    }
    throw std::logic_error(fmt::format("Cannot create an integer of {} bits", size));
}

EPrimitiveType PrimitiveType::getType() {
    return this->type;
}

PrimitiveType::PrimitiveType(EPrimitiveType type) : type(type) {}

std::string PrimitiveType::print() {
    switch (type) {
        case EPrimitiveType::Void:
            return "void";
        case EPrimitiveType::Bool:
            return "bool";
        case EPrimitiveType::Int:
            return "int";
        case EPrimitiveType::Float:
            return "float";
        case EPrimitiveType::Double:
            return "double";
        case EPrimitiveType::i8:
            return "i8";
        case EPrimitiveType::i16:
            return "i16";
        case EPrimitiveType::i32:
            return "i32";
        case EPrimitiveType::i64:
            return "i64";
        case EPrimitiveType::i128:
            return "i128";
        case EPrimitiveType::Uint:
            return "uint";
        case EPrimitiveType::u8:
            return "u8";
        case EPrimitiveType::u16:
            return "u16";
        case EPrimitiveType::u32:
            return "u32";
        case EPrimitiveType::u64:
            return "u64";
        case EPrimitiveType::u128:
            return "u128";
    }
    throw std::logic_error("Unknown type");
}

llvm::Type *GetFloatType(llvm::LLVMContext *TheContext) {
    return llvm::Type::getFloatTy(*TheContext);
}

llvm::Type *GetBoolType(llvm::LLVMContext *TheContext) {
    return llvm::Type::getInt1Ty(*TheContext);
}

llvm::Type *GetVoidType(llvm::LLVMContext *TheContext) {
    return llvm::Type::getVoidTy(*TheContext);
}

llvm::Type *GetDoubleType(llvm::LLVMContext *TheContext) {
    return llvm::Type::getDoubleTy(*TheContext);
}

llvm::Type *PrimitiveType::GetLLVMType(CodeGen &gen) {
    switch (type) {
        case EPrimitiveType::Void:
            return GetVoidType(gen.TheContext);
        case EPrimitiveType::Bool:
            return GetBoolType(gen.TheContext);
        case EPrimitiveType::Float:
            return GetFloatType(gen.TheContext);
        case EPrimitiveType::Double:
            return GetDoubleType(gen.TheContext);
        default:
            break;
    }

    if (isIntegerType()) {
        return llvm::Type::getIntNTy(*gen.TheContext, getSize(gen) * 8);
    }

    throw std::logic_error("Unknown primitive type");
}

bool PrimitiveType::isIntegerType() const {
    return this->type >= EPrimitiveType::Int && this->type <= EPrimitiveType::u128;
}

bool PrimitiveType::isFloatingType() const {
    return this->type == EPrimitiveType::Float ||
           this->type == EPrimitiveType::Double;
}

bool PrimitiveType::isEqual(CodeGen &gen, std::weak_ptr<Type> other) const {
    auto shType = other.lock();
    const auto &other_derived = std::dynamic_pointer_cast<PrimitiveType>(shType);
    return this->type == other_derived->type;
}

std::string PrimitiveType::GetManglingName() {
    switch (type) {
        case EPrimitiveType::Int:
            return "I";
        case EPrimitiveType::Float:
            return "F";
        case EPrimitiveType::Bool:
            return "b";
        case EPrimitiveType::Void:
            return "V";
        case EPrimitiveType::Double:
            return "D";
        default:
            break;
    }

    if (this->type >= EPrimitiveType::i8 && this->type <= EPrimitiveType::u128 && this->type != EPrimitiveType::Int) {
        return print();
    }

    throw std::logic_error("Unknown type");
}

unsigned int PrimitiveType::getSize(CodeGen &gen) const {
    if (this->type == EPrimitiveType::Bool) return 1;
    else return this->getSizeInBits(gen) / 8;
}

bool PrimitiveType::isSigned() const {
    return this->type >= EPrimitiveType::Int && this->type <= EPrimitiveType::i128;
}

unsigned int PrimitiveType::getSizeInBits(CodeGen &gen) const {
    switch (this->type) {
        case EPrimitiveType::i8:
        case EPrimitiveType::u8:
            return 8;
        case EPrimitiveType::i16:
        case EPrimitiveType::u16:
            return 16;
        case EPrimitiveType::i32:
        case EPrimitiveType::u32:
            return 32;
        case EPrimitiveType::i64:
        case EPrimitiveType::u64:
            return 64;
        case EPrimitiveType::i128:
        case EPrimitiveType::u128:
            return 128;
        case EPrimitiveType::Int:
        case EPrimitiveType::Uint:
            return gen.GetPointerSize();
        case EPrimitiveType::Float:
            return 32;
        case EPrimitiveType::Double:
            return 64;
        case EPrimitiveType::Bool:
            return 1;
        case EPrimitiveType::Void:
            return 0;
        default:
            throw std::logic_error("Unknown type");
    }
}

llvm::DIType *PrimitiveType::GetDIType(CodeGen &gen) {
    switch (this->type) {
        case EPrimitiveType::Void:
            return gen.DB->createBasicType("void", 0, llvm::dwarf::DW_ATE_unsigned);
        case EPrimitiveType::Bool:
            return gen.DB->createBasicType("bool", 1, llvm::dwarf::DW_ATE_boolean);
        case EPrimitiveType::Float:
            return gen.DB->createBasicType("float", 32, llvm::dwarf::DW_ATE_float);
        case EPrimitiveType::Double:
            return gen.DB->createBasicType("double", 64, llvm::dwarf::DW_ATE_float);
        case EPrimitiveType::Int:
            return gen.DB->createBasicType("int", gen.GetPointerSize(), llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::i8:
            return gen.DB->createBasicType("i8", 8, llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::i16:
            return gen.DB->createBasicType("i16", 16, llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::i32:
            return gen.DB->createBasicType("i32", 32, llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::i64:
            return gen.DB->createBasicType("i64", 64, llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::i128:
            return gen.DB->createBasicType("i128", 128, llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::Uint:
            return gen.DB->createBasicType("uint", gen.GetPointerSize(), llvm::dwarf::DW_ATE_signed_fixed);
        case EPrimitiveType::u8:
            return gen.DB->createBasicType("u8", 8, llvm::dwarf::DW_ATE_unsigned_fixed);
        case EPrimitiveType::u16:
            return gen.DB->createBasicType("u16", 16, llvm::dwarf::DW_ATE_unsigned_fixed);
        case EPrimitiveType::u32:
            return gen.DB->createBasicType("u32", 32, llvm::dwarf::DW_ATE_unsigned_fixed);
        case EPrimitiveType::u64:
            return gen.DB->createBasicType("u64", 64, llvm::dwarf::DW_ATE_unsigned_fixed);
        case EPrimitiveType::u128:
            return gen.DB->createBasicType("u128", 128, llvm::dwarf::DW_ATE_unsigned_fixed);
    }
    throw std::logic_error("Unknown primitive type");
}

Type::~Type() {
}

bool Type::areEqual(CodeGen &gen, std::weak_ptr<Type> a, std::weak_ptr<Type> b) {
    std::shared_ptr<Type> ra = gen.ResolveType(a).lock();
    std::shared_ptr<Type> rb = gen.ResolveType(b).lock();

    if (ra == nullptr || rb == nullptr) return false;
    if (ra == rb) return true;
    auto &rra = *ra.get();
    auto &rrb = *rb.get();
    if (typeid(rra) == typeid(rrb)) {
        return ra->isEqual(gen, rb);
    }
    return false;
}

void Type::noop() {}

ArrayType::ArrayType(std::weak_ptr<Type> component, unsigned long size)
        : component(std::move(component)), size(size) {}

std::string ArrayType::print() {
    std::stringstream ss;
    ss << this->component.lock()->print();
    ss << '[' << size << ']';
    return ss.str();
}

std::weak_ptr<Type> ArrayType::ComponentType() const {
    return component;
}

unsigned long ArrayType::Size() const {
    return size;
}

llvm::Type *ArrayType::GetLLVMType(CodeGen &gen) {
    this->component = gen.ResolveType(this->component);
    return llvm::ArrayType::get(this->component.lock()->GetLLVMType(gen), size);
}

bool ArrayType::isEqual(CodeGen &gen, std::weak_ptr<Type> other) const {
    auto shType = other.lock();
    const auto &o = std::dynamic_pointer_cast<ArrayType>(shType);
    return Type::areEqual(gen, this->component, o->ComponentType()) && this->size == o->size;
}

std::string ArrayType::GetManglingName() {
    std::stringstream ss;
    // TODO Choose a better char for begin and end array type
    ss << "AB" << size << "AE";
    return this->component.lock()->GetManglingName() + ss.str();
}

unsigned int ArrayType::getSize(CodeGen &gen) const {
    // TODO Possible overflow
    return static_cast<unsigned int>(this->component.lock()->getSize(gen) * size);
}

llvm::DIType *ArrayType::GetDIType(CodeGen &gen) {
    llvm::DIType *compT = this->component.lock()->GetDIType(gen);
    llvm::SmallVector<llvm::Metadata *, 1> subrange;
    subrange.push_back(gen.DB->getOrCreateSubrange(0, static_cast<int64_t>(size)));
    compT = gen.DB->createArrayType(size, 0, compT, gen.DB->getOrCreateArray(subrange));
    return compT;
}

NamedType::NamedType(Position pos, std::string name)
        : pos(std::move(pos)), name(std::move(name)) {}

const std::string &NamedType::getName() const {
    return name;
}

std::string NamedType::print() {
    return this->name;
}

llvm::Type *NamedType::GetLLVMType(CodeGen &gen) {
    std::weak_ptr<StructDeclaration> type = gen.FindStruct(name);
    llvm::Type *structType = type.lock()->GetLLVMType(gen);
    if (structType != nullptr) return structType;
    gen.error(pos, "Type named '{}' has not been declared", name);
    return nullptr;
}

bool NamedType::isEqual(CodeGen &gen, std::weak_ptr<Type> other) const {
    auto shType = other.lock();
    const auto &o = std::dynamic_pointer_cast<NamedType>(shType);
    return this->name == o->name;
}

std::string NamedType::GetManglingName() {
    return this->name;
}

unsigned int NamedType::getSize(CodeGen &gen) const {
    std::weak_ptr<StructDeclaration> type = gen.FindStruct(name);
    return type.lock()->getSize(gen);
}

llvm::DIType *NamedType::GetDIType(CodeGen &gen) {
    std::weak_ptr<StructDeclaration> type = gen.FindStruct(name);
    llvm::DIType *structType = type.lock()->GetDIType(gen);
    if (structType != nullptr) return structType;
    gen.error(pos, "Type named '{}' has not been declared", name);
    return nullptr;
}

PointerToType::PointerToType(std::weak_ptr<Type> type)
        : type(std::move(type)) {}

std::weak_ptr<Type> PointerToType::getElement() const {
    return this->type;
}

std::string PointerToType::print() {
    return "*" + this->type.lock()->print();
}

llvm::Type *PointerToType::GetLLVMType(CodeGen &gen) {
    this->type = gen.ResolveType(this->type);
    llvm::Type *elementType = this->type.lock()->GetLLVMType(gen);
    if (elementType == nullptr) return nullptr;
    return elementType->getPointerTo();
}

bool PointerToType::isEqual(CodeGen &gen, std::weak_ptr<Type> other) const {
    auto shType = other.lock();
    const auto &o = std::dynamic_pointer_cast<PointerToType>(shType);
    return Type::areEqual(gen, this->type, o->type);
}

std::string PointerToType::GetManglingName() {
    return "P" + this->type.lock()->GetManglingName();
}

unsigned int PointerToType::getSize(CodeGen &gen) const {
    return gen.GetPointerSize() / 8;
}

llvm::DIType *PointerToType::GetDIType(CodeGen &gen) {
    this->type = gen.ResolveType(this->type);
    llvm::DIType *elementType = this->type.lock()->GetDIType(gen);
    if (elementType == nullptr) return nullptr;
    return gen.DB->createPointerType(elementType, gen.GetPointerSize());
}
