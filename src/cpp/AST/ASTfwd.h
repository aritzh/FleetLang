#pragma once

#include "Function.h"
#include "StructDeclaration.h"

class TopLevelDecl;

class StructDeclaration;

class FunctionDeclaration;

class Function;

class StmtAST;

class VarDeclStmtAST;

class AssignStmtAST;

class IfStmtAST;

class WhileStmtAST;

class ReturnStmtAST;

class ExprStmtAST;

class BlockStmtAST;

class VarDeclStmtAST;

class ForStmtAST;

class DoWhileStmtAST;

class ExprAST;

class StringExprAST;

class FloatExprAST;

class DoubleExprAST;

class IntExprAST;

class BinaryExprAST;

class UnaryExprAST;

class CallExprAST;

class VariableExprAST;

class ComparisonExprAST;

class BoolOpExprAST;

class LiteralBoolExprAST;

class CastingExprAST;

class ArrayExprAST;

class MemberAccessAST;

class AddressOfExprAST;

class DereferenceExprAST;
