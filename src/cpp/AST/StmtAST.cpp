// Created by aritz.

#include <sstream>
#include <utility>
#include "StmtAST.h"
#include "ExprAST.h"

AssignStmtAST::AssignStmtAST(Position pos, std::unique_ptr<ExprAST> left,
                             std::unique_ptr<ExprAST> value,
                             AssignmentOp op)
        : StmtAST(std::move(pos)), left(std::move(left)), value(std::move(value)), op(op) {}

std::string AssignStmtAST::print() {
    return this->left->print() + " = " + this->value->print() + ";\n";
}

ExprAST *AssignStmtAST::GetLeft() {
    return this->left.get();
}

ExprAST *AssignStmtAST::GetValue() {
    return this->value.get();
}

void AssignStmtAST::Generate(CodeGen &gen) {
//    gen.PushScope();
    /*llvm::Value *lValue = */left->Generate(gen);
    llvm::Value *address = left->GetLLVMAddress();

    if (left->GetLLVMValue() == nullptr) {
//        gen.PopScope();
        return;
    }

    if (address == nullptr) {
//        gen.PopScope();
        gen.error(pos, "Left side of assignment is not assignable");
        return;
    }

    if (!address->getType()->isPointerTy()) {
//        gen.PopScope();
        throw std::logic_error("The address of the left side of the assignment is not a pointer");
    }

    if (op == AssignmentOp::ADD_ASSIGN) {
        throw std::logic_error("Remove this");
    }

    llvm::Value *pValue = value->Generate(gen);
    if (pValue == nullptr) {
//        gen.PopScope();
        return;
    }

    // TODO Check if this is right
    if (!Type::areEqual(gen, this->left->GetType(), this->value->GetType())) {
        gen.error(pos,
                  "Cannot store value of type '{}' into a '{}'",
                  this->value->GetType()->print(),
                  this->left->GetType()->print());
//        gen.PopScope();
        return;
    }

    if (pValue->getType()->isPointerTy() && pValue->getType()->getPointerElementType()->isArrayTy() &&
        address->getType()->getPointerElementType()->isPointerTy() &&
        pValue->getType()->getPointerElementType()->getArrayElementType() ==
        address->getType()->getPointerElementType()->getPointerElementType()) {
        pValue = gen.Builder.CreateConstGEP2_32(pValue->getType()->getPointerElementType(), pValue, 0, 0);
    }

//    if (pValue->getType()->isArrayTy() && pValue->getType()->getArrayElementType() == address->getType()->getPointerElementType()) {
//        pValue = gen.Builder.CreateConstGEP2_32(pValue->getType(), pValue, 0, 0);
//    }

    gen.Builder.CreateStore(pValue, address); // TODO Add name from lval
//    gen.PopScope();
}

IfStmtAST::IfStmtAST(Position pos,
                     std::unique_ptr<ExprAST> cond,
                     std::unique_ptr<StmtAST> tru,
                     std::unique_ptr<StmtAST> fals)
        : StmtAST(std::move(pos)), cond(std::move(cond)), tru(std::move(tru)), fals(std::move(fals)) {}

std::string IfStmtAST::print() {
    std::stringstream ss;
    ss << "if (" << this->cond->print() << ") {\n";
    ss << this->tru->print();
    ss << "}";
    if (this->fals) {
        ss << " else {\n";
        ss << this->fals->print();
        ss << "}\n";
    } else {
        ss << "\n";
    }
    return ss.str();
}

ExprAST *IfStmtAST::GetCond() {
    return this->cond.get();
}

StmtAST *IfStmtAST::GetTrue() {
    return this->tru.get();
}

StmtAST *IfStmtAST::GetFalse() {
    return this->fals.get();
}

void IfStmtAST::Generate(CodeGen &gen) {
    llvm::Function *TheFunction = gen.Builder.GetInsertBlock()->getParent();

    llvm::BasicBlock *thenBB = llvm::BasicBlock::Create(*gen.TheContext, "then", TheFunction);
    llvm::BasicBlock *elseBB = nullptr;
    llvm::BasicBlock *mergeBB = llvm::BasicBlock::Create(*gen.TheContext, "ifcont");

    llvm::Value *condV = cond->Generate(gen);
    const auto &p = std::dynamic_pointer_cast<PrimitiveType>(cond->GetType());
    if (!p || p->getType() != EPrimitiveType::Bool) {
        gen.error(pos, "If condition must be a boolean expression");
        return;
    }

    bool jumpGenerated = false;
    if (fals) {
        elseBB = llvm::BasicBlock::Create(*gen.TheContext, "else");
        gen.Builder.CreateCondBr(condV, thenBB, elseBB);
    } else {
        jumpGenerated = true;
        gen.Builder.CreateCondBr(condV, thenBB, mergeBB);
    }

    gen.Builder.SetInsertPoint(thenBB);
    tru->Generate(gen);
    if (!TheFunction->back().back().isTerminator()) {
        jumpGenerated = true;
        gen.Builder.CreateBr(mergeBB);
    }

    if (fals) {
        TheFunction->getBasicBlockList().push_back(elseBB);
        gen.Builder.SetInsertPoint(elseBB);
        fals->Generate(gen);
        if (!TheFunction->back().back().isTerminator()) {
            jumpGenerated = true;
            gen.Builder.CreateBr(mergeBB);
        }
    }

    if (jumpGenerated) {
        TheFunction->getBasicBlockList().push_back(mergeBB);
        gen.Builder.SetInsertPoint(mergeBB);
    } else {
        delete mergeBB;
    }
}

WhileStmtAST::WhileStmtAST(Position pos, std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> body)
        : StmtAST(std::move(pos)), cond(std::move(cond)), body(std::move(body)) {}

std::string WhileStmtAST::print() {
    std::stringstream ss;
    ss << "while (" << this->cond->print() << ") {\n";
    ss << this->body->print();
    ss << "}\n";
    return ss.str();
}

ExprAST *WhileStmtAST::GetCond() {
    return this->cond.get();
}

StmtAST *WhileStmtAST::GetBody() {
    return this->body.get();
}

void WhileStmtAST::Generate(CodeGen &gen) {
    gen.PushScope();
    llvm::Function *TheFunction = gen.Builder.GetInsertBlock()->getParent();

    llvm::BasicBlock *condBB = llvm::BasicBlock::Create(*gen.TheContext, "whilecond", TheFunction);
    llvm::BasicBlock *whileBB = llvm::BasicBlock::Create(*gen.TheContext, "while");
    llvm::BasicBlock *contBB = llvm::BasicBlock::Create(*gen.TheContext, "whilecont");

    gen.Builder.CreateBr(condBB); // Previous block will jump to condition block

    gen.Builder.SetInsertPoint(condBB);

    llvm::Value *condV = cond->Generate(gen);
    const auto &p = std::dynamic_pointer_cast<PrimitiveType>(cond->GetType());
    if (!p || p->getType() != EPrimitiveType::Bool) {
        // TODO pos should be the position of the cond expression, so add a getter and use that.
        // TODO also change the other stmts, like if, do-while and for.
        gen.error(pos, "While condition must be a boolean expression");
        return;
    }

    gen.Builder.CreateCondBr(condV, whileBB, contBB);

    TheFunction->getBasicBlockList().push_back(whileBB);
    gen.Builder.SetInsertPoint(whileBB);

    body->Generate(gen);
    if (!TheFunction->back().back().isTerminator()) gen.Builder.CreateBr(condBB);

    TheFunction->getBasicBlockList().push_back(contBB);
    gen.Builder.SetInsertPoint(contBB);

    gen.PopScope();
}

ReturnStmtAST::ReturnStmtAST(Position pos, std::unique_ptr<ExprAST> value)
        : StmtAST(std::move(pos)), value(std::move(value)) {}

std::string ReturnStmtAST::print() {
    if (this->value) {
        return "return " + this->value->print() + ";\n";
    } else {
        return "return;\n";
    }
}

ExprAST *ReturnStmtAST::GetExpr() {
    return this->value.get();
}

void ReturnStmtAST::Generate(CodeGen &gen) {
    if (value) gen.Builder.CreateRet(value->Generate(gen));
    else gen.Builder.CreateRetVoid();
}

ExprStmtAST::ExprStmtAST(Position pos, std::unique_ptr<ExprAST> expr)
        : StmtAST(std::move(pos)), expr(std::move(expr)) {}

std::string ExprStmtAST::print() {
    return this->expr->print() + ";\n";
}

ExprAST *ExprStmtAST::GetExpr() {
    return this->expr.get();
}

void ExprStmtAST::Generate(CodeGen &gen) {
    expr->Generate(gen);
}

BlockStmtAST::BlockStmtAST(Position pos, std::vector<std::unique_ptr<StmtAST>> stmts)
        : StmtAST(std::move(pos)), stmts(std::move(stmts)) {}

std::string BlockStmtAST::print() {
    std::stringstream ss;

    for (auto &s : this->stmts) {
        ss << s->print() << "\n";
    }
    return ss.str();
}

const std::vector<std::unique_ptr<StmtAST>> &BlockStmtAST::GetStmts() {
    return this->stmts;
}

void BlockStmtAST::Generate(CodeGen &gen) {
    gen.PushScope();
    for (auto &ss : stmts) {
        ss->Generate(gen);
    }
    gen.PopScope();
}

VarDeclStmtAST::VarDeclStmtAST(Position pos,
                               std::string name,
                               std::weak_ptr<Type> type,
                               std::unique_ptr<ExprAST> expr)
        : StmtAST(std::move(pos)), name(std::move(name)), type(std::move(type)), expr(std::move(expr)) {
    assert(!this->type.expired());
}

std::string VarDeclStmtAST::print() {
    return this->type.lock()->print() + " " + this->name + ";\n";
}

std::string VarDeclStmtAST::GetName() {
    return this->name;
}

std::weak_ptr<Type> VarDeclStmtAST::GetType() {
    return this->type;
}

void VarDeclStmtAST::Generate(CodeGen &gen) {
    const std::weak_ptr<Type> &type = gen.ResolveType(this->type);
    if (!type.lock()) {
        gen.error(this->pos, "Unknown type");
        return;
    }
    this->type = type;
    if (this->sttc) {
        std::string name = gen.GetCurrentFunction() + "." + this->name;
        llvm::Type *llvmType = this->type.lock()->GetLLVMType(gen);
        gen.TheModule->getOrInsertGlobal(name, llvmType);
        llvm::GlobalVariable *var = gen.TheModule->getNamedGlobal(name);
        var->setLinkage(llvm::GlobalVariable::LinkageTypes::InternalLinkage);

        if (this->expr) {
            this->expr->Generate(gen);
            if (!this->expr->isComptimeConstant()) {
                gen.error(pos, "Initializer values for static variables must be compile time constant");
                return;
            } else {
                var->setInitializer(this->expr->GetConstant());
            }
        }
        this->llvmValue = var;
        gen.InsertSymbol(this->name, this);
    } else {
        if (this->global) {
            llvm::Type *llvmType = this->type.lock()->GetLLVMType(gen);
            if (gen.TheModule->getNamedGlobal(name)) {
                gen.error(pos, "Global with name '{}' is already defined!", name);
                return;
            }
            gen.TheModule->getOrInsertGlobal(name, llvmType);
            llvm::GlobalVariable *var = gen.TheModule->getNamedGlobal(name);
            if (this->expr) {
                llvm::Value *value = this->expr->Generate(gen);
                if (value == nullptr) return;
                if (!expr->isComptimeConstant()) {
                    gen.error(pos, "Initializer values for global variables must be compile time constant");
                    return;
                }
                // TODO Extend 0-to-null to any assignment, not only global initializers
                llvm::Constant *val = this->expr->GetConstant();
                const std::shared_ptr<Type> &ptr = this->type.lock();
                if (std::dynamic_pointer_cast<PointerToType>(ptr)) {
                    var->setInitializer(llvm::ConstantPointerNull::get(llvm::dyn_cast<llvm::PointerType>(llvmType)));
                } else {
                    var->setInitializer(val);
                }
                var->setLinkage(llvm::GlobalVariable::LinkageTypes::InternalLinkage);
                this->llvmValue = var;
            }
        } else {
            this->llvmValue = gen.CreateAlloca(type.lock().get(), name);
            if (this->expr) {
                llvm::Value *value = this->expr->Generate(gen);
                if (value == nullptr) return;
                gen.Builder.CreateStore(value, this->llvmValue);
            }
        }
        gen.InsertSymbol(this);

    }
}

llvm::Value *VarDeclStmtAST::GetLLVMValue() const {
    return this->llvmValue;
}

void VarDeclStmtAST::SetLLVMValue(llvm::Value *value) {
    this->llvmValue = value;
}

void VarDeclStmtAST::SetStatic(bool sttc) {
    this->sttc = sttc;
}

bool VarDeclStmtAST::isStatic() const {
    return this->sttc;
}

void VarDeclStmtAST::SetGlobal(bool global) {
    this->global = global;
}

std::string ForStmtAST::print() {
    // TODO
    return "";
}

ForStmtAST::ForStmtAST(Position pos,
                       std::unique_ptr<StmtAST> init,
                       std::unique_ptr<ExprAST> cond,
                       std::unique_ptr<StmtAST> update,
                       std::unique_ptr<StmtAST> body)
        : StmtAST(std::move(pos)),
          init(std::move(init)),
          cond(std::move(cond)),
          update(std::move(update)),
          body(std::move(body)) {}

StmtAST *ForStmtAST::GetInit() const {
    return init.get();
}

ExprAST *ForStmtAST::GetCond() const {
    return cond.get();
}

StmtAST *ForStmtAST::GetUpdate() const {
    return update.get();
}

StmtAST *ForStmtAST::GetBody() const {
    return body.get();
}

void ForStmtAST::Generate(CodeGen &gen) {
    gen.PushScope();

    llvm::Function *TheFunction = gen.Builder.GetInsertBlock()->getParent();

    llvm::BasicBlock *condBB = llvm::BasicBlock::Create(*gen.TheContext, "forcond", TheFunction);
    llvm::BasicBlock *bodyBB = llvm::BasicBlock::Create(*gen.TheContext, "for");
    llvm::BasicBlock *contBB = llvm::BasicBlock::Create(*gen.TheContext, "forcont");

    if (init) init->Generate(gen);

    gen.Builder.CreateBr(condBB); // Previous block will jump to condition block

    gen.Builder.SetInsertPoint(condBB);

    if (cond) {
        llvm::Value *condV = cond->Generate(gen);
        const auto &p = std::dynamic_pointer_cast<PrimitiveType>(cond->GetType());
        if (!p || p->getType() != EPrimitiveType::Bool) {
            gen.error(pos, "For condition must be a boolean expression");
            return;
        }
        gen.Builder.CreateCondBr(condV, bodyBB, contBB);
    } else {
        gen.Builder.CreateBr(bodyBB);
    }

    TheFunction->getBasicBlockList().push_back(bodyBB);
    gen.Builder.SetInsertPoint(bodyBB);

    body->Generate(gen);
    if (!TheFunction->back().back().isTerminator()) {
        if (update) update->Generate(gen);
        gen.Builder.CreateBr(condBB);
    }

    TheFunction->getBasicBlockList().push_back(contBB);
    gen.Builder.SetInsertPoint(contBB);

    gen.PopScope();
}

DoWhileStmtAST::DoWhileStmtAST(Position pos, std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> body)
        : StmtAST(std::move(pos)), cond(std::move(cond)), body(std::move(body)) {}

std::string DoWhileStmtAST::print() {
    // TODO
    return "";
}

ExprAST *DoWhileStmtAST::GetCond() {
    return this->cond.get();
}

StmtAST *DoWhileStmtAST::GetBody() {
    return this->body.get();
}

void DoWhileStmtAST::Generate(CodeGen &gen) {
    gen.PushScope();

    llvm::Function *TheFunction = gen.Builder.GetInsertBlock()->getParent();

    llvm::BasicBlock *bodyBB = llvm::BasicBlock::Create(*gen.TheContext, "", TheFunction);
    // FIXME For some reason, I cannot give this block a name!
    // bodyBB->setName("dowhile");
    llvm::BasicBlock *contBB = llvm::BasicBlock::Create(*gen.TheContext, "dowhilecont");

    gen.Builder.CreateBr(bodyBB); // Previous block will jump to condition block
    TheFunction->getBasicBlockList().push_back(bodyBB);

    gen.Builder.SetInsertPoint(bodyBB);
    if (body) body->Generate(gen);
    llvm::Value *condV = cond->Generate(gen);
    const auto &p = std::dynamic_pointer_cast<PrimitiveType>(cond->GetType());
    if (!p || p->getType() != EPrimitiveType::Bool) {
        gen.error(pos, "Do-while condition must be a boolean expression");
        return;
    }
    gen.Builder.CreateCondBr(condV, bodyBB, contBB);

    TheFunction->getBasicBlockList().push_back(contBB);
    gen.Builder.SetInsertPoint(contBB);

    gen.PopScope();
}

StmtAST::StmtAST(Position pos)
        : pos(std::move(pos)) {}

StmtAST::~StmtAST() {}

void StmtAST::Generate(CodeGen &gen) {}
