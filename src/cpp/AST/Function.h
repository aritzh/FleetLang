// Created by aritz.

#pragma once


#include <memory>
#include <llvm/IR/Function.h>
#include "TopLevelDecl.h"
#include "Types.h"
#include "Position.h"
#include "Annotation.h"


class Function;

class StmtAST;

class VarDeclStmtAST;

class CodeGen;

enum class FunctionType {
    Normal, // Normal functions.
    Foreign, // Cannot be overloaded, but its name is not mangled
};

class FunctionDeclaration : public TopLevelDecl {
protected:
    Position pos;
    std::weak_ptr<Type> ret;
    std::string name;
    std::vector<std::unique_ptr<VarDeclStmtAST>> args;
    std::vector<std::unique_ptr<Annotation>> annotations;
    bool isPublic;
    bool declared = false;
    FunctionType type;

    llvm::Function *llvmValue = nullptr;
public:
    ~FunctionDeclaration() override = default;

    FunctionDeclaration(Position pos,
                        std::weak_ptr<Type> ret,
                        std::string name,
                        std::vector<std::unique_ptr<VarDeclStmtAST>> args,
                        std::vector<std::unique_ptr<Annotation>> annotations,
                        bool isPublic,
                        FunctionType type);

    std::string print() override;

    std::string GetName();

    std::weak_ptr<Type> GetReturnType();

    const std::vector<std::unique_ptr<VarDeclStmtAST>> &GetArgs();

    bool operator==(const FunctionDeclaration &other) const;

    bool operator!=(const FunctionDeclaration &other) const;

    virtual llvm::Function *Generate(CodeGen &gen);

    llvm::Function *GetLLVMValue();

    bool accepts(CodeGen &gen, const std::vector<std::weak_ptr<Type>> &types) const;

    std::string GetMangledName() const;

    Annotation *FindAnnotation(const std::string &name) const;

    std::string PrintSignature();

    Position GetPosition() const;

    llvm::Function *GenerateDeclaration(CodeGen &gen);
};

class Function : public FunctionDeclaration {
    std::vector<std::unique_ptr<StmtAST>> stmts;

public:
    ~Function() override = default;

    explicit Function(Position pos,
                      std::weak_ptr<Type> ret,
                      std::string name,
                      std::vector<std::unique_ptr<VarDeclStmtAST>> args,
                      std::vector<std::unique_ptr<StmtAST>> stmts,
                      std::vector<std::unique_ptr<Annotation>> annotations,
                      bool isPublic,
                      FunctionType type);

    std::string print() override;

    const std::vector<std::unique_ptr<StmtAST>> &GetStmts();

    llvm::Function *Generate(CodeGen &gen) override;
};


