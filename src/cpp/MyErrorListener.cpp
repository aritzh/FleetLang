#include "MyErrorListener.h"

void MyErrorListener::syntaxError(antlr4::Recognizer *recognizer, antlr4::Token *offendingSymbol, size_t line,
                                  size_t charPositionInLine, const std::string &msg, std::exception_ptr e) {
    logger->error("[{}:{}:{}] Syntax error: {}", fileName, line, charPositionInLine, msg);
}

void MyErrorListener::reportAmbiguity(antlr4::Parser *recognizer, const antlr4::dfa::DFA &dfa, size_t startIndex,
                                      size_t stopIndex, bool exact, const antlrcpp::BitSet &ambigAlts,
                                      antlr4::atn::ATNConfigSet *configs) {
//    std::cout << "" << std::endl;
}

void MyErrorListener::reportAttemptingFullContext(antlr4::Parser *recognizer,
                                                  const antlr4::dfa::DFA &dfa,
                                                  size_t startIndex,
                                                  size_t stopIndex,
                                                  const antlrcpp::BitSet &conflictingAlts,
                                                  antlr4::atn::ATNConfigSet *configs) {
//    std::cout << "" << std::endl;
}

void MyErrorListener::reportContextSensitivity(antlr4::Parser *recognizer,
                                               const antlr4::dfa::DFA &dfa,
                                               size_t startIndex,
                                               size_t stopIndex,
                                               size_t prediction,
                                               antlr4::atn::ATNConfigSet *configs) {
//    std::cout << "" << std::endl;
}

MyErrorListener::MyErrorListener(std::shared_ptr<spdlog::logger> logger, std::string fileName)
        : logger(std::move(logger)), fileName(std::move(fileName)) {}
