grammar FleetLang;

program
    : tlds+=topLevelDecl* EOF
    ;

topLevelDecl
    : function
    | structDecl
    | declaration stmtEnd
    ;

structDecl
    : annotations+=annotation* CLASS name=ID '{' (fields+=declaration stmtEnd)* '}'
    ;

function
    : annotations+=annotation* pub=PUBLIC? (retType=type | voidTy=voidType) name=(ID|FOREIGN_FUNC_ID) '(' (args+=type argNames+=ID (',' args+=type argNames+=ID)* )? ')'
      ( ('{' stmts+=statement* '}') | ';')
    ;

annotation
    : name=INTRINSIC_FUNC_ID ('(' args+=STRING_LIT (',' args+=STRING_LIT)* ')')?
    ;

type
    : name=ID             # customBaseType
    | name=primitiveType  # primitiveBaseType
    | '*' ty=type         # pointerToType
    | base=type '[' arrayIdxs=INT_LIT ']' # arrayType
    ;

primitiveType
    : INT
    | UINT
    | FLOAT
    | DOUBLE
    | BOOLEAN
    | SIZED_INT_TYPE
    ;

voidType
    : VOID
    ;

statement
    : IF '(' cond=expression')' whenTrue=statement (ELSE whenFalse=statement)?                     # ifStmt
    | WHILE '(' cond=expression ')' whenTrue=statement                                             # whileStmt
    | RETURN expr=expression? stmtEnd                                                              # returnStmt
    | '{' stmts+=statement* '}'                                                                    # blockStmt
    | sttc=STATIC? declaration stmtEnd                                                             # declStmt
    | assign=assignmentStmt stmtEnd                                                                # assignStmt
    | expr=expression stmtEnd                                                                      # expressionStmt
    | FOR '(' init=declaration? ';' cond=expression? ';' update=assignmentStmt? ')' body=statement # forStmt
    | DO body=statement WHILE '(' cond=expression ')' stmtEnd                                      # doWhileStmt
    | stmtEnd                                                                                      # emptyStmt
    ;

assignmentStmt
    : left=expression op=assignmentOperator right=expression  # normalAssignment
    | left=expression op=(INCREMENT|DECREMENT)                # incDecAssignment
    ;

assignmentOperator
    : op=('+=' | '-=' | '*=' | '/=' | '&&=' | '||=' | '&=' | '|=' | '^=' | '%=' | '=');

declaration
    : baseType=type name=ID ('=' expr=expression)?
    ;

expression
    : atom=INT_LIT                                                  # intExpr
    | atom=DOUBLE_LIT                                               # doubleExpr
    | atom=FLOAT_LIT                                                # floatExpr
    | atom=STRING_LIT                                               # stringExpr
    | atom=CHAR_LIT                                                 # charExpr
    | atom=(TRUE | FALSE)                                           # literalBool
    | '(' ty=type ')' expr=expression                               # castExpr
//    | '{' (arrayIdxs+=INT_LIT (',' arrayIdxs+=INT_LIT)*)? '}'       # arrayLiteral
    | atom=ID                                                       # idExpr
    | expr=expression '.' member=ID                                 # memberAccess
    | var=expression '[' idx=expression ']'                         # arrayExpr
    | sign=('+'|'-') expr=expression                                # signedExpression
    | '!' expr=expression                                           # notExpression
    | SIZEOF '(' type ')'                                           # sizeofExpr
    | func=(ID|INTRINSIC_FUNC_ID) '(' (args+=expression (',' args+=expression)*)? ')'   # functionCallExpr
    | '&' expr=expression                                           # addressExpr
    | '*' expr=expression                                           # dereferenceExpr
    | left=expression op=('*'|'/'|'%') right=expression             # opExpr
    | left=expression op=('+'|'-') right=expression                 # opExpr
    | left=expression op=('>>>'|'>>'|'<<') right=expression         # opExpr
    | left=expression op=('<='|'>='|'>'|'<') right=expression       # comparisonOp
    | left=expression op=('=='|'!=') right=expression               # comparisonOp
    | left=expression op='&' right=expression                       # opExpr
    | left=expression op='^' right=expression                       # opExpr
    | left=expression op='|' right=expression                       # opExpr
    | left=expression op='&&' right=expression                      # booleanOp
    | left=expression op='||' right=expression                      # booleanOp
    | '(' expr=expression ')'                                       # parenExpr
  //| exprs+=expression (',' exprs+=expression)+                    # tupleExpr
    | ASM '(' asmStr=STRING_LIT (
      ':' (outputs+=asmExprPart (',' outputs+=asmExprPart)* )? (
      ':' (inputs+=asmExprPart (',' inputs+=asmExprPart)* )? (
      ':' clobberStr+=STRING_LIT? (',' clobberStr+=STRING_LIT)* )? )? )? ')' #asmExpr
    ;

asmExprPart
    : str=STRING_LIT '(' var=expression ')'
    ;

stmtEnd
    : ';'
    ;

CLASS: 'class';

UINT: 'uint';
INT: 'int';
FLOAT: 'float';
DOUBLE: 'double';
BOOLEAN: 'bool';
VOID: 'void';

IF: 'if';
WHILE: 'while';
RETURN: 'return';
ELSE: 'else';
FOR: 'for';
DO: 'do';

TRUE: 'true';
FALSE: 'false';

PUBLIC: 'pub';
STATIC: 'static';

SIZEOF: 'sizeof';

ADDRESS: '&'; // And bitwise and

INCREMENT: '++';
DECREMENT: '--';

LEFT_SHIFT: '<<';
LOGICAL_RIGHT_SHIFT: '>>>';
ARITHMETIC_RIGHT_SHIFT: '>>';

LESS_EQUAL: '<=';
GREATER_EQUAL: '>=';
LESS_THAN: '<';
GREATER_THAN: '>';
EQUALS: '==';
NOT_EQUALS: '!=';

ADD_ASSIGN: '+=';
SUB_ASSIGN: '-=';
MUL_ASSIGN: '*=';
DIV_ASSIGN: '/=';
MODULO_ASSIGN: '%=';
BOOL_AND_ASSIGN: '&&=';
BOOL_OR_ASSIGN: '||=';
BIT_AND_ASSIGN: '&=';
BIT_OR_ASSIGN: '|=';
BIT_XOR_ASSIGN: '^=';

BOOL_OR: '||';
BOOL_AND: '&&';

ADD: '+';
SUBTRACT: '-';
MULTIPLY: '*';
DIVIDE: '/';
BIT_OR: '|';
BIT_XOR: '^';
MODULO: '%';

ASSIGN: '=';


LPAREN: '(';
RPAREN: ')';

AT: '@';

ASM: 'asm';

SIZED_INT_TYPE: [iu]('8'|'16'|'32'|'64'|'128');
ID:           [_]?[a-zA-Z][a-zA-Z_0-9]*;
FOREIGN_FUNC_ID: '#' ID;
INTRINSIC_FUNC_ID: '@' ID; // Also annotation IDs

FLOAT_LIT:  [0-9]*'.'[0-9]+([eE][+\-]?[0-9]+)?'f';
DOUBLE_LIT: [0-9]*'.'[0-9]+([eE][+\-]?[0-9]+)?;
INT_LIT:    ('0'[oObBxX]?)?[0-9]+([iu]('8'|'16'|'32'|'64'|'128')?)?;
STRING_LIT: '"' StringCharacter* '"';
CHAR_LIT:   '\'' Character '\'';

DOT: '.';

// These tokens are ignored, do not get through to syntax analysis
WS:           [ \t\r\n]+ -> skip;
COMMENT:      '/*' .*? '*/' -> skip;
LINE_COMMENT: '//' ~[\r\n]* -> skip;

// Character literal
// Adib.: a, \t, \n, \\, \'
fragment Character
    : ~['\\]
    | '\\' [btnfr"'\\]
    ;

// Literal string character
// Adib.: a, \t, \n, \\, \"
fragment StringCharacter
    : ~["\\]
    | '\\' [btnfr"'\\] // Escape sequence
    ;
