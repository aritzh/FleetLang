grammar Fleetv2;

compilation_unit
    : top_level_declaration+ EOF
    ;

top_level_declaration
    : function
    | enum
    | class
    ;

function
    : fn_modifiers* FN ID '(' (ID ':' type (',' ID ':' var_modifiers? type)* )? ')' (':' type)? function_body?
    ;

enum
    : ENUM ID '{' ID (',' ID)* ','? '}'
    ;

class
    : 'TO BE DONE'
    ;

function_body
    : '{' stmt* '}'
    | '=' expr
    ;

fn_modifiers
    : PUB
    | EXTERN
    ;

var_modifiers
    : MUT
    ;

type
    : INTEGER_TYPE                   # int_type
    | FLOAT_TYPE                     # float_type
    | ID                             # id_type
    | POINTER type                   # ptr_type
    | '[' INTEGER_LITERAL? ']' type  # array_type
    | type ',' type                  # tuple_type
    ;

stmt
    : RETURN expr                           # return_stmt
    | decl_modifier ID ':' type             # decl_stmt
    | decl_modifier ID '=' expr             # auto_decl_stmt
    | expr '=' expr                         # assign_stmt
    | WHILE expr stmt_block                 # while_stmt
    | IF expr stmt_block (ELSE stmt_block)? # if_stmt
    | FOR VAR ID IN expr stmt_block         # for_stmt
    | expr                                  # expr_stmt
    ;

decl_modifier
    : VAR
    | CONST
    ;

stmt_block
    : stmt
    | '{' stmt* '}'
    ;

expr
    : expr '*' expr                            # mult_expr
    | expr ('+'|'-') expr                      # sum_expr
    | expr ',' expr                            # tuple_expr
    | expr ('<'|'>'|'<='|'>='|'=='|'!=') expr  # compare_expr
    | expr ('&&'|'||') expr                    # bool_op_expr
    | expr '[' expr ']'                        # array_access_expr
    | expr '(' ( expr (',' expr)* )? ')'       # call_expr
    | expr '++'                                # inc_expr
    | expr '--'                                # dec_expr
    | expr '.' expr                            # member_expr
    | SWITCH expr '{' switch_case* '}'         # switch_expr
    | ('-'|'+')?INTEGER_LITERAL                # int_expr
    | STRING_LITERAL                           # str_expr
    | ID                                       # id_expr
    | '(' expr ')'                             # paren_expr
    ;

switch_case
    : expr '->' expr
    | ELSE '->' expr
    ;

IF: 'if';
FN: 'fn';
RETURN: 'return';
VAR: 'var';
POINTER: '*';
PUB: 'pub';
MUT: 'mut';
WHILE: 'while';
ELSE: 'else';
FOR: 'for';
IN: 'in';
SWITCH: 'switch';
EXTERN: 'extern';
ENUM: 'enum';
CONST: 'const';
INTEGER_TYPE: [iu]('8'|'16'|'32'|'64');
FLOAT_TYPE: 'f'('32'|'64');
ID: [_]?[a-zA-Z_][a-zA-Z_0-9]*;

STRING_LITERAL: '"' StringCharacter* '"';
fragment StringCharacter
    : ~["\\]
    | '\\' [btnfr"'\\] // Escape sequence
    ;

WS:           [ \t\r\n]+ -> skip;

INTEGER_LITERAL: [0-9]+;
